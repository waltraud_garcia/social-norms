<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration{


    public function up(){
  		$this->createDatabase();
 		$this->importMinimumData();
// 		//$this->setDemo();
//$this->setExperiment1();
//$this->setExperiment2();
    $this->setExperiment3();
 		$this->insert('content',['owner' => 1, 'section' => 1, 'category' => 1,'title' => 'Look inside this message!', 'type' => 'Text', 'url' => '', 'message' => "You have been cursed, the amazing King Butterfly will come after you and suck the nectar of the flowers of your garden, this is not any kind of innuendo. In case you do not want this horrific scene to happen you have to post this message at 10 other forums.", 'num_of_views' => 0, 'num_of_complaints' => 0, 'actualDate'=>'2015-03-23 02:41:04']);
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
		$this->setContent();
    }

    public function down(){
    	$this->dropTable('message');
        $this->dropTable('source_message');
        $this->dropTable('view');
        $this->dropTable('complain');
        $this->dropTable('log');
        $this->dropTable('configuration');
        $this->dropTable('event');
        $this->dropTable('argument_rate');
        $this->dropTable('argument');
        $this->dropTable('argument_type');
        $this->dropTable('norm');
        $this->dropTable('modality');
        $this->dropTable('complain_category');
        $this->dropTable('content');
        $this->dropTable('section');
        $this->dropTable('{{%user}}');
        $this->dropTable('role');
        $this->dropTable('content_category');
        $this->dropTable('action');
    }

    private function createDatabase(){
    	echo "\nCreating tables...\n";

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}

    	$this->createTable('source_message', [
    			'id' => Schema::TYPE_PK,
    			'category' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'message' => Schema::TYPE_TEXT,
    	]);

    	$this->createTable('message', [
    			'id' => Schema::TYPE_INTEGER,
    			'language' => Schema::TYPE_STRING . '(16) NOT NULL',
    			'translation' => Schema::TYPE_TEXT,
    			'PRIMARY KEY (id, language)',
    			'FOREIGN KEY (id) REFERENCES source_message(id)',
    	]);

    	$this->createTable('role', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(100) NOT NULL',
    	], $tableOptions);

    	$this->createTable('{{%user}}', [
    			'id' => Schema::TYPE_PK,
    			'username' => Schema::TYPE_STRING . ' NOT NULL',
    			'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
    			'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
    			'password_reset_token' => Schema::TYPE_STRING,
    			'email' => Schema::TYPE_STRING . ' NOT NULL',
    			'role' => Schema::TYPE_INTEGER .' DEFAULT 1',
    			'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
    			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (role) REFERENCES role(id)',
    	], $tableOptions);



    	$this->createTable('section', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(50) NOT NULL',
    	], $tableOptions);

    	$this->createTable('content_category', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(20) NOT NULL',
    	], $tableOptions);

    	$this->createTable('content', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'category' => Schema::TYPE_INTEGER,
    			'section' => Schema::TYPE_INTEGER,
    			'owner' => Schema::TYPE_INTEGER,
    			'title' => Schema::TYPE_STRING . '(512) NOT NULL',
    			'type' => Schema::TYPE_STRING . '(20) NOT NULL',
    			'url' => Schema::TYPE_STRING . '(100)',
    			'message' => Schema::TYPE_STRING . '(512)',
    			'num_of_views' => Schema::TYPE_INTEGER,
    			'num_of_complaints' => Schema::TYPE_INTEGER,
    			'violated_norm' => Schema::TYPE_INTEGER,
    			'actualdate' => Schema::TYPE_STRING . '(20) NOT NULL',
    			'FOREIGN KEY (category) REFERENCES content_category(id)',
    			'FOREIGN KEY (section) REFERENCES section(id)',
    			'FOREIGN KEY (owner) REFERENCES user(id)',
    	], $tableOptions);

    	$this->createTable('complain_category', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(20) NOT NULL',
    	], $tableOptions);

    	$this->createTable('complain', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'user' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'content' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'complain_category' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (complain_category) REFERENCES complain_category(id)',
    	], $tableOptions);

    	$this->createTable('view', [
    			'user' => Schema::TYPE_INTEGER,
    			'content' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'PRIMARY KEY (user, content)',
    			'FOREIGN KEY (user) REFERENCES user(id)',
    			'FOREIGN KEY (content) REFERENCES content(id)',
    	], $tableOptions);

     	$this->createTable('modality', [
    	     	'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
     			'description' => Schema::TYPE_STRING . '(50) NOT NULL',
     	], $tableOptions);

    	$this->createTable('action', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(20) NOT NULL',
    	], $tableOptions);

    	$this->createTable('norm', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'user' => Schema::TYPE_INTEGER,
    			'section' => Schema::TYPE_INTEGER,
    			'complain_category' => Schema::TYPE_INTEGER,
    			'modality' => Schema::TYPE_INTEGER,
    			'action' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'rating' => Schema::TYPE_DOUBLE,
    			'ratingpos' => Schema::TYPE_DOUBLE,
    			'ratingneg' => Schema::TYPE_DOUBLE,
    			'closing' => Schema::TYPE_STRING,
    			'name' => Schema::TYPE_STRING . '(20) NOT NULL',
    			'active' => Schema::TYPE_INTEGER .' DEFAULT 0',
    			'FOREIGN KEY (complain_category) REFERENCES complain_category(id)',
    			'FOREIGN KEY (section) REFERENCES section(id)',
    			'FOREIGN KEY (modality) REFERENCES modality(id)',
    			'FOREIGN KEY (action) REFERENCES action(id)',
    	], $tableOptions);


    	$this->createTable('event', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'user' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'content' => Schema::TYPE_INTEGER,
    			'action' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'complain_category' => Schema::TYPE_INTEGER,
    			'infringedNorm' => Schema::TYPE_INTEGER,
    			'fulfilledNorm' => Schema::TYPE_INTEGER,
    			'checked' => Schema::TYPE_INTEGER,
    			'FOREIGN KEY (user) REFERENCES user(id)',
    			'FOREIGN KEY (content) REFERENCES content(id)',
    			'FOREIGN KEY (action) REFERENCES action(id)',
    			'FOREIGN KEY (complain_category) REFERENCES complain_category(id)',
    			'FOREIGN KEY (infringedNorm) REFERENCES norm(id)',
    			'FOREIGN KEY (fulfilledNorm) REFERENCES norm(id)',
    	], $tableOptions);

    	$this->createTable('argument_type', [
    			'id' => Schema::TYPE_PK,
    			'description' => Schema::TYPE_STRING . '(20) NOT NULL',
    	], $tableOptions);

    	$this->createTable('argument', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'norm' => Schema::TYPE_INTEGER. '(20) NOT NULL',
    			'user' => Schema::TYPE_INTEGER,
    			'type' => Schema::TYPE_INTEGER,
    			'description' => Schema::TYPE_STRING . '(512) NOT NULL',
					'rate' => Schema::TYPE_DOUBLE,
					'numrate' => SCHEMA::TYPE_INTEGER,
					'weight' => SCHEMA::TYPE_DOUBLE,
    			'FOREIGN KEY (norm) REFERENCES norm(id)',
    			'FOREIGN KEY (user) REFERENCES user(id)',
    			'FOREIGN KEY (type) REFERENCES argument_type(id)',
    	], $tableOptions);

    	$this->createTable('argument_rate', [
    			'id_argument' => Schema::TYPE_INTEGER,
    			'id_user' => Schema::TYPE_INTEGER,
    			'rate' => Schema::TYPE_INTEGER,
    			'PRIMARY KEY (id_argument, id_user)',
    			'FOREIGN KEY (id_argument) REFERENCES argument(id)',
    			'FOREIGN KEY (id_user) REFERENCES user(id)',
    	], $tableOptions);

    	$this->createTable('configuration', [
    			'id_experiment' => Schema::TYPE_PK,
    			'description' => Schema::TYPE_STRING . '(100) NOT NULL',
    			'active' => Schema::TYPE_BOOLEAN,
    	], $tableOptions);

    	$this->createTable('log', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'id_experiment' => Schema::TYPE_INTEGER,
    			'event' => Schema::TYPE_INTEGER,
    			'argument' => Schema::TYPE_INTEGER,
    			'argument_rate' => Schema::TYPE_INTEGER,
    			'norm' => Schema::TYPE_INTEGER,
    			'section' => Schema::TYPE_INTEGER,
    			'date' => Schema::TYPE_STRING . '(20) NOT NULL',
    			'FOREIGN KEY (event) REFERENCES event(id)',
    			'FOREIGN KEY (section) REFERENCES section(id)',
    			'FOREIGN KEY (argument) REFERENCES argument(id)',
    			'FOREIGN KEY (norm) REFERENCES norm(id)',
    			'FOREIGN KEY (id_experiment) REFERENCES configuration(id_experiment)',
    	], $tableOptions);
    }

    private function setUsers(){
    	echo "\nInstalling users...\n";
		//Normal users
    	$this->insert('user',['username' => 'normal1', 'auth_key' => 'AcFUMSFjj0r8nQnpTBZ_ojPZ1CN575e6', 'password_hash' => '$2y$13$K1EQ4/yFUt/BC4vuOWbA6eTmQESDpMnt6gTuVH3ngBReYcmJP2sDS', 'password_reset_token' => null, 'email' => 'normal1@normal1.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal2', 'auth_key' => 'ylNEHNwKrlxavswxw3Twn25oGPZoZ22w', 'password_hash' => '$2y$13$xJe0w0oy1IPmZzD52jFjyuAYdlAQ5ZJwlBBD40EoZGPAIWvXasm5W', 'password_reset_token' => null, 'email' => 'normal2@normal2.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal3', 'auth_key' => 'SkbD3UsaiMtXzE4-w0cQmk29OrJzIbq6', 'password_hash' => '$2y$13$IpL5bhI24yI9ly40t0Afeeow6JMmapd4IxR284ikjvLyyG1FtKMtC', 'password_reset_token' => null, 'email' => 'normal3@normal3.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal4', 'auth_key' => 'H2OjCd-HGVXTddp4YdrLW4fTHiTQ7ymd', 'password_hash' => '$2y$13$Vl19gSmLCmxqYB9ASqX8vuq34ZHmvTQnmD77pPnrStyDGtTDP9YlC', 'password_reset_token' => null, 'email' => 'normal4@normal4.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal5', 'auth_key' => 'x-P9-BgstTl7KAo_Nyuqz6fZDi1VR3i8', 'password_hash' => '$2y$13$tIVwtvWjuZsV2WRw9b7UO.ofOlVkqemK3bZFMBjHlmurTRhvN3jBK', 'password_reset_token' => null, 'email' => 'normal5@normal5.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal6', 'auth_key' => '7xUJpU9rcOLYGyldkU9FOBSuca-lipP6', 'password_hash' => '$2y$13$I/sNgAU.QyJH.fRkk08nFu5y/2M7jhCwRMHRUzuqmBY9kW8c0GmLa', 'password_reset_token' => null, 'email' => 'normal6@normal6.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal7', 'auth_key' => 'nTTeI-vdf3eXCS5im_d6KSJFliroXTlF', 'password_hash' => '$2y$13$GvpExcU53vrmw0IO/tUshuC1zDlBimR6y9eI278FsLNo5qSnx6vIG', 'password_reset_token' => null, 'email' => 'normal7@normal7.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal8', 'auth_key' => 'ABh2orYAypg8W7ds3OuXXZ86b1xAr1mz', 'password_hash' => '$2y$13$GhSETI455f7B8JoB56olhuNttJSsPobMoilphz9sj2A2ca155hz.u', 'password_reset_token' => null, 'email' => 'normal8@normal8.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal9', 'auth_key' => '91UiFcd1R2z2wX-cihKGehyNqT1cRX0v', 'password_hash' => '$2y$13$REHEiWRcSej2yigO3Xc8nuL6p2xkXvcD8Pq3tU2lRP3NrXWDSHwpW', 'password_reset_token' => null, 'email' => 'normal9@normal9.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal10', 'auth_key' => 'XtlzrvhjE9BpgZ1geWYK7VednElbehbV', 'password_hash' => '$2y$13$sdImuJd891ZUN/5a52Mrd.XkxQQGcBZALAQ6TfvpjC6G9UbKK.6RK', 'password_reset_token' => null, 'email' => 'normal01@normal10.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

    	//Spammers users
    	$this->insert('user',['username' => 'spam1', 'auth_key' => 'Mr4SCK4uKxTAz6RcKgM9tgxtYHGSFv5e', 'password_hash' => '$2y$13$fBdrTuuneKhzSP/JtxMuUezl/tZ8yAxO3vZZJPruBpoowo2PgdMxq', 'password_reset_token' => null, 'email' => 'spam1@spam1.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam2', 'auth_key' => 'cyWo0Rqh1-BIiXzQrFjWhNAhi79w6REY', 'password_hash' => '$2y$13$F8s22aObEzfB/l5R0SeUneHBzsLLyd5KrwrI/HGMWj9zd2xNtDaxW', 'password_reset_token' => null, 'email' => 'spam2@spam2.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam3', 'auth_key' => 'HZKcaTgbAASD03gMGHNM0pERTU_-0ixB', 'password_hash' => '$2y$13$ireJAhSSLrD0KPkd.Bxx0O1le9Dc8.AgmZkdMS.7EYca1ZJFZdRA2', 'password_reset_token' => null, 'email' => 'spam3@spam3.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam4', 'auth_key' => 'DhsXsfnNLqSSDLRMUeqP0BSXzulQrAXu', 'password_hash' => '$2y$13$ireJAhSSLrD0KPkd.Bxx0O1le9Dc8.AgmZkdMS.7EYca1ZJFZdRA2', 'password_reset_token' => null, 'email' => 'spam4@spam4.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam5', 'auth_key' => 'rAiT_WBTa7nagSWtTaAFT0K-if1Dlt0g', 'password_hash' => '$2y$13$MlqmjAiqUXlJxhFlateQ0uqDmpKE6gQCXmDZHgCI/TG5fYsK43wcC', 'password_reset_token' => null, 'email' => 'spam5@spam5.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam6', 'auth_key' => 'n9pftDB_017wPxFetFzoMb-VKE5ul-S9', 'password_hash' => '$2y$13$amqn1IyaAybX1mQq4x2a0.yo3eY1vu72avuKghpddydxVp8DTbhEi', 'password_reset_token' => null, 'email' => 'spam6@spam6.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam7', 'auth_key' => 'gi9FGS3-a6_ymuojWkQfT6tX1gPOEJwi', 'password_hash' => '$2y$13$H7XYQz5s93EGmq0gbdg06u1jq14Z60i22pP2STnsYmQCVTnT04leO', 'password_reset_token' => null, 'email' => 'spam7@spam7.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam8', 'auth_key' => 'ni_0-aGybhsZXUmbcYxtTpTA0p4ktEas', 'password_hash' => '$2y$13$LqYZG4tINVKUreX7TUWkpu35bOM2.kH2j/kPNqSemWTYYOiUvWAZu', 'password_reset_token' => null, 'email' => 'spam8@spam8.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam9', 'auth_key' => 'NzJRJb8j6gVw2eyyPHrlnzOMQ3vnH8Jz', 'password_hash' => '$2y$13$aR/hg5IskOJt0Csl9HfOJudirDYdftTuI6GTviW1H1BGHOcPHstHi', 'password_reset_token' => null, 'email' => 'spam9@spam9.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spam10', 'auth_key' => 'dKxcOs65TMC6FeBIe8LiTkZjjUjzMBuD', 'password_hash' => '$2y$13$nSjm/gerRsWH.CuR6ZCIvuSPNuS4wF/CwY3DRK4cwfo6vbepR1wN6', 'password_reset_token' => null, 'email' => 'spam10@spam10.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    }

    public function importMinimumData(){
    	echo "\nImporting data...\n";

     	//Section
    	$this->insert('section',['description' => 'Reporter']);
    	$this->insert('section',['description' => 'Forum']);
    	$this->insert('section',['description' => 'Image & Video']);
    	$this->insert('section',['description' => 'Reporter, Forum or Image/Video']);

    	//Content Category
    	$this->insert('content_category',['description' => 'Spam']);
//     	$this->insert('content_category',['description' => 'Porn']);
//     	$this->insert('content_category',['description' => 'Violent']);
//     	$this->insert('content_category',['description' => 'Insult']);
//     	$this->insert('content_category',['description' => 'Wrong Placement']);
    	$this->insert('content_category',['description' => 'OK']);
//     	$this->insert('content_category',['description' => 'All']);

    	//Complain Category
    	$this->insert('complain_category',['description' => 'Spam']);
//     	$this->insert('complain_category',['description' => 'Porn']);
//     	$this->insert('complain_category',['description' => 'Violent']);
//     	$this->insert('complain_category',['description' => 'Insult']);
//     	$this->insert('complain_category',['description' => 'Wrong Placement']);
//     	$this->insert('complain_category',['description' => 'OK']);
//     	$this->insert('complain_category',['description' => 'All']);

    	//Modality
    	$this->insert('modality',['description' => 'Obligation']);
    	$this->insert('modality',['description' => 'Phohibition']);

    	//Actions
    	$this->insert('action',['description' => 'upload']);
    	$this->insert('action',['description' => 'view content']);
    	$this->insert('action',['description' => 'complain']);
    	$this->insert('action',['description' => 'notUpload']);
    	$this->insert('action',['description' => 'view section']);
    	$this->insert('action',['description' => 'create norm']);
    	$this->insert('action',['description' => 'discuss norm']);
    	$this->insert('action',['description' => 'add argument']);
    	$this->insert('action',['description' => 'rate argument']);
    	$this->insert('action',['description' => 'active norm']);


    	//Argument type
    	$this->insert('argument_type',['id' => 1, 'description' => 'positive']);
    	$this->insert('argument_type',['id' => 2,'description' => 'negative']);

    	//Roles
    	$this->insert('role',['id' => 1, 'description' => 'Demo']);
    	$this->insert('role',['id' => 2, 'description' => 'Spammer']);
    	$this->insert('role',['id' => 3, 'description' => 'Ok']);
    }

    private function setDemo(){
    	echo "\nSetting demo...\n";

//     	//Identificate the experiment and setting demo user
     	$this->insert('configuration',['id_experiment' => 1, 'description' => 'Demo', 'active' => true]);

//     	//$this->insert('user',['username' => 'tester', 'auth_key' => 'l97C8SZjgKbujdNQMvnHrbqG4WV7rL1I', 'password_hash' => '$2y$13$Lkhb1U0e2sq5YN4RRycnSOYR.8O9Gh3M8R8m5Zu8nX8q8eFZcpsnq', 'password_reset_token' => null, 'email' => 'tester@guttmann.com', 'role' => '10', 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
//     	$this->insert('user',['username' => 'demo', 'auth_key' => 'TEgxPPnZQVMA0RtDLkvniNpzdlGUry8t', 'password_hash' => '$2y$13$CbMxMW1kw05PbwRMceFade/R.lwbBHnDarGHjKaCSTnvQn9Xt8bHy', 'password_reset_token' => null, 'email' => 'demo@demo.com', 'role' => 1, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'maite', 'auth_key' => 'tdhNfNTiNHNUbk6y1Vt6Wx92HZpoKlqd', 'password_hash' => '$2y$13$FJ17CdzDJvxuNMAa/12AI.Tto9/en4h7OJLcfhfeUZOZ1F5qduGuq', 'password_reset_token' => null, 'email' => 'maite@norm.crowdsourcing.com', 'role' => 1, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'david', 'auth_key' => '1XC4w545GCpajfBV8931bh1ozh_7l4a9', 'password_hash' => '$2y$13$Z9klNcQPVy8.6iTNeY3x.OLzo.sPVxtWivCnUEGjwtOYM7jDGfASS', 'password_reset_token' => null, 'email' => 'david@norm.crowdsourcing.com', 'role' => 1, 'status' => '10', 'created_at' => '1423586411', 'updated_at' => '1423586411']);

    	//Content
//     	$this->insert('content',['owner' => 1, 'section' => 1, 'category' => 6,  'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => 'OK', 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 1, 'category' => 1,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => "I haven't seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 1, 'category' => 1, 'title' => 'Can Cristiano Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => 'SPAM', 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 6,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => 'OK', 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 1,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => 'SPAM', 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 3, 'category' => 6,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => 'images\content\funny1.jpg', 'message' => 'OK', 'num_of_views' => 0, 'num_of_complaints'=> 0]);
//     	$this->insert('content',['section' => 3, 'category' => 1,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => 'images\content\funny2.jpg', 'message' => 'SPAM', 'num_of_views' => 0, 'num_of_complaints' => 0]);

//     	//SPAM
//     	$this->insert('content',['owner' => 1, 'section' => 1, 'category' => 1,'title' => 'Look inside this message!', 'type' => 'Text', 'url' => '', 'message' => "You have been cursed, the amazing King Butterfly will come after you and suck the nectar of the flowers of your garden, this is not any kind of innuendo. In case you do not want this horrific scene to happen you have to post this message at 10 other forums.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 1, 'category' => 1,'title' => 'Free passes for the next Championsleague competition', 'type' => 'Text', 'url' => '', 'message' => "Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can't sell this tickets I will willingly give these to the 5 person that visits my 'totally not a rip off' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 1,'title' => "Aberdeen Season Tickets up 35%", 'type' => 'Text', 'url' => '', 'message' => "Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being only 17 miles south of Dundee. Gerritrighupyez", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 1,'title' => "SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS", 'type' => 'Text', 'url' => '', 'message' => "Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 3, 'category' => 1,'title' => "Do you want to stay at home, do barely nothing and get PAID?!", 'type' => 'Text', 'url' => 'images\content\spam.jpg', 'message' => "I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 3, 'category' => 1,'title' => 'Free soccer t-shirts', 'type' => 'Text', 'url' => 'images\content\tshirt1.jpg', 'message' => 'Obtain a lot of variety of soccer t-shirts free. You only need to add your credit number and the t-shirts will be you!', 'num_of_views' => 0, 'num_of_complaints' => 0]);

//     	//OK
//     	$this->insert('content',['section' => 1, 'category' => 6,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => "I haven't seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 1, 'category' => 6,'title' => 'Sven In China', 'type' => 'Text', 'url' => '', 'message' => "Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 6,'title' => "Why Do Scottish, Welsh And Irish Leagues All Suck?", 'type' => 'Text', 'url' => '', 'message' => "I watched Celtic's champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can't help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that's why their champions had to play a lot qualifier games to get to the group stage. And what is more interesting to me is that Welsh, Irish and Northen Irish leagues are all near the bottom in the UEFA ranking. I believe football is almost as popular in all these areas as in England. I don't expect their football leagues to be as good as the Premier League due to demographic and economic reasons. But, at least Scotland can have better, or at least as good football teams than those in Belgium, Austria, and Cyprus. And the others on the British Irelands can certainly do a lot more better than Liechtenstein, Malta and Luxembourg.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 2, 'category' => 6,'title' => "Your sport is SOCCER, learn the difference", 'type' => 'Text', 'url' => '', 'message' => "Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 3, 'category' => 6,'title' => "Tutorial: How to do the Ronaldo trick", 'type' => 'Text', 'url' => 'images\content\ronald', 'message' => "", 'num_of_views' => 0, 'num_of_complaints' => 0]);
//     	$this->insert('content',['section' => 3, 'category' => 6,'title' => 'Amazing goal!', 'type' => 'Text', 'url' => 'images\content\golazo', 'message' => "", 'num_of_views' => 0, 'num_of_complaints' => 0]);
    }


    private function setExperiment1(){
		//Normal users
    	$this->insert('user',['username' => 'normal10', 'auth_key' => 'XtlzrvhjE9BpgZ1geWYK7VednElbehbV', 'password_hash' => '$2y$13$sdImuJd891ZUN/5a52Mrd.XkxQQGcBZALAQ6TfvpjC6G9UbKK.6RK', 'password_reset_token' => null, 'email' => 'normal1@normal1.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal11', 'auth_key' => 'hqrJ0bLNnPGfRKRfBPnlTRNohTL7p-Ar', 'password_hash' => '$2y$13$z1znCNjq.pgLt3ACq4EPQ.0MyFrabTxiAy1KILSfl.BNm4NmnnGsO', 'password_reset_token' => null, 'email' => 'normal2@normal2.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal12', 'auth_key' => 'SkbD3UsaiMtXzE4-w0cQmk29OrJzIbq6', 'password_hash' => '$2y$13$IpL5bhI24yI9ly40t0Afeeow6JMmapd4IxR284ikjvLyyG1FtKMtC', 'password_reset_token' => null, 'email' => 'normal3@normal3.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal13', 'auth_key' => 'H2OjCd-HGVXTddp4YdrLW4fTHiTQ7ymd', 'password_hash' => '$2y$13$Vl19gSmLCmxqYB9ASqX8vuq34ZHmvTQnmD77pPnrStyDGtTDP9YlC', 'password_reset_token' => null, 'email' => 'normal4@normal4.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal14', 'auth_key' => 'C132-N42AEmS0keeFdEOEQC5byEi8mB4', 'password_hash' => '$2y$13$0Sc0PPCHNZuwtdTNcyCASOiNEACSQFDA4cgsvrpYoz.dsugeVpl0u', 'password_reset_token' => null, 'email' => 'normal5@normal5.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal15', 'auth_key' => 'gYa5niuHr7GjKZkw9RMRUwIuTGJ471K4', 'password_hash' => '$2y$13$OgFl5H1hlWqWtUerGx3thOVI5sH.Qgh4F2LqjUeljDpH387OCVsOO', 'password_reset_token' => null, 'email' => 'normal6@normal6.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal16', 'auth_key' => 'y9BU1-tE8w1LRdxJ2owjc0TgpPvn7cPU', 'password_hash' => '$2y$13$eqLVT2B9rby5eO3Y.l1qwORdtXGZbS1Tkihf5g3/fACrXqZkOeTLO', 'password_reset_token' => null, 'email' => 'normal7@normal7.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal17', 'auth_key' => 'lgXQGOkkhV9xGU0OLrY6xeNd0X5_Z-Dk', 'password_hash' => '$2y$13$1/9rH2dXQTC/APXuPSuv7u.snOUI2VqaWTcdanzL8XOAspSHxpFnS', 'password_reset_token' => null, 'email' => 'normal8@normal8.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal18', 'auth_key' => 'buvxB_DoykjVWFoKLYxArrF41dSMk-yZ', 'password_hash' => '$2y$13$JiTDm7TCLp7CGj56aTCArus4VURrWgrIUAhIwkDaHSEk6JftyMyeO', 'password_reset_token' => null, 'email' => 'normal9@normal9.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal19', 'auth_key' => '0qydqJ1Kb74kktznFaxvdlsTRAD1DaN1', 'password_hash' => '$2y$13$Aw4WWT6YECSfs0.pR0pLOOavOCWUTYqW.0qNf/fDCWYhyAGqTPX.W', 'password_reset_token' => null, 'email' => 'normal01@normal10.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

    	//spamers users
    	$this->insert('user',['username' => 'spamer10', 'auth_key' => 'dKxcOs65TMC6FeBIe8LiTkZjjUjzMBuD', 'password_hash' => '$2y$13$nSjm/gerRsWH.CuR6ZCIvuSPNuS4wF/CwY3DRK4cwfo6vbepR1wN6', 'password_reset_token' => null, 'email' => 'spam1@spam1.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer11', 'auth_key' => 'UACGvGXrTgAQKTvSECSAQWqfPI333nhv', 'password_hash' => '$2y$13$fuCiTYgqJsCJoFYJZJ85Lu65ReNGi0JJuBltIhw7Pz6yp3FiEU9iS', 'password_reset_token' => null, 'email' => 'spamermer2@spamer2.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer12', 'auth_key' => '8gdSujmwfFsSqlJkemt6arT3mJSLsS23', 'password_hash' => '$2y$13$y0hZdK2fgSSUdskcuqmTtuyfkUxiqFXDSXQELdDOCkETohHBjcBXO', 'password_reset_token' => null, 'email' => 'spamer3@spamer3.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer13', 'auth_key' => 'r3wC4B_wLpnJaqFdR4o_iuxY8blUitXk', 'password_hash' => '$2y$13$UEb1DZdg959cXqqHRVqlIufz.Niuzt9Trcnia1TwuQajVam.8lJMi', 'password_reset_token' => null, 'email' => 'spamer4@spamer4.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer14', 'auth_key' => 'Uhz2sVdfGmkfdg0NaoMLj4eJ9loo1ckK', 'password_hash' => '$2y$13$2MN16jH9doK7aoxXHfhzPebw9btf4Ny9uDRQRtg.RnUHbL8SjKu.G', 'password_reset_token' => null, 'email' => 'spamer5@spamer5.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer15', 'auth_key' => 'MmTAnG8oEztFrPDsrVYP_0Zo-2_F2Gfh', 'password_hash' => '$2y$13$TA.oY7ZV6s56rfiGWoz5vOzmW1w7FlWoLpxHlNV06WjGg0lL3sbxq', 'password_reset_token' => null, 'email' => 'spamer6@spamer6.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer16', 'auth_key' => '4FrC4fBTigjf4FQOH6vsDUakqi7y9XSC', 'password_hash' => '$2y$13$tz/LbKifgxOy3HoRvcfiTeuRcRyXxS8K2ENDMoCm5oA0XyV9MZt2O', 'password_reset_token' => null, 'email' => 'spamer7@spamer7.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer17', 'auth_key' => 'wnW2MZAFscNM_L-McDgIS5fMsBF_KwKQ', 'password_hash' => '$2y$13$Kvvuu0ESNC9skCEtWgKsReRkB/M69ebemQ0p4b/tU5qswfLTLLsZa', 'password_reset_token' => null, 'email' => 'spamer8@spamer8.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer18', 'auth_key' => 'x3laoPusAShoTpECN45PxBwZXu9p4k34', 'password_hash' => '$2y$13$bkNOyPEwkjdXz.h/guXHsuhYVdQdvrWKg9ojcJLXWWmgfyYCo4vku', 'password_reset_token' => null, 'email' => 'spamer9@spamer9.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer19', 'auth_key' => 'YgrrU0_fbm7KZNRjS4vh2L0aAAiJuxya', 'password_hash' => '$2y$13$M0Bpb7Lkt9dYcSWpOk/Uq.EVqHpHBWr6OCVI0GuBmYiffjAbMi./q', 'password_reset_token' => null, 'email' => 'spamer10@spamer10.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	//Identificate the experiment
    	$this->insert('configuration',['id_experiment' => 2, 'description' => '30% Spamer--70% Normal', 'active' => true]);
    }

    private function setExperiment2(){
    	//Normal users
    	$this->insert('user',['username' => 'normal20', 'auth_key' => 'ANz3tbRncr1SvcfRXGZHaz8oFpVOA4zv', 'password_hash' => '$2y$13$ePl.TG/DIvhc8Igx/9VAzuyyu3bjEIsri5XmEpb0wZvNabs8pnYDK', 'password_reset_token' => null, 'email' => 'normal1@normal1.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal21', 'auth_key' => 'lS3dyYYuS4Q_6zVuiZp7rYMTZ7DMK9pE', 'password_hash' => '$2y$13$NWfCEfkPjEA4yxmRycnipe10mPHPM0bIt9p7m.rBa.T0MKsTcAF6q', 'password_reset_token' => null, 'email' => 'normal2@normal2.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal22', 'auth_key' => '594ZVtj4hyxLLOyCiijBG4IXr9S8NoHC', 'password_hash' => '$2y$13$Rh4PeBogl8ApvVVpflWUyeUC94gNyiythhsyYcFZmPmqkemxN0FVi', 'password_reset_token' => null, 'email' => 'normal3@normal3.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal23', 'auth_key' => '8tz7GglRqSa7iylOBuFSHj77kwzMz4js', 'password_hash' => '$2y$13$D09dtHlmuPFcelkWdQoOSe71gbLJPWsjVU2q9a6AapAGOJGybKqw6', 'password_reset_token' => null, 'email' => 'normal4@normal4.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal24', 'auth_key' => 'CiJjKvIOT_ObOaz2z53eIM-zLdrxYUr1', 'password_hash' => '$2y$13$Ly2.CiGAfBsm02/9AdzDK.PIiWiRR53uZGqGQqlzlOGyZGQUc98i2', 'password_reset_token' => null, 'email' => 'normal5@normal5.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal25', 'auth_key' => 'HJPesPdEjqioSjstkPjC1kNwR14JiCn1', 'password_hash' => '$2y$13$5YXjq8FAKm1uGXBEQsJ2z.DtxQGvKmdFQqBYw6BdrF2cX5HBVqV0K', 'password_reset_token' => null, 'email' => 'normal6@normal6.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal26', 'auth_key' => 'mOWPbexnoLlUg-N-MzapZYdf9ZoJ9ccH', 'password_hash' => '$2y$13$5jQep2skEciqz7jH9cY48.LGaai0xSdLejEQqX/nml3EhgE1oMIDC', 'password_reset_token' => null, 'email' => 'normal7@normal7.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal27', 'auth_key' => '_dPtl_vmnl_KxgPvZt0goKY2kuFpvYRz', 'password_hash' => '$2y$13$ARx8/UwX4zYC71w3wZ56huUeH1QJKs1sztElp7DCOQHXlKpmcHF7O', 'password_reset_token' => null, 'email' => 'normal8@normal8.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal28', 'auth_key' => 'HHAnr44B25tDx6WvnuRIwJd9VYrWJHVz', 'password_hash' => '$2y$13$7FZDJozZk4GYc/4zc.ltmOnlZ40kz4r7DxGv2K8pcdBAvS4vL1Kk6', 'password_reset_token' => null, 'email' => 'normal9@normal9.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'normal29', 'auth_key' => 'OWyuCnabyvKCutk-II0VaL9ow3rqnHsm', 'password_hash' => '$2y$13$LVfzgZwzCgp1PUIQSUaYFOhKsi1LHF400j95Pkw0SRgkESsHgWQN6', 'password_reset_token' => null, 'email' => 'normal01@normal10.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

    	//spamermers users
    	$this->insert('user',['username' => 'spamer20', 'auth_key' => 'wwfnrQyTQBZKkCXNQW3yaY5VRtDJ2OOo', 'password_hash' => '$2y$13$VFhktucBT3pve1I4/owGAOZCDC0Z0zJUx7Rtw.9EyWyolS3XreHKC', 'password_reset_token' => null, 'email' => 'spamer1@spamer1.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer21', 'auth_key' => 'mDGJAqclGztPyArQ-fYwDoZDLgJbepWm', 'password_hash' => '$2y$13$6B9uxGkdIdX4eBKKwQv8tuBv73dOxkaRop/lQu/kicw6ivhi3st2u', 'password_reset_token' => null, 'email' => 'spamer2@spamer2.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer22', 'auth_key' => 'j2ERZhCtvCYcYEPaM5Q1ClAgQwUP_Zwf', 'password_hash' => '$2y$13$ireJAhSSLrD0KPkd.Bxx0O1le9Dc8.AgmZkdMS.7EYca1ZJFZdRA2', 'password_reset_token' => null, 'email' => 'spamer3@spamer3.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer23', 'auth_key' => 'DhsXsfnNLqSSDLRMUeqP0BSXzulQrAXu', 'password_hash' => '$2y$13$yYaLPWmeWfk8Bc5bCwjn0ebrOELDyDvCyAVXlA/1SsHWWellwH.am', 'password_reset_token' => null, 'email' => 'spamer4@spamer4.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer24', 'auth_key' => 'T4J4tiRQwwr6W3lssUAXl7IQpl9ir2oG', 'password_hash' => '$2y$13$OUTRZpdccGQhVPkyfR9yje9YY9ICMPfpUmR8zAfPeLEFsksE0VtA.', 'password_reset_token' => null, 'email' => 'spamer5@spamer5.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer25', 'auth_key' => '_shfDk3oVYqnUAOmSZ1JLEuqtz-Yr8Px', 'password_hash' => '$2y$13$emltVdArL4EbYav1DOz0eOCFVgWlj8H7G3I8a1Blu3YZQl3ALHJVu', 'password_reset_token' => null, 'email' => 'spamer6@spamer6.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer26', 'auth_key' => 'TtgvDKhpcTgvK3r9u-MgAgeB5CUfstbU', 'password_hash' => '$2y$13$wqk7uXNQC4jgkFwPYjWMh.REO/m8D9CHMWPZugHZ64Nsl24mZFNw6', 'password_reset_token' => null, 'email' => 'spamer7@spamer7.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer27', 'auth_key' => 'Rq3sbVP5XiGRsnS_OWQJmNUCqPRWU6-u', 'password_hash' => '$2y$13$aSwj.P1KatBnQ6ExrTBjdevxb0fX34lpWuCC4ZBVHxM9cJTDKWwva', 'password_reset_token' => null, 'email' => 'spamer8@spamer8.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer28', 'auth_key' => '4usXbjHDnNHtQ9bRi_bTHsQH3XOLy6RU', 'password_hash' => '$2y$13$PX7gNRSMuuAjpJJCvpSluu2X3G7RQTJyP8P2ZyDv0.cjBV9XaG3d2', 'password_reset_token' => null, 'email' => 'spamer9@spamer9.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'spamer29', 'auth_key' => '0Gzov3RY_0bUVnouIASZYTqiCDfyZPTa', 'password_hash' => '$2y$13$78598eJMyglX0YOJtyNf0uer/eMPI8OgzwWSlh3UFz9mEdW9UXnba', 'password_reset_token' => null, 'email' => 'spamer10@spamer10.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

    	//Identificate the experiment
    	$this->insert('configuration',['id_experiment' => 3, 'description' => '70% Spamer--30% Normal', 'active' => true]);
   	}
    private function setExperiment3(){
      //Normal users
      $this->insert('user',['username' => 'normal10', 'auth_key' => 'XtlzrvhjE9BpgZ1geWYK7VednElbehbV', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal1@normal1.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal11', 'auth_key' => 'hqrJ0bLNnPGfRKRfBPnlTRNohTL7p-Ar', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal2@normal2.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal12', 'auth_key' => 'SkbD3UsaiMtXzE4-w0cQmk29OrJzIbq6', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal3@normal3.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal13', 'auth_key' => 'H2OjCd-HGVXTddp4YdrLW4fTHiTQ7ymd', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal4@normal4.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal14', 'auth_key' => 'C132-N42AEmS0keeFdEOEQC5byEi8mB4', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal5@normal5.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal15', 'auth_key' => 'gYa5niuHr7GjKZkw9RMRUwIuTGJ471K4', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal6@normal6.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal16', 'auth_key' => 'y9BU1-tE8w1LRdxJ2owjc0TgpPvn7cPU', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal7@normal7.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal17', 'auth_key' => 'lgXQGOkkhV9xGU0OLrY6xeNd0X5_Z-Dk', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal8@normal8.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal18', 'auth_key' => 'buvxB_DoykjVWFoKLYxArrF41dSMk-yZ', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal9@normal9.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal19', 'auth_key' => '0qydqJ1Kb74kktznFaxvdlsTRAD1DaN1', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal01@normal10.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal20', 'auth_key' => 'ANz3tbRncr1SvcfRXGZHaz8oFpVOA4zv', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal1@normal1.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal21', 'auth_key' => 'lS3dyYYuS4Q_6zVuiZp7rYMTZ7DMK9pE', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal2@normal2.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal22', 'auth_key' => '594ZVtj4hyxLLOyCiijBG4IXr9S8NoHC', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal3@normal3.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal23', 'auth_key' => '8tz7GglRqSa7iylOBuFSHj77kwzMz4js', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal4@normal4.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal24', 'auth_key' => 'CiJjKvIOT_ObOaz2z53eIM-zLdrxYUr1', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal5@normal5.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal25', 'auth_key' => 'HJPesPdEjqioSjstkPjC1kNwR14JiCn1', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal6@normal6.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal26', 'auth_key' => 'mOWPbexnoLlUg-N-MzapZYdf9ZoJ9ccH', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal7@normal7.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal27', 'auth_key' => '_dPtl_vmnl_KxgPvZt0goKY2kuFpvYRz', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal8@normal8.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal28', 'auth_key' => 'HHAnr44B25tDx6WvnuRIwJd9VYrWJHVz', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal9@normal9.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'normal29', 'auth_key' => 'OWyuCnabyvKCutk-II0VaL9ow3rqnHsm', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'normal01@normal10.com', 'role' => 2, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

      //spamers users
      $this->insert('user',['username' => 'spammer10', 'auth_key' => 'dKxcOs65TMC6FeBIe8LiTkZjjUjzMBuD', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spam1@spam1.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer11', 'auth_key' => 'UACGvGXrTgAQKTvSECSAQWqfPI333nhv', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamermer2@spamer2.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer12', 'auth_key' => '8gdSujmwfFsSqlJkemt6arT3mJSLsS23', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer3@spamer3.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer13', 'auth_key' => 'r3wC4B_wLpnJaqFdR4o_iuxY8blUitXk', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer4@spamer4.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer14', 'auth_key' => 'Uhz2sVdfGmkfdg0NaoMLj4eJ9loo1ckK', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer5@spamer5.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer15', 'auth_key' => 'MmTAnG8oEztFrPDsrVYP_0Zo-2_F2Gfh', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer6@spamer6.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer16', 'auth_key' => '4FrC4fBTigjf4FQOH6vsDUakqi7y9XSC', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer7@spamer7.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer17', 'auth_key' => 'wnW2MZAFscNM_L-McDgIS5fMsBF_KwKQ', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer8@spamer8.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer18', 'auth_key' => 'x3laoPusAShoTpECN45PxBwZXu9p4k34', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer9@spamer9.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer19', 'auth_key' => 'YgrrU0_fbm7KZNRjS4vh2L0aAAiJuxya', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer10@spamer10.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer20', 'auth_key' => 'wwfnrQyTQBZKkCXNQW3yaY5VRtDJ2OOo', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer1@spamer1.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer21', 'auth_key' => 'mDGJAqclGztPyArQ-fYwDoZDLgJbepWm', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer2@spamer2.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer22', 'auth_key' => 'j2ERZhCtvCYcYEPaM5Q1ClAgQwUP_Zwf', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer3@spamer3.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer23', 'auth_key' => 'DhsXsfnNLqSSDLRMUeqP0BSXzulQrAXu', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer4@spamer4.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer24', 'auth_key' => 'T4J4tiRQwwr6W3lssUAXl7IQpl9ir2oG', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer5@spamer5.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer25', 'auth_key' => '_shfDk3oVYqnUAOmSZ1JLEuqtz-Yr8Px', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer6@spamer6.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer26', 'auth_key' => 'TtgvDKhpcTgvK3r9u-MgAgeB5CUfstbU', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer7@spamer7.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer27', 'auth_key' => 'Rq3sbVP5XiGRsnS_OWQJmNUCqPRWU6-u', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer8@spamer8.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer28', 'auth_key' => '4usXbjHDnNHtQ9bRi_bTHsQH3XOLy6RU', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer9@spamer9.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
      $this->insert('user',['username' => 'spammer29', 'auth_key' => '0Gzov3RY_0bUVnouIASZYTqiCDfyZPTa', 'password_hash' => Yii::$app->security->generatePasswordHash('sN2016'), 'password_reset_token' => null, 'email' => 'spamer10@spamer10.com', 'role' => 3, 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);

      //Identificate the experiment
      $this->insert('configuration',['id_experiment' => 4, 'description' => 'disscussion test', 'active' => true]);
    }

   	private function setContent(){
   		//SPAM
   		$this->insert('content',['section' => 1, 'category' => 1,'title' => 'Free passes for the next Championsleague competition', 'type' => 'Text', 'url' => '', 'message' => "Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can't sell this tickets I will willingly give these to the 5 person that visits my 'totally not a rip off' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 1,'title' => 'Free tickets for the next Realmadrid match', 'type' => 'Text', 'url' => '', 'message' => "Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can't sell this tickets I will willingly give these to the 5 person that visits my 'totally not a rip off' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 1,'title' => 'The real ball of FBC signed by Messi', 'type' => 'Text', 'url' => '', 'message' => "Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can't sell this tickets I will willingly give these to the 5 person that visits my 'totally not a rip off' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 1,'title' => 'The signed t-shirt by Cristiano ronaldo', 'type' => 'Text', 'url' => '', 'message' => "Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can't sell this tickets I will willingly give these to the 5 person that visits my 'totally not a rip off' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!", 'num_of_views' => 0, 'num_of_complaints' => 0]);

   		$this->insert('content',['section' => 2, 'category' => 1,'title' => "Aberdeen Season Tickets up 35%", 'type' => 'Text', 'url' => '', 'message' => "Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being only 17 miles south of Dundee. Gerritrighupyez", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 1,'title' => "SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS", 'type' => 'Text', 'url' => '', 'message' => "Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 1,'title' => "SELLING OF AUTOGRAPH OF MESSI", 'type' => 'Text', 'url' => '', 'message' => "Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 1,'title' => "Selling my Renault Clio", 'type' => 'Text', 'url' => '', 'message' => "Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 1,'title' => "Visit my page a buy something", 'type' => 'Text', 'url' => '', 'message' => "Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net", 'num_of_views' => 0, 'num_of_complaints' => 0]);

   		$this->insert('content',['section' => 3, 'category' => 1,'title' => "Spam", 'type' => 'Image', 'url' => 'images\content\spam.jpg', 'message' => "I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 3, 'category' => 1,'title' => "Premier T-shirts very cheaper!", 'type' => 'Image', 'url' => 'images\content\tshirt1.jpg', 'message' => "I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 3, 'category' => 1,'title' => "Catalona T-shirt 2euros", 'type' => 'Image', 'url' => 'images\content\tshirt2.jpg', 'message' => "I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com", 'num_of_views' => 0, 'num_of_complaints' => 0]);

   		//OK
   		$this->insert('content',['section' => 1, 'category' => 2,'title' => 'Can Neymar Become The Greatest Player In The World?', 'type' => 'Text', 'url' => '', 'message' => "I haven't seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 2,'title' => 'Sven In China', 'type' => 'Text', 'url' => '', 'message' => "Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 2,'title' => 'How many golden ball Messi will obtained?', 'type' => 'Text', 'url' => '', 'message' => "Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 2,'title' => 'Who is the most football player from Japan?', 'type' => 'Text', 'url' => '', 'message' => "Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 1, 'category' => 2,'title' => 'Football in Brasil', 'type' => 'Text', 'url' => '', 'message' => "Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.", 'num_of_views' => 0, 'num_of_complaints' => 0]);

   		$this->insert('content',['section' => 2, 'category' => 2,'title' => "What is the most spectacular goal?", 'type' => 'Text', 'url' => '', 'message' => "I watched Celtic's champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can't help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that's why their champions had to play a lot qualifier games to get to the group stage. And what is more interesting to me is that Welsh, Irish and Northen Irish leagues are all near the bottom in the UEFA ranking. I believe football is almost as popular in all these areas as in England. I don't expect their football leagues to be as good as the Premier League due to demographic and economic reasons. But, at least Scotland can have better, or at least as good football teams than those in Belgium, Austria, and Cyprus. And the others on the British Irelands can certainly do a lot more better than Liechtenstein, Malta and Luxembourg.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 2,'title' => "How to improve your technique", 'type' => 'Text', 'url' => '', 'message' => "I watched Celtic's champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can't help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that's why their champions had to play a lot qualifier games to get to the group stage. And what is more interesting to me is that Welsh, Irish and Northen Irish leagues are all near the bottom in the UEFA ranking. I believe football is almost as popular in all these areas as in England. I don't expect their football leagues to be as good as the Premier League due to demographic and economic reasons. But, at least Scotland can have better, or at least as good football teams than those in Belgium, Austria, and Cyprus. And the others on the British Irelands can certainly do a lot more better than Liechtenstein, Malta and Luxembourg.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 2,'title' => "How to make ronaldinho tricks", 'type' => 'Text', 'url' => '', 'message' => "I watched Celtic's champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can't help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that's why their champions had to play a lot qualifier games to get to the group stage. And what is more interesting to me is that Welsh, Irish and Northen Irish leagues are all near the bottom in the UEFA ranking. I believe football is almost as popular in all these areas as in England. I don't expect their football leagues to be as good as the Premier League due to demographic and economic reasons. But, at least Scotland can have better, or at least as good football teams than those in Belgium, Austria, and Cyprus. And the others on the British Irelands can certainly do a lot more better than Liechtenstein, Malta and Luxembourg.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 2,'title' => "Is Cristano better player rather than Messi", 'type' => 'Text', 'url' => '', 'message' => "I watched Celtic's champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can't help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that's why their champions had to play a lot qualifier games to get to the group stage. And what is more interesting to me is that Welsh, Irish and Northen Irish leagues are all near the bottom in the UEFA ranking. I believe football is almost as popular in all these areas as in England. I don't expect their football leagues to be as good as the Premier League due to demographic and economic reasons. But, at least Scotland can have better, or at least as good football teams than those in Belgium, Austria, and Cyprus. And the others on the British Irelands can certainly do a lot more better than Liechtenstein, Malta and Luxembourg.", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 2, 'category' => 2,'title' => "Your sport is SOCCER, learn the difference", 'type' => 'Text', 'url' => '', 'message' => "Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!", 'num_of_views' => 0, 'num_of_complaints' => 0]);

   		$this->insert('content',['section' => 3, 'category' => 2,'title' => "Tutorial: How to do the Ronaldo trick", 'type' => 'Image', 'url' => 'images\content\funny1.jpg', 'message' => "", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   		$this->insert('content',['section' => 3, 'category' => 2,'title' => "Tutorial: How to do the Cristiano trick", 'type' => 'Video', 'url' => 'images\content\funny2.jpg', 'message' => "", 'num_of_views' => 0, 'num_of_complaints' => 0]);
   	}
}
