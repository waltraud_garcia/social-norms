<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use yii\db\Connection;
use common\modules\iguttmann\modules\scale\models\Scale;


/**
 * Initial migration that builds the whole database
 * @author dsanchez
 *
 */
class m130524_201442_init extends Migration{

	
	/**
	 * (non-PHPdoc)
	 * @see \yii\db\Migration::up()
	 */
	public function up(){
		//Create the infrastructure
		$this->createTables();
    	
    	//Users
    	$this->setUsers();
    	
    	//Language
        $this->setLanguageData();
        
        //Save the Squeleton of the query
        $this->setScaleSqueleton();
        //$this->fimSqueleton();
        
        //Data relationed with the ICF
        $this->setCoresetType();
        $this->setCoreset();
        $this->setICFData();
        
    	$this->setScale();
    	$this->setCoresetIcfScale();
    }

    private function createDatabase(){
    	$this->createDatabase('cloudrehab');
    }
    
    /**
     * Method to manage the creation of the tables
     */
    private function createTables(){
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	echo "\nCreating tables...\n";
    	
    	$this->createTable('patient', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'name' => Schema::TYPE_STRING . '(255)',
    			'firstsurname' => Schema::TYPE_STRING . '(255)',
    			'secondsurname' => Schema::TYPE_STRING . '(255)',
    			'etiology' => Schema::TYPE_STRING . '(255)',
    			'c_historia' => Schema::TYPE_INTEGER . ' NOT NULL',
    	]);
    	
    	$this->createTable('evaluation', [
    			'id' => Schema::TYPE_PK,
    			'indate' => Schema::TYPE_STRING . '(255)',
    			'outdate' => Schema::TYPE_STRING . '(255)',
    			'id_patient' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'prestation'  => Schema::TYPE_INTEGER . ' NOT NULL',
    			'fiProces'  => Schema::TYPE_STRING. '(1)',
    			'reason' =>  Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (id_patient) REFERENCES patient(id)',
    	]);

    	$this->createTable('scalegroup', [
    			'id' => Schema::TYPE_PK,
    			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
    	]);
    	 
    	$this->createTable('scale', [
    			'id' => Schema::TYPE_PK,
    			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'acronym' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'active' => 'tinyint' . ' NOT NULL',
    			'id_scalegroup' => Schema::TYPE_INTEGER,
    			'FOREIGN KEY (id_scalegroup) REFERENCES scalegroup(id)',
    	]);

    	$this->createTable('adminscale', [
    			'id' => Schema::TYPE_INTEGER .' AUTO_INCREMENT',
    			'id_scale' => Schema::TYPE_INTEGER. ' NOT NULL',
    			'id_evaluation' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'date' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'type' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'modifiedType' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'canceled' => Schema::TYPE_STRING . '(1) NOT NULL',
    			'FOREIGN KEY (id_scale) REFERENCES scale(id)',
    			'FOREIGN KEY (id_evaluation) REFERENCES evaluation(id)',
    			'PRIMARY KEY (id)',
    	]);
    	
    	$this->createTable('icfitem', [
    			'id' => Schema::TYPE_STRING . '(10) NOT NULL',
    			'description' => Schema::TYPE_STRING . '(400) NOT NULL',
    			'PRIMARY KEY (id)'
    	]);
    	
    	$this->createTable('coresettype', [
    			'id' => Schema::TYPE_PK.' AUTO_INCREMENT',
    			'type' => Schema::TYPE_STRING . '(255) NOT NULL',
    	]);
    	
    	$this->createTable('coreset', [
    			'id' => Schema::TYPE_PK,
    			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'id_coresettype' =>Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (id_coresettype) REFERENCES coresettype(id)'
    	]);
    	
//     	$this->createTable('process', [
//     			'id' => Schema::TYPE_PK,
//     			'ini_date' => Schema::TYPE_DATE . ' NOT NULL',
//     			'fi_date' => Schema::TYPE_DATE . ' NOT NULL',
//     			'etiology' => Schema::TYPE_STRING . '(200) NOT NULL',
//     			'id_patient' => Schema::TYPE_INTEGER . ' NOT NULL',
//     	]);
    	
    	$this->createTable('role', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'description' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'active' => 'tinyint' . ' NOT NULL',
    	]);
    	
    	$this->createTable('scaleitem', [
    			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'id_scale' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'PRIMARY KEY (name, id_scale)',
    			'FOREIGN KEY (id_scale) REFERENCES scale(id)',
    	]);
    	
    	$this->createTable('scalevalue', [
    			'id' => Schema::TYPE_PK.' AUTO_INCREMENT',
    			'name_scaleitem' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'id_scale' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'id_adminscale' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'value' => Schema::TYPE_STRING . '(255)',
    			'FOREIGN KEY (name_scaleitem) REFERENCES scaleitem(name)',
    			'FOREIGN KEY (id_scale) REFERENCES scaleitem(id_scale)',
    			'FOREIGN KEY (id_adminscale) REFERENCES adminscale(id)',
    	]);
    	 
    	$this->createTable('source_message', [
    			'id' => Schema::TYPE_PK,
    			'category' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'message' => Schema::TYPE_TEXT,
    	]);

    	$this->createTable('message', [
    			'id' => Schema::TYPE_INTEGER,
    			'language' => Schema::TYPE_STRING . '(16) NOT NULL',
    			'translation' => Schema::TYPE_TEXT,
    			'PRIMARY KEY (id, language)',
    			'FOREIGN KEY (id) REFERENCES source_message(id)',
    	]);
    	    	
    	$this->createTable('{{%user}}', [
    			'id' => Schema::TYPE_PK .' AUTO_INCREMENT',
    			'username' => Schema::TYPE_STRING . ' NOT NULL',
    			'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
    			'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
    			'password_reset_token' => Schema::TYPE_STRING,
    			'email' => Schema::TYPE_STRING . ' NOT NULL',
    			'role' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
    			 
    			'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
    			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
    	]);
    	 
    	$this->createTable('userrole', [
    			'id_user' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'id_role' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (id_user) REFERENCES user(id)',
    			'FOREIGN KEY (id_role) REFERENCES role(id)',
    	]);
    	
    	$this->createTable('user_patient', [
    			'id_user' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'id_patient' => Schema::TYPE_INTEGER . ' NOT NULL',
    			'FOREIGN KEY (id_user) REFERENCES user(id)',
    			'FOREIGN KEY (id_patient) REFERENCES patient(id)',
    	]);
    	
    	$this->createTable('scaleSqueleton', [
    			'scaleCode' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'item' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'nameCode' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'table' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'tableSource' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'idSource' => Schema::TYPE_STRING . '(255) NOT NULL',
    			'unitat' => Schema::TYPE_STRING . '(255) NOT NULL',
    	]);
    	
    	$this->createTable('coreset_icfitem_scaleitem', [
    			'id' => Schema::TYPE_PK.' AUTO_INCREMENT',
    			'id_coreset' => Schema::TYPE_INTEGER,
    			'id_icfitem' => Schema::TYPE_STRING . '(10) NOT NULL',
    			'name_scaleitem' => Schema::TYPE_STRING . '(255)',
    			'id_scale' => Schema::TYPE_INTEGER,
    			'question' => Schema::TYPE_STRING . '(255)',
    			'answer0'=> Schema::TYPE_STRING . '(255)',
    			'answer1'=> Schema::TYPE_STRING . '(255)',
    			'answer2'=> Schema::TYPE_STRING . '(255)',
    			'answer3'=> Schema::TYPE_STRING . '(255)',
    			'answer4'=> Schema::TYPE_STRING . '(255)',
    			'value_answer'=> Schema::TYPE_SMALLINT . '(5)',
    			'FOREIGN KEY (id_coreset) REFERENCES coreset(id)',
    			'FOREIGN KEY (id_icfitem) REFERENCES icfitem(id)',
    			'FOREIGN KEY (name_scaleitem) REFERENCES scaleitem(name)',
    			'FOREIGN KEY (id_scale) REFERENCES scaleitem(id_scale)',
    	]);
 
    	$this->createTable('codebar', [
    			'id' => Schema::TYPE_PK.' AUTO_INCREMENT',
    			'id_icfitem' => Schema::TYPE_STRING . '(10) NOT NULL',
    			'qualifier' => Schema::TYPE_STRING . '(255)',
    			'type' => Schema::TYPE_STRING . '(255)',
    			'id_patient' => Schema::TYPE_INTEGER,
    			'id_evaluation' => Schema::TYPE_INTEGER,
    			'id_adminscale' => Schema::TYPE_INTEGER,
    			
    			'FOREIGN KEY (id_icfitem) REFERENCES icfitem(id)',
    			'FOREIGN KEY (id_patient) REFERENCES patient(id)',
     			'FOREIGN KEY (id_evaluation) REFERENCES evaluation(id)',
     			'FOREIGN KEY (id_adminscale) REFERENCES adminscale(id)',
    	]);
    }
    
    /**
     * Method for fill ouy the users
     */
    private function setUsers(){
    	echo "\nInstalling users...\n";
    	$this->insert('user',['username' => 'tester', 'auth_key' => 'l97C8SZjgKbujdNQMvnHrbqG4WV7rL1I', 'password_hash' => '$2y$13$Lkhb1U0e2sq5YN4RRycnSOYR.8O9Gh3M8R8m5Zu8nX8q8eFZcpsnq', 'password_reset_token' => null, 'email' => 'tester@guttmann.com', 'role' => '10', 'status' => '10', 'created_at' => '1423586410', 'updated_at' => '1423586410']);
    	$this->insert('user',['username' => 'guttmann', 'auth_key' => 'Icrb8-s_J28fNQhK2I7MGMQC7vgO6d54', 'password_hash' => '$2y$13$SeLb3MEYDDKRWweclmou/.VFxlfQSGWZq//Xo5Dwo4Fs4psgHOyHm', 'password_reset_token' => null, 'email' => 'guttmann@guttmann.com', 'role' => '10', 'status' => '10', 'created_at' => '1423586619', 'updated_at' => '1423586619']);
    }
    
    /**
     * Method for fill out the message translation component
     */
    private function setLanguageData(){	
    	echo "\nInstalling the language message...\n";
    	$this->insert('source_message',['category' => 'welcome', 'message' => 'Welcome to CloudReh@b!']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Home']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Patients']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Configuration']);
    	$this->insert('source_message',['category' => 'language', 'message' => 'English']);
    	$this->insert('source_message',['category' => 'language', 'message' => 'Catalan']);
    	$this->insert('source_message',['category' => 'language', 'message' => 'Spanish']);
    	$this->insert('source_message',['category' => 'scale', 'message' => 'ICF']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Login']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Logout']);
    	$this->insert('source_message',['category' => 'menu', 'message' => 'Register']);
    	$this->insert('source_message',['category' => 'title', 'message' => 'CloudReh@b']);
    	$this->insert('source_message',['category' => 'title', 'message' => 'Administrator mode']);
    	$this->insert('source_message',['category' => 'title', 'message' => 'Patient mode']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Login']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Please fill out the following fields to login:']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Username']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Password']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Remember']);
    	$this->insert('source_message',['category' => 'form', 'message' => 'Incorrect username or password.']);
    	$this->insert('source_message',['category' => 'error', 'message' => 'The above error occurred while the Web server was processing your request.']);
    	$this->insert('source_message',['category' => 'error', 'message' => 'Please contact us if you think this is a server error. Thank you.']);
    	 
    	$this->insert('message',['id' => '1', 'language' => 'ca', 'translation' => 'Benvingut a CloudReh@b!']);
    	$this->insert('message',['id' => '1', 'language' => 'es', 'translation' => 'Bienvenido a CloudReh@b!']);
    	$this->insert('message',['id' => '2', 'language' => 'ca', 'translation' => 'Inici']);
    	$this->insert('message',['id' => '2', 'language' => 'es', 'translation' => 'Inicio']);
    	$this->insert('message',['id' => '3', 'language' => 'ca', 'translation' => 'Pacients']);
    	$this->insert('message',['id' => '3', 'language' => 'es', 'translation' => 'Pacientes']);
    	$this->insert('message',['id' => '4', 'language' => 'ca', 'translation' => 'Configuraci&#xF3;']);
    	$this->insert('message',['id' => '4', 'language' => 'es', 'translation' => 'Configuraci&#xF3;n']);
    	$this->insert('message',['id' => '5', 'language' => 'ca', 'translation' => '&#xC0;ngles']);
    	$this->insert('message',['id' => '5', 'language' => 'es', 'translation' => 'Ingles']);
    	$this->insert('message',['id' => '6', 'language' => 'ca', 'translation' => 'Catal&#xE0;']);
    	$this->insert('message',['id' => '6', 'language' => 'es', 'translation' => 'Catalan']);
    	$this->insert('message',['id' => '7', 'language' => 'ca', 'translation' => 'Castell&#xE0;']);
    	$this->insert('message',['id' => '7', 'language' => 'es', 'translation' => 'Castellano']);
    	$this->insert('message',['id' => '8', 'language' => 'ca', 'translation' => 'CIF']);
    	$this->insert('message',['id' => '8', 'language' => 'es', 'translation' => 'CIF']);
    	$this->insert('message',['id' => '9', 'language' => 'ca', 'translation' => 'Inicia sessi&#xF3;']);
    	$this->insert('message',['id' => '9', 'language' => 'es', 'translation' => 'Iniciar sesi&#xF3;n']);
    	$this->insert('message',['id' => '10', 'language' => 'ca', 'translation' => 'Tancament de sessi&#xF3;']);
    	$this->insert('message',['id' => '10', 'language' => 'es', 'translation' => 'Cierre de sesi&#xF3;n']);
    	$this->insert('message',['id' => '11', 'language' => 'ca', 'translation' => 'Registrar-se']);
    	$this->insert('message',['id' => '11', 'language' => 'es', 'translation' => 'Registrarse']);
    	$this->insert('message',['id' => '12', 'language' => 'ca', 'translation' => 'CloudReh@b']);
    	$this->insert('message',['id' => '12', 'language' => 'es', 'translation' => 'CloudReh@b']);
    	$this->insert('message',['id' => '13', 'language' => 'ca', 'translation' => 'Mode administrador']);
    	$this->insert('message',['id' => '13', 'language' => 'es', 'translation' => 'Modo administrador']);
    	$this->insert('message',['id' => '14', 'language' => 'ca', 'translation' => 'Mode pacient']);
    	$this->insert('message',['id' => '14', 'language' => 'es', 'translation' => 'Modo paciente']);
    	$this->insert('message',['id' => '15', 'language' => 'ca', 'translation' => 'Inicia sessi&#xF3;']);
    	$this->insert('message',['id' => '15', 'language' => 'es', 'translation' => 'Inicie sesi&#xF3;n']);
    	$this->insert('message',['id' => '16', 'language' => 'ca', 'translation' => 'Ompli els seg&#xFC;ents camps:']);
    	$this->insert('message',['id' => '16', 'language' => 'es', 'translation' => 'Rellene los siguientes campos:']);
    	$this->insert('message',['id' => '17', 'language' => 'ca', 'translation' => "Nom d&#x27;usuari"]);
    	$this->insert('message',['id' => '17', 'language' => 'es', 'translation' => 'Nombre de usuario']);
    	$this->insert('message',['id' => '18', 'language' => 'ca', 'translation' => 'Contrasenya']);
    	$this->insert('message',['id' => '18', 'language' => 'es', 'translation' => 'Contrase&#xF1;a']);
    	$this->insert('message',['id' => '19', 'language' => 'ca', 'translation' => "Recorda&#x27;m"]);
    	$this->insert('message',['id' => '19', 'language' => 'es', 'translation' => 'Recu&#xE9;rdeme']);
    	$this->insert('message',['id' => '20', 'language' => 'ca', 'translation' => "Nom d&#x27;usuari o contrasenya incorrecte"]);
    	$this->insert('message',['id' => '20', 'language' => 'es', 'translation' => 'Nombre de usuario o contrase&#xF1;a incorrectos']);
    	$this->insert('message',['id' => '21', 'language' => 'ca', 'translation' => 'Ha aparegut un error en el moment que el servidor web estava processant la petici&#xF3;.']);
    	$this->insert('message',['id' => '21', 'language' => 'es', 'translation' => 'Ha aparecido un error en el momento en que el servidor web estaba procesando la petici&#xF3;n.']);
    	$this->insert('message',['id' => '22', 'language' => 'ca', 'translation' => "Siusplau, posis en contacte amb l&#x27;administrador de la p&#xE0;gina."]);
    	$this->insert('message',['id' => '22', 'language' => 'es', 'translation' => "Por favor, p&#xF3;ngase en contacto con el administrado de la p&#xE1;gina."]);
    }
    
    
    private function setScale(){
    	echo "\n\nStoring the scales...\n\n";
    	$query = new Query();
    	$query->select(['scaleCode','table'],'distinct')->from('scalesqueleton');
    	$command = $query->createCommand();
    	$scales = $command->queryAll();
     	foreach($scales as $scale){
    		//Storing the scales
    		$this->insert('scale', ['id' => $scale['scaleCode'], 'acronym' => $scale['table'], 'active' => true]);
    		
    		//Save the items of this scale
    		$this->setScaleItem($scale);
    	}
    	echo "Scales was stored!\n\n";
    }
    
    
    private function setScaleItem($scale){	
    	$queryScaleItem = new Query();
    	$queryScaleItem->select('table,nameCode')->from('scalesqueleton')->where('scaleCode ='.$scale['scaleCode']);
    	$commnandScaleItem = $queryScaleItem->createCommand();
    	$items = $commnandScaleItem->queryAll();
    		
    	foreach($items as $item){
    		echo $item['nameCode'];
    		$id = $item['nameCode'];		
    		//Storing the scaleitem
    		$this->insert('scaleitem', ['name'=> $id,'id_scale'=>$scale['scaleCode']]);
    	}
    	
    	echo "Scales items was stored!\n\n";    	 
    }

    
    /*
     * Method for fill out the coreset type
     */
    private function setCoresetType(){
    	echo "\nInstalling the coreset type data...\n";
    	$this->insert('coresettype',['id' => '1', 'type' => 'DCA']);
    }

    /*
     * Method for fill out the coreset
     */
    private function setCoreset(){
    	echo "\nInstalling the coreset type data...\n";
    	$this->insert('coreset',['id' => '1', 'name' => 'Traumatic Brain Injury', 'id_coresettype' => '1']);
    	$this->insert('coreset',['id' => '2', 'name' => 'Stroke', 'id_coresettype' => '1']);
    }
    
    /*
     * Method for fill out the ICF data
     */
    private function setICFData(){
    	echo "\nInstalling the ICF DCA coreset data...\n";
    	$this->insert('icfitem',['id' => 'b110', 'description' => 'Consciousness functions']);
    	$this->insert('icfitem',['id' => 'b114', 'description' => 'Orientation functions']);
    	$this->insert('icfitem',['id' => 'b130', 'description' => 'Energy and drive functions']);
    	$this->insert('icfitem',['id' => 'b140', 'description' => 'Attention functions']);
    	$this->insert('icfitem',['id' => 'b144', 'description' => 'Memory functions']);
    	$this->insert('icfitem',['id' => 'b152', 'description' => 'Emotional functions']);
    	$this->insert('icfitem',['id' => 'b164', 'description' => 'Higher-level cognitive functions']);
    	$this->insert('icfitem',['id' => 'b167', 'description' => 'Mental functions of language']);
    	$this->insert('icfitem',['id' => 'b280', 'description' => 'Sensation of pain']);
    	$this->insert('icfitem',['id' => 'b730', 'description' => 'Muscle power functions']);
    	$this->insert('icfitem',['id' => 'b760', 'description' => 'Control of voluntary movement functions']);
    	$this->insert('icfitem',['id' => 'd230', 'description' => 'Carrying out daily routine']);
    	$this->insert('icfitem',['id' => 'd310', 'description' => 'Communicating with - receiving - spoken messages']);
    	$this->insert('icfitem',['id' => 'd330', 'description' => 'Speaking']);
    	$this->insert('icfitem',['id' => 'd350', 'description' => 'Conversation']);
    	$this->insert('icfitem',['id' => 'd450', 'description' => 'Walking']);
    	$this->insert('icfitem',['id' => 'd455', 'description' => 'Moving around']);
    	$this->insert('icfitem',['id' => 'd5', 'description' => 'SELF-CARE']);
    	$this->insert('icfitem',['id' => 'd510', 'description' => 'Washing oneself']);
    	$this->insert('icfitem',['id' => 'd530', 'description' => 'Toileting']);
    	$this->insert('icfitem',['id' => 'd540', 'description' => 'Dressing']);
    	$this->insert('icfitem',['id' => 'd550', 'description' => 'Eating']);
    	$this->insert('icfitem',['id' => 'd720', 'description' => 'Complex interpersonal interactions']);
    	$this->insert('icfitem',['id' => 'd760', 'description' => 'Family relationships']);
    	$this->insert('icfitem',['id' => 'd845', 'description' => 'Acquiring, keeping and terminating a job']);
    	$this->insert('icfitem',['id' => 'd850', 'description' => 'Remunerative employment']);
    	$this->insert('icfitem',['id' => 'd920', 'description' => 'Recreation and leisure']);
    	$this->insert('icfitem',['id' => 'e115', 'description' => 'Products and technology for personal use in daily living']);
    	$this->insert('icfitem',['id' => 'e120', 'description' => 'Products and technology for personal indoor and outdoor mobility and transportation']);
    	$this->insert('icfitem',['id' => 'e310', 'description' => 'Immediate family']);
    	$this->insert('icfitem',['id' => 'e320', 'description' => 'Friends']);
    	$this->insert('icfitem',['id' => 'e355', 'description' => 'Health professionals']);
    	$this->insert('icfitem',['id' => 'e570', 'description' => 'Social security services, systems and policies']);
    	$this->insert('icfitem',['id' => 'e580', 'description' => 'Health services, systems and policies']);
    	$this->insert('icfitem',['id' => 's110', 'description' => 'Structure of brain']);
    	$this->insert('icfitem',['id' => 's730', 'description' => 'Structure of upper extremity']); 
    }
    
    /*
     * Method for fill out the coreset, icf and scale
     */
    private function setCoresetIcfScale(){
    	/* TBI */
    	echo "\nInstalling the Coreset, I and SCALE...\n";
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b110', 'name_scaleitem' => 'OberturaUlls', 'id_scale' => Scale::DRS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b110', 'name_scaleitem' => 'CapacitatComunicacio', 'id_scale' => Scale::DRS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b110', 'name_scaleitem' => 'RespostaMotora', 'id_scale' => Scale::DRS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b130']); 	//HIBS
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b140', 'name_scaleitem' => 'ATENCIO_Span', 'id_scale' => Scale::BATERIA]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b144', 'name_scaleitem' => 'MEMORIA_RAVLT075', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b144', 'name_scaleitem' => 'MEMORIA_RAVLT015', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b144', 'name_scaleitem' => 'MEMORIA_RAVLT015R', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b152']); 	//HIBS
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b164', 'name_scaleitem' => 'FEXECUTIVAS_TmtB', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b280', 'question' => "Quant dolor ha tingut durant la l&#x27;&#xFA;ltima setmana?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b760', 'question' => "En quina mesura pensa que el pacient t&#xE9; un transtorn del moviment?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 's110', 'name_scaleitem' => 'OberirOrdresSimples', 'id_scale' => Scale::GOSE]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd230', 'question' => 'Dur a terme rutines diaries?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd350', 'question' => 'Pragm&#xE0;tica del llenguatge?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd450', 'name_scaleitem' => 'Marxa', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd455', 'question' => 'En quina mesura t&#xE9; dificultat per fer activitats diaries com correr, nadar, saltar, ballar...?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd5', 'name_scaleitem' => 'Total', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd720', 'name_scaleitem' => 'InteraccioSocial', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd760']); 	//HIBS
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd845', 'name_scaleitem' => 'Laboral', 'id_scale' => Scale::ESIG_1AV]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd850', 'name_scaleitem' => 'PENSIO', 'id_scale' => Scale::ESIG_1AV]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'd920', 'id_scale' => Scale::CIQ_G]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e115', 'question' => 'En quin grau t&#xE9; facilitat per dispossar de material de suport per les activitats de &#xFA;s diari?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e120', 'question' => 'En quin grau t&#xE9; facilitat per dispossar de material de suport per la mobilitat interior i exterior', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e310', 'name_scaleitem' => 'Convivencia', 'id_scale' => Scale::ESIG_1AV]);  
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e320', 'id_scale' => Scale::CIQ_G]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e570', 'name_scaleitem' => 'PENSIO', 'id_scale' => Scale::ESIG_1AV]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'e580', 'question' => "En quina mesura creus que el sistema de salut (politiques, serveis, etc.) t&#x27;ajuden en el teu dia a dia?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	
    	/* STROKE */
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b110', 'name_scaleitem' => 'OberturaUlls', 'id_scale' => Scale::DRS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b110', 'name_scaleitem' => 'CapacitatComunicacio', 'id_scale' => Scale::DRS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b110', 'name_scaleitem' => 'RespostaMotora', 'id_scale' => Scale::DRS]);    
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b114', 'name_scaleitem' => 'ORIENTACIO_Temps', 'id_scale' => Scale::BATERIA]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b130']); //HIBS
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b140', 'name_scaleitem' => 'ATENCIO_Span', 'id_scale' => Scale::BATERIA]); 
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '1', 'id_icfitem' => 'b144', 'name_scaleitem' => 'MEMORIA_RAVLT075', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b152']); 	//HIBS
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b167', 'name_scaleitem' => 'LLENGUATGE_DenominacioTB', 'id_scale' => Scale::BATERIA]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b280', 'question' => "Quant dolor ha tingut durant la l&#x27;&#xFA;ltima setmana?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'b730', 'question' => 'Quin &#xE9;s el balan&#xE7; muscular?', 'answer0' => 'No contracció', 'answer1' => 'Contracció muscular sense o amb moviment', 'answer2' => 'Contracció muscular contra gravetat', 'answer3' => 'Contraccio muscular contra resistencia', 'answer4' => 'Contraccio muscular normal']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 's110', 'name_scaleitem' => 'Total', 'id_scale' => Scale::NIHSS]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 's730', 'question' => "De les activitats de l&#x27;extremitat superior l&#x27;alteraci&#xF3; de la estructura el limita?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd230', 'question' => 'Dur a terme rutines diaries?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd310', 'name_scaleitem' => 'CompresioAuditiva', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd330', 'name_scaleitem' => 'Bany', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd450', 'name_scaleitem' => 'Marxa', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd455', 'question' => 'En quina mesura t&#xE9; dificultat per fer activitats diaries com correr, nadar, saltar, ballar...?', 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd510', 'name_scaleitem' => 'ExpressioVerbal', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd530', 'name_scaleitem' => 'Marxa', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd540', 'name_scaleitem' => 'VestitSuperior', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd540', 'name_scaleitem' => 'VestitInferior', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd550', 'name_scaleitem' => 'Alimentacio', 'id_scale' => Scale::FIM]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'd850', 'name_scaleitem' => 'PENSIO', 'id_scale' => Scale::ESIG_1AV]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'e310', 'name_scaleitem' => 'Convivencia', 'id_scale' => Scale::ESIG_1AV]);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'e355', 'question' => "En quina mesura creus que els professionals de la salut t&#x27;ajuden (tracte, atenci&#xF3;, comprenci&#xF3;...) en el teu dia a dia?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']);
    	$this->insert('coreset_icfitem_scaleitem',['id_coreset' => '2', 'id_icfitem' => 'e580', 'question' => "En quina mesura creus que el sistema de salut (politiques, serveis, etc.) t&#x27;ajuden en el teu dia a dia?", 'answer0' => '0%', 'answer1' => '25%', 'answer2' => '50%', 'answer3' => '75%', 'answer4' => '100%']); 
    }
    
    /**
     * Method to built the scales squeleton
     */
    private function setScaleSqueleton(){
    	echo "\nInstalling the scaleSqueleton data...\n";
    	
    	/*FIM*/
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '2', 'nameCode' => 'Alimentacio', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '3', 'nameCode' => 'CuresAparensa', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '4', 'nameCode' => 'Higiene', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '5', 'nameCode' => 'VestitSuperior', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '6', 'nameCode' => 'VestitInferior', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '7', 'nameCode' => 'Bany', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '9', 'nameCode' => 'Bufeta', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '10', 'nameCode' => 'Intesti', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '12', 'nameCode' => 'LlitCadiraCadiraRodes', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '13', 'nameCode' => 'WC', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '14', 'nameCode' => 'BanyeraDutxa', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '16', 'nameCode' => 'Marxa', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '17', 'nameCode' => 'CadiraRodes', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '18', 'nameCode' => 'Escala', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '361', 'nameCode' => 'TotalMotor', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '947', 'nameCode' => 'MitjanaMotor', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '20', 'nameCode' => 'CompresioAuditiva', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '21', 'nameCode' => 'CompresioVisual', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '22', 'nameCode' => 'ExpressioVerbal', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '23', 'nameCode' => 'ExpressioNoVerbal', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '25', 'nameCode' => 'InteraccioSocial', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '26', 'nameCode' => 'ResolucioProblemes', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '27', 'nameCode' => 'Memoria', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '946', 'nameCode' => 'TotalCognitiu', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '948', 'nameCode' => 'MitjanaCognititu', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '28', 'nameCode' => 'Total', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' => '1', 'item' => '949', 'nameCode' => 'Mitjana', 'table' => 'FIM', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);

      	/*FIMFAM*/
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '30', 'nameCode' => 'Alimentacio', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '31', 'nameCode' => 'CuresAparensa', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '32', 'nameCode' => 'Higiene', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '33', 'nameCode' => 'VestitSuperior', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '34', 'nameCode' => 'VestitInferior', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '35', 'nameCode' => 'Bany', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '36', 'nameCode' => 'Deglucio', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '38', 'nameCode' => 'Bufeta', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '39', 'nameCode' => 'Intesti', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '41', 'nameCode' => 'LlitCadiraCadiraRodes', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '42', 'nameCode' => 'WC', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '43', 'nameCode' => 'BanyeraDutxa', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '44', 'nameCode' => 'Cotxe', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '46', 'nameCode' => 'Marxa', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '47', 'nameCode' => 'CadiraRodes', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '48', 'nameCode' => 'Escala', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '49', 'nameCode' => 'MobilitatComunitat', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '51', 'nameCode' => 'ComprensioAuditiva', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '52', 'nameCode' => 'ComprensioVisual', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '53', 'nameCode' => 'ExpressioVerbal', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '54', 'nameCode' => 'ExpressioNoVerbal', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '55', 'nameCode' => 'Lectura', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '56', 'nameCode' => 'Escriptura', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '57', 'nameCode' => 'Parla', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '59', 'nameCode' => 'InteraccioSocial', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '60', 'nameCode' => 'EstatEmocional', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '61', 'nameCode' => 'AdaptacioLimitacio', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '62', 'nameCode' => 'CapacitatReinsercioSocial', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '63', 'nameCode' => 'ResolucioProblemes', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '64', 'nameCode' => 'Memoria', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '65', 'nameCode' => 'Orientacio', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '66', 'nameCode' => 'Atencio', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '67', 'nameCode' => 'JudiciSeguretat', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '68', 'nameCode' => 'TOTAL', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
      	$this->insert('scaleSqueleton',['scaleCode' =>'2', 'item' => '362', 'nameCode' => 'TOTALMOTOR', 'table' => 'FIMFAM', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
    	
    	/*Bateria*/
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VORIENTA', 'nameCode' => 'V_Orientacio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'OPERSONA', 'nameCode' => 'ORIENTACIO_Persona', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'OESPAI', 'nameCode' => 'ORIENTACIO_Espai', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'OTEMPS', 'nameCode' => 'ORIENTACIO_Temps', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VAWAIS', 'nameCode' => 'V_ATENCIO_WAIS', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'ASPAN', 'nameCode' => 'ATENCIO_Span', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VATMTA', 'nameCode' => 'V_ATENCIO_TMT_A', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'ATMTA', 'nameCode' => 'ATENCIO_TmtA', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VASTROOP', 'nameCode' => 'V_ATENCIOStrop', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'APARAULA', 'nameCode' => 'ATENCIO_STROP_Paraula', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'ACOLOR', 'nameCode' => 'ATENCIO_STROOP_Color', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'APARAULACOLOR', 'nameCode' => 'ATENCIO_STROOP_ParaulaColor', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VVWAIS', 'nameCode' => 'V_VWAIS', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VPWAISIII', 'nameCode' => 'VELOCITATPROCESSAMENTINFORMACIO', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VLLAMINA', 'nameCode' => 'V_LLENGUATGE_LaminaA', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'LLAMINATB', 'nameCode' => 'LLENGUATGE_LaminaTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VLREPE', 'nameCode' => 'V_LLENGUATGE_Repeticio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'LREPETICIOTB', 'nameCode' => 'LLENGUATGE_RepeticioTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VLDENO', 'nameCode' => 'V_LLENGUATGE_Denominacio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'LDENOMINACIOTB', 'nameCode' => 'LLENGUATGE_DenominacioTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VLCOMP', 'nameCode' => 'V_LLENGUATGE_Comprensio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'LCOMPRENSIOTB', 'nameCode' => 'LLENGUATGE_ComprensioTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VPERCEP', 'nameCode' => 'V_Percepcio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VPIMATGES', 'nameCode' => 'VISUOPERCEPCIO_ISuperposadesTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VCONSTRUC', 'nameCode' => 'V_Construccio', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VCCUBS', 'nameCode' => 'VISUOCONSTRUCCIO_Cubs', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VMDIGITS', 'nameCode' => 'V_MEMORIA_Digits', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'MDIGITS', 'nameCode' => 'MEMORIA_DigitsInversos', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VMLLETRES', 'nameCode' => 'V_MEMORIA_Lletres', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'MLLETRES', 'nameCode' => 'MEMORIA_LletresNumeros', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VMRAVLT075', 'nameCode' => 'VALORABLE_MEMORIA_RAVLT075', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'MRAVLT075', 'nameCode' => 'MEMORIA_RAVLT075', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VMRAVLT015', 'nameCode' => 'V_MEMORIA_RAVLT015', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'MRAVLT015', 'nameCode' => 'MEMORIA_RAVLT015', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VMRAVLT015R', 'nameCode' => 'V_MEMORIA_RAVLT015R', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'MRAVLT015R', 'nameCode' => 'MEMORIA_RAVLT015R', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VFETMTB', 'nameCode' => 'V_FEXECUTIVES_TMTB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'FETMTB', 'nameCode' => 'FEXECUTIVAS_TmtB', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VFEWCST', 'nameCode' => 'V_FXEXECUTIVES_WCST', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'FEWCSTC', 'nameCode' => 'FEXECUTIVAS_WcstCategories', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'FEWCSTE', 'nameCode' => 'FEXECUTIVAS_WcstEPerseverants', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'FESTROOP', 'nameCode' => 'FEXECUTIVAS_StroopCalculA', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'VFEPMR', 'nameCode' => 'V_FXEXECUTIVES_PMR', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'52', 'item' => 'FEPMR', 'nameCode' => 'FEXECUTIVAS_PRM', 'table' => 'BATERIA', 'tableSource' => 'ESCBATERIA',  'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);	
    	
    	/*DRS*/
      	$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '73', 'nameCode' => 'OberturaUlls', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '74', 'nameCode' => 'CapacitatComunicacio', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '75', 'nameCode' => 'RespostaMotora', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '77', 'nameCode' => 'Alimentacio', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '78', 'nameCode' => 'Higiene', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '79', 'nameCode' => 'CuresAparensa', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '81', 'nameCode' => 'NivellFuncionalitat', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'7', 'item' => '83', 'nameCode' => 'Empleabilitat', 'table' => 'DRS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(10,14,15,16,17,18,19)']);
  		
  		/*GOSE*/
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '386', 'nameCode' => 'OberirOrdresSimples', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '389', 'nameCode' => 'AjudaTerceraPersonaVidaDiaria', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '390', 'nameCode' => 'AjudaTerceraPersonaCasa', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '391', 'nameCode' => 'AjudaAbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '393', 'nameCode' => 'Comprar', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '394', 'nameCode' => 'ComprarAbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '395', 'nameCode' => 'Viatjar', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '396', 'nameCode' => 'ViatjarAbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '398', 'nameCode' => 'TreballNivellPrevi', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '399', 'nameCode' => 'Limitacio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '400', 'nameCode' => 'SituacioLaboralAbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '402', 'nameCode' => 'CapacitatActivitatsSocials', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '403', 'nameCode' => 'GrauRestriccio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '404', 'nameCode' => 'AbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '406', 'nameCode' => 'ProblemesPsicologic', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '407', 'nameCode' => 'GrauTranstornTensio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '408', 'nameCode' => 'ProblemesFamiliars', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '410', 'nameCode' => 'AltresProblemes', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '411', 'nameCode' => 'ProblemesSimilarsAbansLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '413', 'nameCode' => 'CrisisDespresLesio', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '414', 'nameCode' => 'RiscActual', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '415', 'nameCode' => 'FactorImportantResultat', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'51', 'item' => '416', 'nameCode' => 'Total', 'table' => 'GOSE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'id', 'unitat' => '(10,19)']);

  		/*NIHSS*/
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '604', 'nameCode' => 'NivellConsciencia', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '605', 'nameCode' => 'PreguntesLOC', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '606', 'nameCode' => 'OrdresLOC', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '607', 'nameCode' => 'Mirada', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '608', 'nameCode' => 'Visual', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '609', 'nameCode' => 'ParalisiFacial', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '662', 'nameCode' => 'MBrasDret', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '663', 'nameCode' => 'MBrasEsquerre', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '664', 'nameCode' => 'MCamaDreta', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '665', 'nameCode' => 'MCamaEsquerra', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '612', 'nameCode' => 'AtaxisDeMembres', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '666', 'nameCode' => 'BrasDret', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '667', 'nameCode' => 'BrasEsquerra', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '668', 'nameCode' => 'CamaDreta', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '669', 'nameCode' => 'CamaEquerra', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '613', 'nameCode' => 'Sensibilitat', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '614', 'nameCode' => 'Llenguatge', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '615', 'nameCode' => 'Disartria', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '616', 'nameCode' => 'ExtincioIntencioNegligencia', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '617', 'nameCode' => 'FuncioMotoraDistra', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '670', 'nameCode' => 'MaDreta', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '671', 'nameCode' => 'MaEsquerra', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'73', 'item' => '672', 'nameCode' => 'Total', 'table' => 'NIHSS', 'tableSource' => 'ESCALESCAP', 'unitat' => '(14,15,16,17,18)']);		
		
  		/*CIQ-G*/
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'HIGIENEPERSONAL', 'nameCode' => 'HigienePersonal', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'PREPARAESMORZAR', 'nameCode' => 'PreparaEsmorzar', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'TASQUESDOMESTIQUES', 'nameCode' => 'TasquesDomestiques', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'FINANCES', 'nameCode' => 'Finances', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'PLANEJAACTIVITATS', 'nameCode' => 'PlanejaActivitats', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'OCI', 'nameCode' => 'Oci', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'VISITES', 'nameCode' => 'Visites', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'OCIAMB', 'nameCode' => 'OciaAmb', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'BONAMIC', 'nameCode' => 'BonAmic', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'SORTIR', 'nameCode' => 'Sortir', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'SITUACIOLABORAL', 'nameCode' => 'SituacioLaboral', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'FORMACIO', 'nameCode' => 'Formacio', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'VOLUNTARIAT', 'nameCode' => 'Voluntariat', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'TOTALLLAR', 'nameCode' => 'TotalLlar', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'TOTALINTEGRACIO', 'nameCode' => 'TotalIntegracio', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'TOTALPRODUCTIVITAT', 'nameCode' => 'TotalProductivitat', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		$this->insert('scaleSqueleton',['scaleCode' =>'34', 'item' => 'TOTAL', 'nameCode' => 'Total', 'table' => 'CIQ', 'tableSource' => 'ESCCIQ','idSource' => 'clau','unitat' => '(10,14,15,16,17,18,19)']);
  		
		/*ESIG 1AV*/
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'ESTUDIS', 'nameCode' => 'Estudis', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'CONVIVENCIA_I', 'nameCode' => 'ConvivenciaI', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'CONVIVENCIA_A', 'nameCode' => 'ConvivenciaA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'RESIDENCIA_I', 'nameCode' => 'ResidenciaI', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'RESIDENCIA_A', 'nameCode' => 'ResidenciaA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LLAR_A', 'nameCode' => 'LlarA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'ACCESSIBILITAT', 'nameCode' => 'Accessibilitat', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORAL_I', 'nameCode' => 'LaboralI', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORALQUI_I', 'nameCode' => 'LaboralQuiI', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORALON_I', 'nameCode' => 'LaboralOnI', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORAL_A', 'nameCode' => 'LaboralA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORALQUI_A', 'nameCode' => 'LaboralQuiA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'LABORALON_A', 'nameCode' => 'LaboralOnA', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'PENSIO', 'nameCode' => 'Pensio', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT1', 'nameCode' => 'Mobilitat1', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT2', 'nameCode' => 'Mobilitat2', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT3', 'nameCode' => 'Mobilitat3', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT4', 'nameCode' => 'Mobilitat4', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT5', 'nameCode' => 'Mobilitat5', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT6', 'nameCode' => 'Mobilitat6', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT7', 'nameCode' => 'Mobilitat7', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'MOBILITAT8', 'nameCode' => 'Mobilitat8', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'FIGURA', 'nameCode' => 'Figura', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'DEDICACIO', 'nameCode' => 'Dedicacio', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI1', 'nameCode' => 'Servei1', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI2', 'nameCode' => 'Servei2', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI3', 'nameCode' => 'Servei3', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI4', 'nameCode' => 'Servei4', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI5', 'nameCode' => 'Servei5', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI6', 'nameCode' => 'Servei6', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI7', 'nameCode' => 'Servei7', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI8', 'nameCode' => 'Servei8', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI9', 'nameCode' => 'Servei9', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI10', 'nameCode' => 'Servei10', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'29', 'item' => 'SERVEI11', 'nameCode' => 'Servei11', 'table' => 'ESIG_1AV', 'tableSource' => 'ESCESIG_1AV', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		
		/*ESIG 1AV V2 */
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'ESTUDIS', 'nameCode' => 'Estudis', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'RESIDENCIA_I', 'nameCode' => 'ResidenciaI', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'RESIDENCIA_A', 'nameCode' => 'ResidenciaA', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'ACCESSIBILITAT', 'nameCode' => 'Accessibilitat', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'CONVIVENCIA_I', 'nameCode' => 'ConvivenciaI', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'CONVIVENCIA_A', 'nameCode' => 'ConvivenciaA', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORAL_I', 'nameCode' => 'LaboralI', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORALQUI_I', 'nameCode' => 'LaboralQuiI', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORALON_I', 'nameCode' => 'LaboralOnI', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORAL_A', 'nameCode' => 'LaboralA', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORALQUI_A', 'nameCode' => 'LaboralQuiA', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LABORALON_A', 'nameCode' => 'LaboralOnA', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PENSIO', 'nameCode' => 'Pensio', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT1', 'nameCode' => 'Mobilitat1', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT2', 'nameCode' => 'Mobilitat2', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT3', 'nameCode' => 'Mobilitat3', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT4', 'nameCode' => 'Mobilitat4', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT5', 'nameCode' => 'Mobilitat5', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT6', 'nameCode' => 'Mobilitat6', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'MOBILITAT7', 'nameCode' => 'Mobilitat7', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'FIGURA', 'nameCode' => 'Figura', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'DEDICACIO', 'nameCode' => 'Dedicacio', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'LLEIDEP', 'nameCode' => 'LleiDependencia', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'CONCEDITPIA', 'nameCode' => 'ConceditPia', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA1', 'nameCode' => 'Pia1', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA2', 'nameCode' => 'Pia2', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA3', 'nameCode' => 'Pia3', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA4', 'nameCode' => 'Pia4', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA5', 'nameCode' => 'Pia5', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA6', 'nameCode' => 'Pia6', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA7', 'nameCode' => 'Pia7', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA8', 'nameCode' => 'Pia8', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'107', 'item' => 'PIA9', 'nameCode' => 'Pia9', 'table' => 'ESIG_1AV_V2', 'tableSource' => 'ESCESIG_1AV_V2', 'idSource' => 'id','unitat' => '(10,14,15,16,17,18,19)']);

		/*ESIG SEG*/
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ESTUDIS', 'nameCode' => 'Estudis', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ESTUDIS_C', 'nameCode' => 'EstudisC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ESTUDIS_S', 'nameCode' => 'EstudisS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'CONVIVENCIA', 'nameCode' => 'Convivencia', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'CONVIVENCIA_C', 'nameCode' => 'ConvivenciaC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'RESIDENCIA', 'nameCode' => 'Residencia', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACCESSIBILITAT', 'nameCode' => 'Accessibilitat', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LLAR_C', 'nameCode' => 'LlarC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LLAR_S', 'nameCode' => 'LlarS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'NOTREBALLA', 'nameCode' => 'NoTreballa', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'TIPUSTREBALL', 'nameCode' => 'TipusTreball', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ASSEGURAT', 'nameCode' => 'Assegurat', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LABORALQUI', 'nameCode' => 'LaboralQui', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LABORALON', 'nameCode' => 'LaboralOn', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LABORAL_C', 'nameCode' => 'LaboralC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'LABORAL_S', 'nameCode' => 'LaboralS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'PENSIO', 'nameCode' => 'Pensio', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'PENSIO_C', 'nameCode' => 'PensioC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'PENSIO_S', 'nameCode' => 'PensioS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT1', 'nameCode' => 'Mobilitat1', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT2', 'nameCode' => 'Mobilitat2', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT3', 'nameCode' => 'Mobilitat3', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT4', 'nameCode' => 'Mobilitat4', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT5', 'nameCode' => 'Mobilitat5', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT6', 'nameCode' => 'Mobilitat6', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT7', 'nameCode' => 'Mobilitat7', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT8', 'nameCode' => 'Mobilitat8', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT_C', 'nameCode' => 'MobilitatC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'MOBILITAT_S', 'nameCode' => 'MobilitatS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS1', 'nameCode' => 'Activita1t', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS2', 'nameCode' => 'Activitat2', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS3', 'nameCode' => 'Activitat3', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS4', 'nameCode' => 'Activitat4', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS5', 'nameCode' => 'Activitat5', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS6', 'nameCode' => 'Activitat6', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS7', 'nameCode' => 'Activitat7', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS8', 'nameCode' => 'Activitat8', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FREQUENCIA', 'nameCode' => 'Frequencia', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'DURADA', 'nameCode' => 'Durada', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS_C', 'nameCode' => 'ActivitatsC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'ACTIVITATS_S', 'nameCode' => 'ActivitatsS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FIGURA', 'nameCode' => 'Figura', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'DEDICACIO', 'nameCode' => 'Dedicacio', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'AJUDAAVD_C', 'nameCode' => 'AjudaAvdC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'AJUDAAVD_S', 'nameCode' => 'AjudaAvdS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI1', 'nameCode' => 'Servei1', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI2', 'nameCode' => 'Servei2', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI3', 'nameCode' => 'Servei3', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI4', 'nameCode' => 'Servei4', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI5', 'nameCode' => 'Servei5', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI6', 'nameCode' => 'Servei6', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI7', 'nameCode' => 'Servei7', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI8', 'nameCode' => 'Servei8', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI9', 'nameCode' => 'Servei9', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI10', 'nameCode' => 'Servei10', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI11', 'nameCode' => 'Servei11', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI_C', 'nameCode' => 'ServeiC', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'SERVEI_S', 'nameCode' => 'ServeiS', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT1', 'nameCode' => 'FQ_Act1', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT2', 'nameCode' => 'FQ_Act2', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT3', 'nameCode' => 'FQ_Act3', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT4', 'nameCode' => 'FQ_Act4', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT5', 'nameCode' => 'FQ_Act5', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT6', 'nameCode' => 'FQ_Act6', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'FQ_ACT7', 'nameCode' => 'FQ_Act7', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'30', 'item' => 'PENSIO_ALTRES', 'nameCode' => 'PensioAltres', 'table' => 'ESIG_SEG', 'tableSource' => 'ESCESIG_SEG', 'idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		
		/*ESIG SEG V2*/
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'ESTUDIS', 'nameCode' => 'Estudis', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'RESIDENCIA', 'nameCode' => 'Residencia', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'ACCESSIBILITAT', 'nameCode' => 'Accessibilitat', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'CONVIVENCIA', 'nameCode' => 'Convivencia', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'LABORAL', 'nameCode' => 'Laboral', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'NOTREBALLA', 'nameCode' => 'Notreballa', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PENSIO', 'nameCode' => 'Pensio', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'MOBILITAT1', 'nameCode' => 'Mobilitat1', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'MOBILITAT2', 'nameCode' => 'Mobilitat2', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'MOBILITAT3', 'nameCode' => 'Mobilitat3', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'FIGURA', 'nameCode' => 'Figura', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'DEDICACIO', 'nameCode' => 'Dedicacio', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'LLEIDEP', 'nameCode' => 'Lleidep', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'CONCEDITPIA', 'nameCode' => 'Conceditpia', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA1', 'nameCode' => 'Pia1', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA2', 'nameCode' => 'Pia2', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA3', 'nameCode' => 'Pia3', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA4', 'nameCode' => 'Pia4', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA5', 'nameCode' => 'Pia5', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA6', 'nameCode' => 'Pia6', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA7', 'nameCode' => 'Pia7', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA8', 'nameCode' => 'Pia8', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'108', 'item' => 'PIA9', 'nameCode' => 'Pia9', 'table' => 'ESIG_SEG_V2', 'tableSource' => 'ESCESIG_SEG_V2','idSource' => 'id', 'unitat' => '(10,14,15,16,17,18,19)']);
		
		/*Nivell d'afectacio del llenguatge*/
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '257', 'nameCode' => 'Llenguatge_NarracioTematica', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '258', 'nameCode' => 'Llenguatge_Descripcio', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '259', 'nameCode' => 'Llenguatge_FluenciaGramatica', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '260', 'nameCode' => 'Llenguatge_ContingutInformatiu', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '262', 'nameCode' => 'Repeticio_Silabes', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '263', 'nameCode' => 'Repeticio_Paraules', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '264', 'nameCode' => 'Repeticio_Frases', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '266', 'nameCode' => 'Denominacio_VisualVerbal', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '267', 'nameCode' => 'Denominacio_VisualVerbalTemps', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '268', 'nameCode' => 'Denominacio_VerboVerbal', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '269', 'nameCode' => 'Denominacio_VerboVerbalTemps', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '270', 'nameCode' => 'Denominacio_Completament', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '271', 'nameCode' => 'Denominacio_CompletamentTemps', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '273', 'nameCode' => 'Comprensio_Paraules', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '274', 'nameCode' => 'Comprensio_ParaulesTemps', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '275', 'nameCode' => 'Comprensio_Ordres', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '276', 'nameCode' => 'ComprensioMaterialVerbalC', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '278', 'nameCode' => 'Lectura_Lletres', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '279', 'nameCode' => 'Lectura_Numeros', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '280', 'nameCode' => 'Lectura_Paraules', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '281', 'nameCode' => 'Lectura_Text', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '283', 'nameCode' => 'Comprensio_ParaulaImatge', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '284', 'nameCode' => 'Comprensio_OrdresEscrites', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '285', 'nameCode' => 'Comprensio_FrasesTextos', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '287', 'nameCode' => 'Escritura_MecanicaEscripturaD', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '288', 'nameCode' => 'Escritura_MecanicaEscripturaE', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '289', 'nameCode' => 'Escriptura_Lletres', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '290', 'nameCode' => 'Escriptura_Numeros', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '291', 'nameCode' => 'Escriptura_Paraules', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
		$this->insert('scaleSqueleton',['scaleCode' =>'36', 'item' => '292', 'nameCode' => 'Escriptura_Frases', 'table' => 'AFECTACIOLLENGUATGE', 'tableSource' => 'ESCALESCAP', 'idSource' => 'CLAU', 'unitat' => '(10,14,15,16,17,18,19)']);
    }
    
    /**
     * (non-PHPdoc)
     * @see \yii\db\Migration::down()
     */
    public function down(){
    	echo "\nReverting the migration...\n";
    	$this->dropTable('user_patient');
    	$this->dropTable('userrole');
    	$this->dropTable('{{%user}}');
    	$this->dropTable('scalevalue');
    	$this->dropTable('role');
    	$this->dropTable('codebar');
    	$this->dropTable('adminscale');
    	$this->dropTable('message');
    	$this->dropTable('source_message');
    	$this->dropTable('scalesqueleton');
    	$this->dropTable('evaluation');
    	$this->dropTable('patient');
    	$this->dropTable('coreset_icfitem_scaleitem');
    	//$this->dropTable('qualifier');
    	//$this->dropTable('scalequalifier');
    	$this->dropTable('scaleitem');
    	$this->dropTable('scale');
    	$this->dropTable('scalegroup');
    	//$this->dropTable('process');
    	$this->dropTable('icfitem');
    	$this->dropTable('coreset');
    	$this->dropTable('coresettype');
    	//$this->dropTable('migration');
    	echo "The migration was reverted!";
    }
}
