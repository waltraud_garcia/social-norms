<?php


namespace console\controllers;

use Yii;
use DateTime;
use yii\helpers\Html;
use yii\db\Connection;
use yii\helpers\Console;
use yii\console\Controller;
use common\modules\iguttmann\modules\scale\models\Scaleitem;
use common\modules\iguttmann\modules\scale\models\Scalevalue;
use common\modules\iguttmann\modules\codebar\models\Codebar;
use common\modules\iguttmann\modules\process\models\Evaluation;
use common\modules\iguttmann\modules\process\models\Adminscale;
use common\modules\iguttmann\modules\icf\models\CoresetIcfitemScaleitem;
use common\modules\iguttmann\modules\patient\models\Patient;


/**
 * Dump controller class that manage the extraction the guttmann data into cloudrehab database
 * @author dsanchez
 * @version 1.0
 *
 */
class CodebarController extends Controller{
	
	public function actionTest(){
		echo "Test";
	}
}