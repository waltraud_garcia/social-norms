<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use common\models\Role;
use common\models\User;
use common\modules\nam\models\event\Log;
use common\modules\nam\models\event\Event;
use common\modules\nam\models\content\Section;
use common\modules\nam\models\configuration\Configuration;
use common\modules\nam\controllers\OWAController;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = 'Norm crowsourcing backend';

$gridColumnsTotal = [
		['attribute'=>'idExperiment.description','label'=>'Experiment'],
		'id',
		['attribute'=>'event0.user0.username','label'=>'User'],
		['attribute'=>'event0.user0.role0.description','label'=>'Role'],
		['attribute'=>'section0.description','label'=>'Section'],
		['attribute'=>'event0.action0.description','label'=>'Action'],
		['attribute'=>'event0.content','label'=>'Content'],
		['attribute'=>'event0.content0.category','label'=>'C.type'],
		['attribute'=>'argument0.description','label'=>'Argument'],
		['attribute'=>'argument0.type0.description','label'=>'Argument type'],
		['attribute'=>'argument_rate','label'=>'Argument rate'],
		['attribute'=>'norm0.name','label'=>'Norm'],
		['attribute'=>'event0.infringedNorm','label'=>'InfringedNorm'],
		['attribute'=>'event0.fulfilledNorm','label'=>'FulfilledNorm'],
		['attribute'=>'event0.complainCategory','label'=>'ComplainCategory'],
		'date'

];


$gridColumns = [
		['attribute'=>'event0.user0.username','label'=>'User'],
		['attribute'=>'event0.user0.role0.description','label'=>'Role'],
		['attribute'=>'section0.description','label'=>'Section'],
		['attribute'=>'event0.action0.description','label'=>'Action'],
		['attribute'=>'event0.content','label'=>'Content'],
		['attribute'=>'event0.content0.category','label'=>'C.type'],
		['attribute'=>'argument0.description','label'=>'Argument'],
		['attribute'=>'argument0.type0.description','label'=>'A.type'],
		['attribute'=>'argument_rate','label'=>'A.rate'],
		['attribute'=>'norm','label'=>'Norm'],
		['attribute'=>'event0.infringedNorm','label'=>'InfringedNorm'],
		['attribute'=>'event0.fulfilledNorm','label'=>'FulfilledNorm'],
		['attribute'=>'event0.complainCategory','label'=>'ComplainCategory'],
		'date'

];

$query = Log::find()->orderBy('id_experiment');
$dProvider = new ActiveDataProvider(['query' => $query, 'pagination' => [
        'pageSize' => 10000,
    ],]);

$logMenu = ExportMenu::widget([
		'dataProvider' => $dProvider,
		'columns' => $gridColumnsTotal,
		// 		'hiddenColumns'=>[0, 3], // SerialColumn & ActionColumn
		// 		'disabledColumns'=>[1, 2], // ID & Name
		'fontAwesome' => true,
		'dropdownOptions' => ['label' => 'Export All','class' => 'btn btn-default'],
]) . "<hr>\n";
?>

<h2><?="Experiments ".$logMenu;?></h2>
<?php 
$experiments = Log::find()->select(['id_experiment'],'distinct')->all();
foreach($experiments as $e){?>
	<div class="col-xs-12 col-xs-offset-0 slide-row panel panel-default">
	
	<h2>
	<br>
	<?php $str = explode("--",$e->idExperiment['description']);?>
	<?php 
	foreach($str as $s){
		if (strpos($s,'Normal') !== false){?>
			<span class = "label label-success"><?=$s;?></span>
		<?php }else{?>
			<span class = "label label-danger"><?=$s;?></span>
		<?php }
	}
	?>
	</h2>
	<hr>	
	<h3> <?php 
	$query = Log::find()->where(['id_experiment' => $e['id_experiment']])->orderBy('date');
	$dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => [
        'pageSize' => 10000,
    ],]);

	echo "Log "?> </h3>
	
	<?php echo GridView::widget([
		'dataProvider'=> $dataProvider,
		'columns' => $gridColumns,
		'responsive'=>true,
		'hover'=>true,
		'summary'=>false,
	]);?>
	</div>
 <?php }
?>


