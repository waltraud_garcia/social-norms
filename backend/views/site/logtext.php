<?php
/* @var $this yii\web\View */

use common\models\User;
use common\models\Role;
use common\modules\nam\models\event\Log;
use common\modules\nam\models\event\Event;
use common\modules\nam\models\content\Section;
use common\modules\nam\models\configuration\Configuration;
use common\modules\nam\controllers\OWAController;

$this->title = 'Norm crowsourcing backend';
?>

<?php 
$experiments = Log::find()->select(['id_experiment'],'distinct')->all();
foreach($experiments as $e){?>	
	<h5>
	<br></br>
	<?= $e->idExperiment['description']. " experiment";?>
	</h5>
	<hr>
	<h3> <?= "Users"?> </h3>
		<?php 
		$roles = Role::find()->all();
		foreach($roles as $r){?>
			<h5>
			<?php echo "Role: ".$r['description']?>
			</h5>
			<?php $users = User::find()->where(['role' => $r['id']])->all();
			foreach($users as $u){?>
				
				<?php echo "Username: ".$u['username']?>
				
				<?php }
		}
		?>
		
	<br>
	<h3> <?= "Log"?> </h3>
	<?php 
	$logs = Log::find()->where(['id_experiment' => $e['id_experiment']])->orderBy('date')->all();
	foreach($logs as $l){?>
		<h6>
		<?php echo "User: ".$l->event0->user0['username']." Role:".$l->event0->user0->role0['description']?>
		<?php echo "Section:".$l->section0['description']." Content:".$l->event0['content']." Action: ".$l->event0->action0['description'];?>
		<?php  echo "InfringedNorm:".$l->event0['infringedNorm']." FulfilledNorm:".$l->event0['fulfilledNorm']." ComplainCategory:".$l->event0['complain_category'];?>
		</h6>
	<?php }?>
<?php }
?>

