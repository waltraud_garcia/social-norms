/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : yii2advanced

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-03-14 16:24:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `action`
-- ----------------------------
DROP TABLE IF EXISTS `action`;
CREATE TABLE `action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of action
-- ----------------------------
INSERT INTO `action` VALUES ('1', 'upload');
INSERT INTO `action` VALUES ('2', 'view content');
INSERT INTO `action` VALUES ('3', 'complain');
INSERT INTO `action` VALUES ('4', 'notUpload');
INSERT INTO `action` VALUES ('5', 'view section');
INSERT INTO `action` VALUES ('6', 'create norm');
INSERT INTO `action` VALUES ('7', 'discuss norm');
INSERT INTO `action` VALUES ('8', 'add argument');
INSERT INTO `action` VALUES ('9', 'rate argument');
INSERT INTO `action` VALUES ('10', 'active norm');

-- ----------------------------
-- Table structure for `argument`
-- ----------------------------
DROP TABLE IF EXISTS `argument`;
CREATE TABLE `argument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `norm` int(20) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `rate` double DEFAULT NULL,
  `numrate` int(11) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `norm` (`norm`),
  KEY `user` (`user`),
  KEY `type` (`type`),
  CONSTRAINT `argument_ibfk_1` FOREIGN KEY (`norm`) REFERENCES `norm` (`id`),
  CONSTRAINT `argument_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `argument_ibfk_3` FOREIGN KEY (`type`) REFERENCES `argument_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of argument
-- ----------------------------

-- ----------------------------
-- Table structure for `argument_rate`
-- ----------------------------
DROP TABLE IF EXISTS `argument_rate`;
CREATE TABLE `argument_rate` (
  `id_argument` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_argument`,`id_user`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `argument_rate_ibfk_1` FOREIGN KEY (`id_argument`) REFERENCES `argument` (`id`),
  CONSTRAINT `argument_rate_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of argument_rate
-- ----------------------------

-- ----------------------------
-- Table structure for `argument_type`
-- ----------------------------
DROP TABLE IF EXISTS `argument_type`;
CREATE TABLE `argument_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of argument_type
-- ----------------------------
INSERT INTO `argument_type` VALUES ('1', 'positive');
INSERT INTO `argument_type` VALUES ('2', 'negative');

-- ----------------------------
-- Table structure for `complain`
-- ----------------------------
DROP TABLE IF EXISTS `complain`;
CREATE TABLE `complain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  `complain_category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `complain_category` (`complain_category`),
  CONSTRAINT `complain_ibfk_1` FOREIGN KEY (`complain_category`) REFERENCES `complain_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of complain
-- ----------------------------

-- ----------------------------
-- Table structure for `complain_category`
-- ----------------------------
DROP TABLE IF EXISTS `complain_category`;
CREATE TABLE `complain_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of complain_category
-- ----------------------------
INSERT INTO `complain_category` VALUES ('1', 'Spam');

-- ----------------------------
-- Table structure for `configuration`
-- ----------------------------
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `id_experiment` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_experiment`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of configuration
-- ----------------------------
INSERT INTO `configuration` VALUES ('4', 'disscussion test', '1');

-- ----------------------------
-- Table structure for `content`
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_of_views` int(11) DEFAULT NULL,
  `num_of_complaints` int(11) DEFAULT NULL,
  `violated_norm` int(11) DEFAULT NULL,
  `actualdate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `section` (`section`),
  KEY `owner` (`owner`),
  CONSTRAINT `content_ibfk_1` FOREIGN KEY (`category`) REFERENCES `content_category` (`id`),
  CONSTRAINT `content_ibfk_2` FOREIGN KEY (`section`) REFERENCES `section` (`id`),
  CONSTRAINT `content_ibfk_3` FOREIGN KEY (`owner`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES ('1', '1', '1', '1', 'Look inside this message!', 'Text', '', 'You have been cursed, the amazing King Butterfly will come after you and suck the nectar of the flowers of your garden, this is not any kind of innuendo. In case you do not want this horrific scene to happen you have to post this message at 10 other forums.', '0', '0', null, '2015-03-23 02:41:04');
INSERT INTO `content` VALUES ('2', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('3', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('4', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('5', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('6', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('7', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('8', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('9', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('10', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('11', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('12', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('13', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('14', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('15', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('16', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('17', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('18', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('19', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('20', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('21', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('22', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('23', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('24', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('25', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('26', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('27', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('28', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('29', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('30', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('31', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('32', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('33', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('34', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('35', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('36', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('37', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('38', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('39', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('40', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('41', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('42', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('43', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('44', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('45', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('46', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('47', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('48', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('49', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('50', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('51', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('52', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('53', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('54', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('55', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('56', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('57', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('58', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('59', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('60', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('61', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('62', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('63', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('64', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('65', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('66', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('67', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('68', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('69', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('70', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('71', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('72', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('73', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('74', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('75', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('76', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('77', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('78', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('79', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('80', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('81', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('82', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('83', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('84', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('85', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('86', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('87', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('88', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('89', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('90', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('91', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('92', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('93', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('94', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('95', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('96', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('97', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('98', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('99', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('100', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('101', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('102', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('103', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('104', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('105', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('106', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('107', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('108', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('109', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('110', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('111', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('112', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('113', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('114', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('115', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('116', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('117', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('118', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('119', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('120', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('121', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('122', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('123', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('124', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('125', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('126', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('127', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('128', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('129', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('130', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('131', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('132', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('133', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('134', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('135', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('136', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('137', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('138', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('139', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('140', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('141', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('142', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('143', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('144', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('145', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('146', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('147', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('148', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('149', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('150', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('151', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('152', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('153', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('154', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('155', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('156', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('157', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('158', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('159', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('160', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('161', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('162', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('163', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('164', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('165', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('166', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('167', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('168', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('169', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('170', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('171', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('172', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('173', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('174', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('175', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('176', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('177', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('178', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('179', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('180', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('181', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('182', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('183', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('184', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('185', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('186', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('187', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('188', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('189', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('190', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('191', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('192', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('193', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('194', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('195', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('196', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('197', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('198', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('199', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('200', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('201', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('202', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('203', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('204', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('205', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('206', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('207', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('208', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('209', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('210', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('211', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('212', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('213', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('214', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('215', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('216', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('217', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('218', '1', '1', null, 'Free passes for the next Championsleague competition', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('219', '1', '1', null, 'Free tickets for the next Realmadrid match', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('220', '1', '1', null, 'The real ball of FBC signed by Messi', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('221', '1', '1', null, 'The signed t-shirt by Cristiano ronaldo', 'Text', '', 'Hi, a few days ago I bought some tickets for the next Championleague competition thinking I would be enjoying it with all of my friends; sadly, most of them died in an jet accident. Since I can\'t sell this tickets I will willingly give these to the 5 person that visits my \'totally not a rip off\' website! You just have to click to link below and Santa will give one for you: http:\\www.this_site_does_not_exist.com  And have a nice day!', '0', '0', null, '');
INSERT INTO `content` VALUES ('222', '1', '2', null, 'Aberdeen Season Tickets up 35%', 'Text', '', 'Speaking to a guy at the ticket office, he was saying the sales have been going mad since the announcement about Rangers. They have calculated the extra 3,500 season tickets more than offset any reduced income over Rangers. Its brilliant to see the clubs really prove to them how much they are NOT the people. In a related point both Dundee and Dundee United announced that their Tayside Derby will actually results in bigger crowds than either would have expected against Rangers, and St Johnstone too, being on', '0', '0', null, '');
INSERT INTO `content` VALUES ('223', '1', '2', null, 'SELLING OF AUTOGRAPH OF FOOTBALL PLAYERS', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('224', '1', '2', null, 'SELLING OF AUTOGRAPH OF MESSI', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('225', '1', '2', null, 'Selling my Renault Clio', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('226', '1', '2', null, 'Visit my page a buy something', 'Text', '', 'Selling autographs of your favourite football players: - Messi - Christiano Ronaldo - A lot more!Just chuck our goods in http://www.i_m_bored_of_this_fake_sites.net', '0', '0', null, '');
INSERT INTO `content` VALUES ('227', '1', '3', null, 'Spam', 'Image', 'images\\content\\spam.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('228', '1', '3', null, 'Premier T-shirts very cheaper!', 'Image', 'images\\content\\tshirt1.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('229', '1', '3', null, 'Catalona T-shirt 2euros', 'Image', 'images\\content\\tshirt2.jpg', 'I have discovered an amazing way to earn money, not discovered until today. You will have to study hard at the beginning but after the firsts phases you will not do a thing anymore. To discover how to be happy for ever just click in the next link: http://www.seriously_do_not_belive_these_methods.com', '0', '0', null, '');
INSERT INTO `content` VALUES ('230', '2', '1', null, 'Can Neymar Become The Greatest Player In The World?', 'Text', '', 'I haven\'t seen much of him, but he appears to be playing very well for Barcelona. For those of you who have have seen him play, does he have the potential to reach the level of Messi and Ronaldo', '0', '0', null, '');
INSERT INTO `content` VALUES ('231', '2', '1', null, 'Sven In China', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('232', '2', '1', null, 'How many golden ball Messi will obtained?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('233', '2', '1', null, 'Who is the most football player from Japan?', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('234', '2', '1', null, 'Football in Brasil', 'Text', '', 'Sven has had a successful first season in China. His team, Guangzhou R&F, finished 6th, one position better than the 7th place they achieved in 2012, and the season before that they were in the 2nd division. This is their best season since 2003. A wonderful achievement by Sven-Goran Eriksson.', '0', '0', null, '');
INSERT INTO `content` VALUES ('235', '2', '2', null, 'What is the most spectacular goal?', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('236', '2', '2', null, 'How to improve your technique', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('237', '2', '2', null, 'How to make ronaldinho tricks', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('238', '2', '2', null, 'Is Cristano better player rather than Messi', 'Text', '', 'I watched Celtic\'s champions league qualifier last night. It was a really good game and I was happy to see them qualified for the group stage. But, it was against a team from Kazakhstan. No offence to them but they were not even a big football country when they played in Asia. Still, they had a good chance to knock out Celtic, who were once the champions of Europe. I can\'t help wondering why Scottish clubs are not as good in Europe as they were a few years ago. And that\'s why their champions had to play a l', '0', '0', null, '');
INSERT INTO `content` VALUES ('239', '2', '2', null, 'Your sport is SOCCER, learn the difference', 'Text', '', 'Just accept it, football is the sport of America, mother of the Liberty and the Justice. Soccer, the name you hate so much but the one that you deserve, is the sport of Granny Europe, our ancestor had to flee of your reign of terror and dictatorship. Later we save your asses 2 times, you are welcomed Sir!', '0', '0', null, '');
INSERT INTO `content` VALUES ('240', '2', '3', null, 'Tutorial: How to do the Ronaldo trick', 'Image', 'images\\content\\funny1.jpg', '', '0', '0', null, '');
INSERT INTO `content` VALUES ('241', '2', '3', null, 'Tutorial: How to do the Cristiano trick', 'Video', 'images\\content\\funny2.jpg', '', '0', '0', null, '');

-- ----------------------------
-- Table structure for `content_category`
-- ----------------------------
DROP TABLE IF EXISTS `content_category`;
CREATE TABLE `content_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of content_category
-- ----------------------------
INSERT INTO `content_category` VALUES ('1', 'Spam');
INSERT INTO `content_category` VALUES ('2', 'OK');

-- ----------------------------
-- Table structure for `event`
-- ----------------------------
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `content` int(11) DEFAULT NULL,
  `action` int(11) NOT NULL,
  `complain_category` int(11) DEFAULT NULL,
  `infringedNorm` int(11) DEFAULT NULL,
  `fulfilledNorm` int(11) DEFAULT NULL,
  `checked` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `content` (`content`),
  KEY `action` (`action`),
  KEY `complain_category` (`complain_category`),
  KEY `infringedNorm` (`infringedNorm`),
  KEY `fulfilledNorm` (`fulfilledNorm`),
  CONSTRAINT `event_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `event_ibfk_2` FOREIGN KEY (`content`) REFERENCES `content` (`id`),
  CONSTRAINT `event_ibfk_3` FOREIGN KEY (`action`) REFERENCES `action` (`id`),
  CONSTRAINT `event_ibfk_4` FOREIGN KEY (`complain_category`) REFERENCES `complain_category` (`id`),
  CONSTRAINT `event_ibfk_5` FOREIGN KEY (`infringedNorm`) REFERENCES `norm` (`id`),
  CONSTRAINT `event_ibfk_6` FOREIGN KEY (`fulfilledNorm`) REFERENCES `norm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of event
-- ----------------------------

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_experiment` int(11) DEFAULT NULL,
  `event` int(11) DEFAULT NULL,
  `argument` int(11) DEFAULT NULL,
  `argument_rate` int(11) DEFAULT NULL,
  `norm` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event` (`event`),
  KEY `section` (`section`),
  KEY `argument` (`argument`),
  KEY `norm` (`norm`),
  KEY `id_experiment` (`id_experiment`),
  CONSTRAINT `log_ibfk_1` FOREIGN KEY (`event`) REFERENCES `event` (`id`),
  CONSTRAINT `log_ibfk_2` FOREIGN KEY (`section`) REFERENCES `section` (`id`),
  CONSTRAINT `log_ibfk_3` FOREIGN KEY (`argument`) REFERENCES `argument` (`id`),
  CONSTRAINT `log_ibfk_4` FOREIGN KEY (`norm`) REFERENCES `norm` (`id`),
  CONSTRAINT `log_ibfk_5` FOREIGN KEY (`id_experiment`) REFERENCES `configuration` (`id_experiment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) NOT NULL,
  `translation` text,
  PRIMARY KEY (`id`,`language`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1457969019');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1457969071');

-- ----------------------------
-- Table structure for `modality`
-- ----------------------------
DROP TABLE IF EXISTS `modality`;
CREATE TABLE `modality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modality
-- ----------------------------
INSERT INTO `modality` VALUES ('1', 'Obligation');
INSERT INTO `modality` VALUES ('2', 'Phohibition');

-- ----------------------------
-- Table structure for `norm`
-- ----------------------------
DROP TABLE IF EXISTS `norm`;
CREATE TABLE `norm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `complain_category` int(11) DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  `action` int(11) NOT NULL,
  `rating` double DEFAULT NULL,
  `ratingpos` double DEFAULT NULL,
  `ratingneg` double DEFAULT NULL,
  `closing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `complain_category` (`complain_category`),
  KEY `section` (`section`),
  KEY `modality` (`modality`),
  KEY `action` (`action`),
  CONSTRAINT `norm_ibfk_1` FOREIGN KEY (`complain_category`) REFERENCES `complain_category` (`id`),
  CONSTRAINT `norm_ibfk_2` FOREIGN KEY (`section`) REFERENCES `section` (`id`),
  CONSTRAINT `norm_ibfk_3` FOREIGN KEY (`modality`) REFERENCES `modality` (`id`),
  CONSTRAINT `norm_ibfk_4` FOREIGN KEY (`action`) REFERENCES `action` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of norm
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Demo');
INSERT INTO `role` VALUES ('2', 'Spammer');
INSERT INTO `role` VALUES ('3', 'Ok');

-- ----------------------------
-- Table structure for `section`
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', 'Reporter');
INSERT INTO `section` VALUES ('2', 'Forum');
INSERT INTO `section` VALUES ('3', 'Image & Video');
INSERT INTO `section` VALUES ('4', 'Reporter, Forum or Image/Video');

-- ----------------------------
-- Table structure for `source_message`
-- ----------------------------
DROP TABLE IF EXISTS `source_message`;
CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of source_message
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) DEFAULT '1',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'normal10', 'XtlzrvhjE9BpgZ1geWYK7VednElbehbV', '$2y$13$VESoP4aCPVQLY10ODGYDZu04fOehmrGnm/qy0r70v8OJOUzSb/PqO', null, 'normal1@normal1.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('2', 'normal11', 'hqrJ0bLNnPGfRKRfBPnlTRNohTL7p-Ar', '$2y$13$vnGv23E834XkbZPkXNPCt.QpNzcZ7wBnCEJS02C.9bm3wSS2dV/Gi', null, 'normal2@normal2.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('3', 'normal12', 'SkbD3UsaiMtXzE4-w0cQmk29OrJzIbq6', '$2y$13$BnO/VG1MZn9iSi0O76IAv.yFqJ5mIjFfUJJt2voeU1yv6dHkTXHDq', null, 'normal3@normal3.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('4', 'normal13', 'H2OjCd-HGVXTddp4YdrLW4fTHiTQ7ymd', '$2y$13$jNe7USVG8rerbyxNDN21d.KcAbqvkPK5XmGD1TTgFMYW9sASgU6Dm', null, 'normal4@normal4.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('5', 'normal14', 'C132-N42AEmS0keeFdEOEQC5byEi8mB4', '$2y$13$rgSWMqmp6i0RbVB1/KQaj.OLK0zd9d8/E6Cct.pNrK3qfJVRifCWy', null, 'normal5@normal5.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('6', 'normal15', 'gYa5niuHr7GjKZkw9RMRUwIuTGJ471K4', '$2y$13$K/ZCLUjcwHaA/sUWkYLjce3xO2MhsogZ6ah2avVX3Ukxi.XUuh8v6', null, 'normal6@normal6.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('7', 'normal16', 'y9BU1-tE8w1LRdxJ2owjc0TgpPvn7cPU', '$2y$13$.2rccZ3PCPUXL9p78sHIueY8hrEP6rtxr8zg9UEKIRD3eapF7GYiq', null, 'normal7@normal7.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('8', 'normal17', 'lgXQGOkkhV9xGU0OLrY6xeNd0X5_Z-Dk', '$2y$13$VbkmfUeoiq00EKPi2EMTeOTheRSik1LExmd70fIE2cW/TVjBhEaLG', null, 'normal8@normal8.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('9', 'normal18', 'buvxB_DoykjVWFoKLYxArrF41dSMk-yZ', '$2y$13$pObbvrpBPq4xZcYRwPC.1eVvnXrlKBbPz/43rSZlQqGR4WcKCoqyK', null, 'normal9@normal9.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('10', 'normal19', '0qydqJ1Kb74kktznFaxvdlsTRAD1DaN1', '$2y$13$vgXwlt4.eBK9CVcjeoGuUu9N6shin79ByDBU3f4Z0xmSU196NhHCq', null, 'normal01@normal10.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('11', 'normal20', 'ANz3tbRncr1SvcfRXGZHaz8oFpVOA4zv', '$2y$13$5GJIOzw/ew5R2NNwhpkUseWwePiKzuiJrZJA0.u2bSwmCd8BtNFbS', null, 'normal1@normal1.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('12', 'normal21', 'lS3dyYYuS4Q_6zVuiZp7rYMTZ7DMK9pE', '$2y$13$dSoGfhGd9J9amMI7BnqQFuP/05gFMD0VaYgNKk/nDE.lcn6qNT0.K', null, 'normal2@normal2.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('13', 'normal22', '594ZVtj4hyxLLOyCiijBG4IXr9S8NoHC', '$2y$13$wf7QSd.YOkJAEnT.0R4pu.H6U6Zv9L3RIxExULSbd3eo8yV1yB7ae', null, 'normal3@normal3.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('14', 'normal23', '8tz7GglRqSa7iylOBuFSHj77kwzMz4js', '$2y$13$5H6KU5uq3TZ/Z1myVKSQ0.rrVssZslVOYeurcgaIaWXPUyelJTDtS', null, 'normal4@normal4.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('15', 'normal24', 'CiJjKvIOT_ObOaz2z53eIM-zLdrxYUr1', '$2y$13$FSduEhmAhaaBXXVz2gRcje7G5UuU.EN.0zSrp3yFbCJtD1ZaigS3.', null, 'normal5@normal5.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('16', 'normal25', 'HJPesPdEjqioSjstkPjC1kNwR14JiCn1', '$2y$13$ECzg0CRnjpJEwjpHtLIBv.woGnctwUpefFTa1h6oetROWdGcL7adq', null, 'normal6@normal6.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('17', 'normal26', 'mOWPbexnoLlUg-N-MzapZYdf9ZoJ9ccH', '$2y$13$2wX8/NHFqb5Mp/rCCDzPHOLkKaweKk/KpryVMQyY1OT7ZEEv3x/ey', null, 'normal7@normal7.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('18', 'normal27', '_dPtl_vmnl_KxgPvZt0goKY2kuFpvYRz', '$2y$13$/Lqbi82QnCXB5Erdcb2Izu1BrhP3m49JWUoyBAP5rw7j5ot4pl4fC', null, 'normal8@normal8.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('19', 'normal28', 'HHAnr44B25tDx6WvnuRIwJd9VYrWJHVz', '$2y$13$cN9lr473CzLYFxD0wDQVm.TFO32DRy10r9rSSH81ydPhebixbyH..', null, 'normal9@normal9.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('20', 'normal29', 'OWyuCnabyvKCutk-II0VaL9ow3rqnHsm', '$2y$13$ZIqD5r.j4ORa18dKKrdiTulK5sq4nbX1GvVG8GBWcFbH7Xxx9G0wK', null, 'normal01@normal10.com', '2', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('21', 'spammer10', 'dKxcOs65TMC6FeBIe8LiTkZjjUjzMBuD', '$2y$13$QV/mpxN18vsRtY.VCU668OF/4PFKwQRP1xmtpXA0is2T6H4LymXA.', null, 'spam1@spam1.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('22', 'spammer11', 'UACGvGXrTgAQKTvSECSAQWqfPI333nhv', '$2y$13$wjkCZ7iGoERjF2obzGgU7eQ1gFoNBLGqbSIzBMCcZg4P7tmpCqpYO', null, 'spamermer2@spamer2.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('23', 'spammer12', '8gdSujmwfFsSqlJkemt6arT3mJSLsS23', '$2y$13$gJZe9HLuqoob8HIh5ovQZO8ngmjmyKfjn.3xhHJmIZQwseFdFy3L.', null, 'spamer3@spamer3.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('24', 'spammer13', 'r3wC4B_wLpnJaqFdR4o_iuxY8blUitXk', '$2y$13$AOjG661YCmyZVAHEovtc0.hYUDDK.8hAdmzuuy9GGx/MpDpsQ4h7q', null, 'spamer4@spamer4.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('25', 'spammer14', 'Uhz2sVdfGmkfdg0NaoMLj4eJ9loo1ckK', '$2y$13$cOVin7u9cYGq63PpFw6QyOpDMgcug0gsDkYglcMf5H6wPXNWSgfam', null, 'spamer5@spamer5.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('26', 'spammer15', 'MmTAnG8oEztFrPDsrVYP_0Zo-2_F2Gfh', '$2y$13$YWJi7.P9keYBJqWysCXKyeUzN4NMV8xkZfRUfoOxaHvdI0SB1lVcC', null, 'spamer6@spamer6.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('27', 'spammer16', '4FrC4fBTigjf4FQOH6vsDUakqi7y9XSC', '$2y$13$pTe7DNFL0h0RKJ8LPzkzU./lxBZgyPSVulv0pIDPfVHoBnlkMM59y', null, 'spamer7@spamer7.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('28', 'spammer17', 'wnW2MZAFscNM_L-McDgIS5fMsBF_KwKQ', '$2y$13$heKpNneC01dVTQH3saffb.5o078TjAyFJfrkHgXgrEPHW8jWZxM0m', null, 'spamer8@spamer8.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('29', 'spammer18', 'x3laoPusAShoTpECN45PxBwZXu9p4k34', '$2y$13$f8D5tGuqZSnS3gi.aenvNekptQTds.eYoYGqT4QSGrZLxenvR5yc6', null, 'spamer9@spamer9.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('30', 'spammer19', 'YgrrU0_fbm7KZNRjS4vh2L0aAAiJuxya', '$2y$13$dpkzA0RXhy4i5lxP.LI7fen1d5NhMamP3nBD7rX6n/symFH6kkShS', null, 'spamer10@spamer10.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('31', 'spammer20', 'wwfnrQyTQBZKkCXNQW3yaY5VRtDJ2OOo', '$2y$13$HBnECL71O8GL.UaKxSdevekh4CdhUiiDIqET89RJLH2Y10udibDgi', null, 'spamer1@spamer1.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('32', 'spammer21', 'mDGJAqclGztPyArQ-fYwDoZDLgJbepWm', '$2y$13$oGTo7DdN6fisadymJ0a3d..LgVqxCY5zeQukphHNRXYe6qN.GJWpq', null, 'spamer2@spamer2.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('33', 'spammer22', 'j2ERZhCtvCYcYEPaM5Q1ClAgQwUP_Zwf', '$2y$13$0Ug/sPgAY1nO1k/vSX7ur.aBInsfU4fxSORKmCZ0ENK6CRJH2CCu6', null, 'spamer3@spamer3.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('34', 'spammer23', 'DhsXsfnNLqSSDLRMUeqP0BSXzulQrAXu', '$2y$13$v6eGzeRvB0m.qUGFRSBHGOzzLhUNDymO90W5Hzw0LW0zYBWv6VyK6', null, 'spamer4@spamer4.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('35', 'spammer24', 'T4J4tiRQwwr6W3lssUAXl7IQpl9ir2oG', '$2y$13$qODFPAgO03dT2DaWyeLvF.0uAbdk6xES0f5dRJUqAJiOIebBeHd.C', null, 'spamer5@spamer5.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('36', 'spammer25', '_shfDk3oVYqnUAOmSZ1JLEuqtz-Yr8Px', '$2y$13$u0EAZsulbbmrnslGl4Q8Xu7NMJat7XyBAoZCttryiNMQ5YycEzRNe', null, 'spamer6@spamer6.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('37', 'spammer26', 'TtgvDKhpcTgvK3r9u-MgAgeB5CUfstbU', '$2y$13$g4Rfgr543BM7/jVK.y5Uu.3D0E1PkCCaveia3Ywluf/fzkr8qe8Fe', null, 'spamer7@spamer7.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('38', 'spammer27', 'Rq3sbVP5XiGRsnS_OWQJmNUCqPRWU6-u', '$2y$13$jJSZkV.MwvFaZM8wMmi2IuFecsfRJaUVlsh0tQ7o44qPFjHOR4m4m', null, 'spamer8@spamer8.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('39', 'spammer28', '4usXbjHDnNHtQ9bRi_bTHsQH3XOLy6RU', '$2y$13$mSTwFrhkOLMW1qq8sGqITuuwwhXrd.aJtxxq3aM2112f/8coQiObi', null, 'spamer9@spamer9.com', '3', '10', '1423586410', '1423586410');
INSERT INTO `user` VALUES ('40', 'spammer29', '0Gzov3RY_0bUVnouIASZYTqiCDfyZPTa', '$2y$13$4A2iYsG7ED/4H1LerjPBRuQEFkyBig.pSqKePNjQ469tIcs4gPOru', null, 'spamer10@spamer10.com', '3', '10', '1423586410', '1423586410');

-- ----------------------------
-- Table structure for `view`
-- ----------------------------
DROP TABLE IF EXISTS `view`;
CREATE TABLE `view` (
  `user` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  PRIMARY KEY (`user`,`content`),
  KEY `content` (`content`),
  CONSTRAINT `view_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `view_ibfk_2` FOREIGN KEY (`content`) REFERENCES `content` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of view
-- ----------------------------
