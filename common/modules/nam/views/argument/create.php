<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\nam\models\norm\Argument */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Argument',
]);
?>
<br><br>
<div class="argument-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
