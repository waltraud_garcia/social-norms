<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\nam\models\norm\Argument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="argument-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 100])->label('Argument'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('button', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
