<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\nam\models\norm\Argument */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Arguments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="argument-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'norm',
            'description',
            'user',
            'type',
            'rating',
        ],
    ]) ?>

</div>
