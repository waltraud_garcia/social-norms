<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\nam\models\norm\Argument */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Argument',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Arguments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="argument-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
