<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Norm */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Norm',
]);
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Norms'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="norm-create">

    <h1><?=Yii::t('title',Html::encode($this->title)) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
