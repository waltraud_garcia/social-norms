<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\nam\models\norm\Argument;
use common\modules\nam\models\content\Content;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use kartik\popover\PopoverX;
use common\modules\nam\models\norm\ArgumentRate;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>



<br><br>
<div class="tree">
    <ul>
		<li>
			<span><i class="glyphicon glyphicon-list"></i>
			<?=Yii::t('norm',$model['name']);?>
			<span title="Category" class="label label-danger"><?= Html::encode($model->categories['description']);?></span>
			<?=Yii::t('norm','at');?>
			<span title="Section" class="label label-default"><?= Html::encode($model->sections['description']);?></span>
			<?=Yii::t('norm','section');?>
			</span>
			<?php generateStarIcons($model['id']);?>
			<ul>
                <li>
                	<span class="label label-success"><i class="icon-minus-sign"></i><?=Yii::t('norm','Positive arguments')?></span> <?php generateStarIcons($model['ratingpos']);?>  <?= Html::a(Yii::t('button', 'Add'), ['norm/addargument', 'norm' => $model['id'], 'positive' => 1, 'category' => $model->content_category, 'section' => $model->section], ['class' => 'label label-primary']);?>
                    <ul>
                    	<?php
                    	showArguments(1, $model);
                  		?>
                    </ul>
                </li>
                <li>
                	<span class="label label-danger"><i class="icon-minus-sign"></i><?=Yii::t('norm','Negative arguments')?></span> <?php generateStarIcons($model['ratingneg']);?>  <?= Html::a(Yii::t('button', 'Add'), ['norm/addargument', 'norm' => $model['id'], 'positive' => 0, 'category' => $model->content_category, 'section' => $model->section], ['class' => 'label label-primary']);?>
                    <ul>
						<?php
                    	showArguments(0, $model);
                  		?>
                    </ul>
                </li>
           </ul>
		</li>
	</ul>
</div>

<?php
/**
 * Function to show the arguments of the norm
 * @param unknown $type Type of the argument
 * @param unknown $model Norm model
 */
function showArguments($type, $model){
	$sql = 'select * from argument where type = '.$type.' && norm = '.$model['id'];
	$query = Argument::findBySql($sql);
	$dataProvider = new ActiveDataProvider(['query' => $query]);
	$models = $dataProvider->models;
	$rate = 0;
	foreach($models as $modelArgument){
		?><li>
			<?php
			$description = $modelArgument['description'];
			if(is_numeric($description)){
				$content = Content::findOne($description);
				echo 'Content such as '.Html::a(Html::encode($content['title']), ['/nam/content/view', 'id' => $modelArgument['description']], ['class' => '']) .' should not be published to upload this content ';
				echo generateStarIcons($modelArgument['rate']).'  ('.$modelArgument['numrate']?><i class="fa fa-user"></i><?php echo ')' ?>
			<?php }else{
				echo Html::encode($description)." ";
				echo generateStarIcons($modelArgument['rate']).'  ('.$modelArgument['numrate']?><i class="fa fa-user"></i><?php echo ')' ?>
			<?php }
			?>
	       <?php $content = '<form name="form" action="" method="get"> <input class="rating" data-max="5" data-min="1" id="input" type="number"/> </form>' ;?>
           <?php
	           if(ArgumentRate::isRated($modelArgument['id'])){
	           ?>	<span>Rate argument:<?php
	           	echo Html::a(Yii::t('button', '1'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>1, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->content_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '2'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>2, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->content_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '3'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>3, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->content_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '4'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>4, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->content_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '5'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>5, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->content_category], ['class' => 'glyphicon glyphicon-star']);
	           	?></span>
	           	<?php
	           }
	           ?>
	   	</li>
<?php }
}



function generateStarIcons($number){
	//Full star
	$num = floor($number);
	$rest = $number-$num;
	for($i=0; $i<$num; $i++){
		echo '<i class="fa fa-star yellow"></i>';
	}

	//Half star
	if($rest >= 0.5){
		echo '<i class="fa fa-star-half-empty yellow"></i>';
		$i++;
	}

	//Empty star
	for($j=$i; $j<5; $j++){
		echo '<i class="fa fa-star-o yellow"></i>';
	}
}
?>

<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
