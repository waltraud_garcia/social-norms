<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\nam\models\norm\Argument;
use common\modules\nam\models\content\Content;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use kartik\popover\PopoverX;
use common\modules\nam\models\norm\ArgumentRate;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<br><br>
<h4><?php echo "Norm     ";
if($model->active==0){?>
	<span class="label label-default"><?= "Under discussion" ?></span>
<?php
		echo '<small>Time Remaining:'.gmdate('H:i',$model->closing-time()).'</small>';
}else if($model->active==2){?>
	<span class="label label-danger"><?= "Closed" ?></span>
	<?php
}else{?>
	<span class="label label-success"><?= "Active" ?></span>
<?php
}?>

<span class="label label-success"></span>
</h4>
<div class="tree">
    <ul>
		<span><?=Yii::t('norm','Proposed Norm:')?></span>
		<li>
		
			<span><i class="glyphicon glyphicon-list"></i>
			<?=Yii::t('norm',$model['name']);?>
			<span title="Category" class="label label-danger"><?= Html::encode($model->complainCategory['description']);?></span>
			<?=Yii::t('norm','at');?>
			<span title="Section" class="label label-default"><?= Html::encode($model->section0['description']);?></span>
			<?=Yii::t('norm','section');?>
			</span>
			<?php generateStarIcons($model['rating']);?>

			<br></br>
			<!--<ul>-->
			<ul>
				<span id="arguments">
                <!--<li>-->
                	<span class="label label-success"><i class="icon-minus-sign"></i><?=Yii::t('norm','Arguments in favour')?></span> <?php generateStarIcons($model['ratingpos']);?>  <?php if(!$model->active) echo Html::a(Yii::t('button', 'Add'), ['norm/addargument', 'norm' => $model['id'], 'positive' => 1, 'category' => $model->complain_category, 'section' => $model->section], ['class' => 'label label-primary']);?>
                    <ul>
                    	<?php
                    	showArguments(1, $model);
                  		?>
                    </ul>
                <!--</li>-->
				</span>
				<span id="arguments">
                <!--<li>-->
                	<span class="label label-danger"><i class="icon-minus-sign"></i><?=Yii::t('norm','Arguments against')?></span> <?php generateStarIcons($model['ratingneg']);?>  <?php if(!$model->active) echo Html::a(Yii::t('button', 'Add'), ['norm/addargument', 'norm' => $model['id'], 'positive' => 2, 'category' => $model->complain_category, 'section' => $model->section], ['class' => 'label label-primary']);?>
                    <ul>
						<?php
                    	showArguments(2, $model);
                  		?>
                    </ul>
                <!--</li>-->
				</span>
			</ul>
            <!--</ul>-->
		</li>
	</ul>
</div>
