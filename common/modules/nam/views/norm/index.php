<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use common\modules\nam\models\norm\Argument;
use yii\data\ActiveDataProvider;
use common\modules\nam\models\content\Content;
use common\modules\nam\models\norm\ArgumentRate;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('norm', 'Norms');
// $this->params['breadcrumbs'][] = $this->title;
?>
<br><br>
<?= Html::a(Yii::t('button', 'Create norm'), ['norm/createnorm'], ['class' => 'btn btn-primary']);?>
<br><br>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
        	return $this->render('view',['model' => $model, 'section' => 1, 'status' => 1]);
        },
        'emptyText' => '',

        'layout' => '{items}{pager}'])
     ?>

<?php
/**
 * Function to show the arguments of the norm
 * @param unknown $type Type of the argument
 * @param unknown $model Norm model
 */
function showArguments($type, $model){
	$sql = 'select * from argument where type = '.$type.' && norm = '.$model['id'];
	$query = Argument::findBySql($sql);
	$dataProvider = new ActiveDataProvider(['query' => $query]);
	$models = $dataProvider->models;
	$rate = 0;
	foreach($models as $modelArgument){
		?><li>
			<?php
			$description = $modelArgument['description'];
			if(is_numeric($description)){
				$content = Content::findOne($description);
				echo 'Content such as '.Html::a(Html::encode($content['title']), ['/nam/content/view', 'id' => $modelArgument['description']], ['class' => '']) .' should not be published to upload this content ';
				echo generateStarIcons($modelArgument['rate']).'  ('.$modelArgument['numrate']?><i class="fa fa-user"></i><?php echo ')' ?>
			<?php }else{
				echo Html::encode($description)." ";
				echo generateStarIcons($modelArgument['rate']).'  ('.$modelArgument['numrate']?><i class="fa fa-user"></i><?php echo ')' ?>
			<?php }
			?>
	       <?php $content = '<form name="form" action="" method="get"> <input class="rating" data-max="5" data-min="1" id="input" type="number"/> </form>' ;?>
           <?php
	           if(ArgumentRate::isRated($modelArgument['id']) && (!$model->active)){
	           ?>	<span>Rate argument:<?php
	           	echo Html::a(Yii::t('button', '1'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>1, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->complain_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '2'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>2, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->complain_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '3'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>3, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->complain_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '4'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>4, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->complain_category], ['class' => 'glyphicon glyphicon-star']);
	           	echo Html::a(Yii::t('button', '5'), ['norm/rate', 'id_argument' => $modelArgument['id'], 'rating' =>5, 'norm' => $model['id'], 'section' => $model->section, 'category' => $model->complain_category], ['class' => 'glyphicon glyphicon-star']);
	           	?></span>
	           	<?php
	           }
	           ?>
	   	</li>
<?php }
}


function generateStarIcons($number){
	//Full star
	$num = floor($number);
	$rest = $number-$num;
	echo '<i title="'.$number.'">';
	for($i=0; $i<$num; $i++){
		echo '<i class="fa fa-star yellow"></i>';
	}

	//Half star
	if($rest >= 0.5){
		echo '<i class="fa fa-star-half-empty yellow"></i>';
		$i++;
	}

	//Empty star
	for($j=$i; $j<5; $j++){
		echo '<i class="fa fa-star-o yellow"></i>';
	}
	echo '</i>';
}
?>

<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
