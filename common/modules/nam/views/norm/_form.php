<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\nam\models\complain\ComplainCategory;
use common\modules\nam\models\content\Section;
use common\modules\nam\models\event\Action;
use common\modules\nam\models\norm\Modality;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Norm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="norm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'complain_category')->textInput()->dropDownList(ArrayHelper::map(ComplainCategory::find()->all(),"id","description"))->label("Forbidden to upload content") ?>

    <?= $form->field($model, 'section')->textInput()->dropDownList(ArrayHelper::map(Section::find()->all(),"id","description"))->label("to section:") ?>

    <?= $form->field($model, 'closing')->widget(TimePicker::classname(),[
    'pluginOptions' => [
        'defaultTime' => '00:15',
        'showMeridian' => false
    ]
])->label("Time until this norm is closed:") ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('button', 'Create') : Yii::t('button', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
