<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<br>
<div class="jumbotron">



    <div class="col-xs-4 col-xs-offset-4 panel panel-default">
    <br>
				<div>
        			<img class="avatar img-circle img-thumbnail" src="images/guest.png" width="64" alt='User'>
				</div>
			
				<h4><?= Html::encode($model['username']);?></h4>
				<hr>
                <h5><?= Html::encode($model['email']);?></h5>            
    <br>
    </div>
    
</div>