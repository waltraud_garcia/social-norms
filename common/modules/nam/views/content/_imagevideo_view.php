<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\nam\models\content\Content;

/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

// $this->title = $model['title'];
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>


<ul class="list-unstyled video-list-thumbs">
	<li class="col-lg-3 col-sm-5 col-xs-4">		
		<h2><?= Html::a(Html::encode($model['title']), ['view', 'id' => $model['id']]) ?></h2>
		    <?php 
		    if($model['type']=='Video'){
		    	?>
		    	<img src=<?= Html::encode($model['url'])?> alt="" class="img-responsive" height="260px" />
		    	<span class="glyphicon glyphicon-play-circle"></span>
		    	<span class="duration"><?= Html::encode($model['message'])?></span>
		    <?php }else{ ?>
		    	<img src=<?= Html::encode($model['url'])?> alt=<?= Html::encode($model['message'])?> class="img-responsive" height="260px" />
		    <?php };?>
			<?php
        	$x = $model->getState();
        	switch($x){
        		case Content::STATE_NORMAL: 
        			if(!$model->isNorm()){
        				echo Html::a(Yii::t('button', 'Discuss norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'label label-primary']);
        			}else{
        				echo Html::a(Yii::t('button', 'Create norm'), ['norm/create', 'id' => $model['id'], 'category' => $model['category'], 'section' => $model['section']], ['class' => 'label label-primary']);
        			}
        			break;
        		case Content::STATE_COMPLAIN:
        			 echo $this->render('_footer_menu', ['model' => $model]);
        			 echo Html::a(Yii::t('button', 'Complain'), ['complain/new', 'content_id' => $model['id'], 'section' => $model['section']], ['class' => 'label label-danger']);
        			 break;
        		case Content::STATE_NORM:
        			echo $this->render('_footer_menu', ['model' => $model]);
					echo Html::a(Yii::t('button', 'View norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'label label-primary']);
        			break;
        		case Content::STATE_NORMAL_CONTENT:
        				break;
        	}
        	?>
	</li>
</ul>
   
    
