<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\nam\models\content\Content;
use common\models\User;



/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

// $this->title = $model['title'];
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
  <ul class="media-list forum">
    <!-- Forum Post -->
    <li class="media well">
      <div class="pull-left user-info">
        <img class="avatar img-circle img-thumbnail" src="images/guest.png" width="64" alt='User'>
          <?= Html::a(Yii::t('app', $model->users['username']), ['/nam/user/view']);?>        
      </div>
      <div class="media-body">
        <!-- Post Info Buttons -->
         <?= Html::a(Html::encode($model['title']), ['view', 'id' => $model['id']], ['class' => '']) ?>
        <div class="pull-right">
               <?= Html::encode($model['actualdate']) ?>
        </div>
        <br>
        <p><?= Html::encode($model['message']);?></p>
		<?php 
        	$x = $model->getState();
        	switch($x){
        		case Content::STATE_NORMAL:
        			?><div class="pull-right">
        			<?php 
        			if(!$model->isNorm()){
        				echo Html::a(Yii::t('button', 'Discuss norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);
        			}else{
        			   echo Html::a(Yii::t('button', 'Create norm'), ['norm/create', 'id' => $model['id'], 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);
        			}?>
        			</div>
        			<?php 
        			break;
        		case Content::STATE_COMPLAIN:
        			?>
        			<?= $this->render('_footer_menu', ['model' => $model]);?>
        			
        			<div class="pull-right">
        			<?php echo Html::a(Yii::t('button', 'Complain'), ['complain/new', 'content_id' => $model['id'], 'section' => $section], ['class' => 'btn btn-danger']);?>
        			</div>
        			<?php break;
        		case Content::STATE_NORM:
        			echo $this->render('_footer_menu', ['model' => $model]);
        			        			
        			?><div class="pull-right">
        			<?php echo Html::a(Yii::t('button', 'View norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);?>
        			</div>
        			<?php 
        			break;
				case Content::STATE_NORMAL_CONTENT :
        			break;
        	}
        ?>
      </div>
    </li>
  </ul>