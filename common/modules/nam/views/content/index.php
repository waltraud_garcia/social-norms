<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'Contents');
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <h1 class="white"><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

             //'id',
//             'category',
          //  'section',
            'owner',
            'title',
            // 'type',
            // 'url:url',
            // 'message:ntext',
            // 'num_of_views',
            // 'num_of_complaints',
            // 'violated_norm',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<!-- Show the action menu that the user can do-->
     <?= $this->render('_menu', ['model' => $dataProvider]) ?>    
</div>
