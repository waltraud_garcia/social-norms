<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'Contents');
// $this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <!-- Show the action menu that the user can do-->
     <?= $this->render('_menu', ['model' => $dataProvider, 'section'=>2]) ?>
</div>


<div class="container">
  <h1 class="page-header"><?=Yii::t('title', 'General topic')?></h1>
		<ul class="media-list forum">
			    <?= ListView::widget([
			        'dataProvider' => $dataProvider,
			        'itemOptions' => ['class' => 'item'],
			        'itemView' => function ($model, $key, $index, $widget) {
			        	return $this->render('_forum_view',['model' => $model, 'section' => 2]);
			        },
			        'emptyText' => '',
					'layout' => '{items}{pager}'])
			     ?>
		</ul>
	<h1 class="page-header"><?=Yii::t('title', 'Others topics')?></h1>
</div>
