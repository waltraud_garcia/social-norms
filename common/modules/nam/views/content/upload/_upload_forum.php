<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contents');
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">
<br><br>
    <?=ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) {
        	return $this->render('_upload_view',['model' => $model, 'section' => 'forum']);
                
        },         
        'layout' => '{items}{pager}',
        'emptyText' => 'All the predefined content sof this type of content for this specific section have uploaded!'
    ]); ?>
    
</div>
<br>