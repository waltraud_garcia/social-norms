<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use common\modules\nam\models\content\Content;
use common\modules\nam\models\content\ContentCategory;
use common\modules\nam\models\content\Section;


/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

// $this->title = $model['title'];
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="row reporter-row">
	<div class="col-xs-8 col-xs-offset-2 slide-row">
		<div class="slide-content">
        	<h4><?= Html::encode($model['title']) ?></h4>
            	<hr>
             	<p>
              		<?= Html::encode($model['message']) ?>
                </p> 
        </div>
        
        <div class="slide-footer">
        <?php 
			$user = \Yii::$app->user->identity->id;
			$content = Content::findOne($model['id']);
			
			/* Get the section and the content category name*/
			$sections = Section::findOne($model['section']);
			$category = ContentCategory::findOne($model['category']);
			
			if(!$content->isNorm()  && !$content->activeNorm()){?>
			    <!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target=<?="#myModal".$model['id']?> >
					<?=Yii::t('button', 'Upload')?>
				</button>

			<!-- Modal -->
			<div class="modal fade" id=<?="myModal".$model['id']?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			              <h4 class="modal-title" id="myModalLabel">Norm alert</h4>
			      </div>
			      
			      <div class="modal-body">
			      <?php 
			      $language = Yii::$app->request->cookies->getValue('language');
			      if($language == 'en'){
			      	echo Yii::t('norm','It is forbidden to upload ')." ".Html::encode($category['description'])." ".Yii::t('norm','content')." ".Yii::t('norm','at')." ".Html::encode($sections['description'])." ".Yii::t('norm','section');
			      }else{
			      	echo Yii::t('norm','It is forbidden to upload')." ".Yii::t('norm','content')." ".Html::encode($category['description'])." ".Yii::t('norm','at')." ".Yii::t('norm','section')." ".Html::encode($sections['description']);
			      }
			      ?>
					<br>
					<?=Yii::t('norm','Are you sure that you want to upload the content anyway?')?>
					
			      </div>
			      <div class="modal-footer">
			        <?= Html::a(Yii::t('button', 'Confirm'), ['upcontent', 'id' => $model['id'], 'section' => $section], ['class' => 'btn btn-primary']) ?>
			        <?= Html::a(Yii::t('button', 'Close'), ['noupcontent', 'id' => $model['id'], 'section' => $section], ['class' => 'btn btn-default']) ?>
			      </div>
			    </div>
			  </div>
			</div>
				
		<?php }else{ ?>
			<?= Html::a(Yii::t('button', 'Upload'), ['alegalupcontent', 'id' => $model['id'], 'section' => $section], ['class' => 'btn btn-primary']) ?>
		<?php }?>
        </div>
	</div>
</div>
</div>



