<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

// $this->title = $model['title'];
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<ul class="list-unstyled video-list-thumbs">
	<li class="col-lg-3 col-sm-4 col-xs-6">
		<h1><a href="#" title=<?= Html::encode($model['title']) ?>></h1>
		    <?php 
		    
		    if($model['type']=='Video'){
		    	?>
		    	<img src=<?= Html::encode($model['url'])?> alt="Barca" class="img-responsive" height="130px" />
		    	<h2><?= Html::encode($model['title']) ?></h2>
		    
		    	<span class="glyphicon glyphicon-play-circle"></span>
		    	<span class="duration"><?= Html::encode($model['message'])?></span>
		    <?php }else{ ?>
	   			<h2><?= Html::encode($model['title']) ?></h2>
		    	<img src=<?= Html::encode($model['url'])?> alt="Video" class="img-responsive" height="130px" />
		    
		    <?php };?>
       	<?= Html::a(Yii::t('button', 'Upload'), ['upcontent', 'id' => $model['id'], 'section' => $section], ['class' => 'label label-primary']) ?>
		  </a>
	</li>
</ul>