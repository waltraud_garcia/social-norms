<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\nam\models\content\Content;
use common\models\User;



/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

// $this->title = $model['title'];
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="row reporter-row">
	<div class="col-xs-8 col-xs-offset-2 slide-row">
		<div class="slide-content">	
			
				<h4><?= Html::a(Html::encode($model['title']), ['view', 'id' => $model['id']], ['class' => '']) ?></h4>
                <p>                
                	<?=Yii::t('other', 'Posted by')?>   <?= Html::a(Html::encode($model->users['username']), ['/nam/user/view']) ?> <?=Yii::t('other', 'on')?>  <?= Html::encode($model['actualdate']) ?>
                </p>
                <hr>
                <p>
                	<?= Html::encode($model['message']);?>
               		<!-- Message content -->
                </p> 
        </div>
			<?php 
        	$x = $model->getState();
        	switch($x){
        		case Content::STATE_NORMAL:
        			?><div class="slide-footer">
        			<?php 
        			if(!$model->isNorm()){
        				echo Html::a(Yii::t('button', 'Discuss norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);
        			}else{
        			   echo Html::a(Yii::t('button', 'Create norm'), ['norm/create', 'id' => $model['id'], 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);
        			}?>
        			</div>
        			<?php 
        			break;
        		case Content::STATE_COMPLAIN:
        			?>
        			<div class="slide-rating">
        			<?= $this->render('_footer_menu', ['model' => $model]);?>
        			</div>
        			<div class="slide-footer">
        			<?php echo Html::a(Yii::t('button', 'Complain'), ['complain/new', 'content_id' => $model['id'], 'section' => $section], ['class' => 'btn btn-danger']);?>
        			</div>
        			<?php break;
        		case Content::STATE_NORM:
        			?><div class="slide-rating">
        			<?php echo $this->render('_footer_menu', ['model' => $model]);?>
        			</div>        			
        			<div class="slide-footer">
        			<?php echo Html::a(Yii::t('button', 'View norm'), ['norm/detail', 'category' => $model['category'], 'section' => $model['section']], ['class' => 'btn btn-primary']);?>
        			</div>
        			<?php 
        			break;
        		case Content::STATE_NORMAL_CONTENT :
        			break;
        	}
        ?>
 	</div>
</div>	