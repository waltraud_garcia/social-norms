<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\nam\models\content\Content;

/* @var $this yii\web\View */
/* @var $model frontend\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="menu_content">
    <p>    
    	<?= Html::a(Yii::t('button', 'Post spam content', ['modelClass' => 'Content',]), ['upload', 'type' => Content::CONTENT_SPAM, 'section'=>$section], ['class' => 'btn btn-primary']) ?>
    	<?php //echo Html::a(Yii::t('button', 'Post erotic/pornographic content', ['modelClass' => 'Content',]), ['upload', 'type' => 2, 'section'=>$section], ['class' => 'btn btn-primary']) ?>
    	<?php //echo Html::a(Yii::t('button', 'Post violent content', ['modelClass' => 'Content',]), ['upload', 'type' => 3, 'section'=>$section], ['class' => 'btn btn-primary']) ?>
    	<?php //echo Html::a(Yii::t('button', 'I\'m angry, I want to insult', ['modelClass' => 'Content',]), ['upload', 'type' => 4, 'section'=>$section], ['class' => 'btn btn-primary']) ?>
    	<?= Html::a(Yii::t('button', 'Post normal content', ['modelClass' => 'Content',]), ['upload', 'type' => Content::CONTENT_NORMAL, 'section'=>$section], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
<br>
