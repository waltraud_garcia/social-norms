<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'Image & Video');
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <!-- Show the action menu that the user can do-->
     <?= $this->render('_menu', ['model' => $dataProvider, 'section'=>3]) ?>
</div>

<div class="content-index">
    <?=ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) {
        	return $this->render('_imagevideo_view',['model' => $model]);
        },
        'emptyText' => '',
		'layout' => '{items}',
    ]); ?>
</div>
<br>

