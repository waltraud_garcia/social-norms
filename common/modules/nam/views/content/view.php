<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Content */

$this->title = $model->title;

// 	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['reporter']];
// 	$this->params['breadcrumbs'][] = $this->title;	
?>

<br><br>
<div class="row reporter-row">
	<div class="col-xs-10 col-xs-offset-1 slide-row2">
		<div class="slide-content3">		
			<h4><?= Html::encode($model['title'])?></h4>            
                <p>                
                	<?=Yii::t('other', 'Posted by')?> <?= Html::a(Yii::t('app', $model->users['username']), ['/nam/user/view']);?> <?=Yii::t('other', 'on')?>  <?= Html::encode($model['actualdate']) ?>
                </p>
     
                <hr>
                <p>
                	<?= Html::encode($model['message']);?>
                </p> 
                
                <br>
                <p>	                
	               <?php 
						if($model->alertView()){
						?>
						<span class="label label-default">Views <?= Html::encode($model['num_of_views']);?></span>  
						<?php 
						}else{
						?>
						<span class="label label-success">Views <?= Html::encode($model['num_of_views']);?></span>  
						
						<?php
						}
						if($model->alertComplain()){ 

					?>
						<span class="label label-default">Complaints <?= Html::encode($model['num_of_complaints']);?></span>
						<span class="label label-danger"> <?= "Conflict detected!" ?></span>
											<?php 
						}else{ ?>
							<span class="label label-default">Complaints <?= Html::encode($model['num_of_complaints']);?></span>
					<?php } ?>
					
					
					<?php 
						if(!$model->isNorm($model['owner']) && !$model->activeNorm()){ 
					?>
						<span class="label label-danger"><?= "Forbidden by norm" ?></span>
										<?php 
						}
					?>
			</p> 
        </div>
        
       
	</div>
</div>	