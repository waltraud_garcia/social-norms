<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'Reporter');
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <!-- Show the action menu that the user can do-->
     <?= $this->render('_menu', ['model' => $dataProvider, 'section'=>1]) ?>
</div>

<div class="content-index">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
        	return $this->render('_reporter_view',['model' => $model, 'section' => 1]);
        },
        'emptyText' => '',
        'layout' => '{items}{pager}',
    ]) ?>
</div>