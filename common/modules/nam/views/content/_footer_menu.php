<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
if($model->alertView()){
?>
<span class="label label-default"><?=Yii::t('button', 'Views')?> <?= Html::encode($model['num_of_views']);?></span>  
<?php 
}else{
?>
<span class="label label-success"><?=Yii::t('button', 'Views')?> <?= Html::encode($model['num_of_views']);?></span>  

<?php
}
if($model->alertComplain()){ 
?>
	<span class="label label-default"><?=Yii::t('button', 'Complaints')?> <?= Html::encode($model['num_of_complaints']);?></span>
	<span class="label label-danger"> <?= "Conflict detected!" ?></span>
<?php 
}else{ ?>
	<span class="label label-default"><?=Yii::t('button', 'Complaints')?> <?= Html::encode($model['num_of_complaints']);?></span>
<?php } ?>


<?php 
if(!$model->isNorm($model['owner'])){ 
?>
	<span class="label label-danger"><?= "Forbidden by norm" ?></span>
<?php 
}
?>
