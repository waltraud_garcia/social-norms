<?php

namespace common\modules\nam;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\nam\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
