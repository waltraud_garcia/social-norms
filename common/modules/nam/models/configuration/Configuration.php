<?php

namespace common\modules\nam\models\configuration;

use Yii;

/**
 * This is the model class for table "configuration".
 *
 * @property integer $id_experiment
 * @property string $description
 * @property integer $active
 *
 * @property Log[] $logs
 */
class Configuration extends \yii\db\ActiveRecord
{
	/*Content variables*/
	const UNIQUECOMPLAINTS = true;
	const UNIQUEVIEWS = true;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configuration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['active'], 'integer'],
            [['description'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_experiment' => Yii::t('configuration', 'Id Experiment'),
            'description' => Yii::t('configuration', 'Description'),
            'active' => Yii::t('configuration', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['id_experiment' => 'id_experiment']);
    }
}
