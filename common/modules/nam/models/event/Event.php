<?php

namespace common\modules\nam\models\event;

use Yii;
use common\models\User;
use common\modules\nam\models\content\Content;
use common\modules\nam\models\event\Action;
use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\event\Log;
use common\modules\nam\models\complain\ComplainCategory;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $user
 * @property integer $content
 * @property integer $action
 * @property integer $complain_category
 * @property integer $infringedNorm
 * @property integer $fulfilledNorm
 * @property integer $checked
 *
 * @property User $user0
 * @property Content $content0
 * @property Action $action0
 * @property ComplainCategory $complainCategory
 * @property Norm $infringedNorm0
 * @property Norm $fulfilledNorm0
 * @property Log[] $logs
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'action'], 'required'],
            [['user', 'content', 'action', 'complain_category', 'infringedNorm', 'fulfilledNorm', 'checked'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('event', 'ID'),
            'user' => Yii::t('event', 'User'),
            'content' => Yii::t('event', 'Content'),
            'action' => Yii::t('event', 'Action'),
            'complain_category' => Yii::t('event', 'Complain Category'),
            'infringedNorm' => Yii::t('event', 'Infringed Norm'),
            'fulfilledNorm' => Yii::t('event', 'Fulfilled Norm'),
            'checked' => Yii::t('event', 'Checked'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent0()
    {
        return $this->hasOne(Content::className(), ['id' => 'content']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction0()
    {
        return $this->hasOne(Action::className(), ['id' => 'action']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplainCategory()
    {
        return $this->hasOne(ComplainCategory::className(), ['id' => 'complain_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfringedNorm0()
    {
        return $this->hasOne(Norm::className(), ['id' => 'infringedNorm']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFulfilledNorm0()
    {
        return $this->hasOne(Norm::className(), ['id' => 'fulfilledNorm']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['event' => 'id']);
    }
    
    /**
     * Method to save attributes into event object
     * @param unknown $user User
     * @param unknown $content Content
     * @param unknown $action Type of action
     * @param string $complaint_category Type of complain
     */
    public function saveAttributes($user, $content, $action, $infringedNorm=null, $fulfilledNorm=null, $complain_category = null){
    	$this->user = $user;
    	$this->content = $content;
    	$this->action = $action;
    	$this->infringedNorm = $infringedNorm;
    	$this->fulfilledNorm = $fulfilledNorm;
    	$this->complain_category = $complain_category;
    }
}
