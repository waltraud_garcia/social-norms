<?php

namespace common\modules\nam\models\event;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\nam\models\event\Event;

/**
 * EventSearch represents the model behind the search form about `common\modules\nam\models\event\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user', 'content', 'action', 'infringedNorm', 'fulfilledNorm', 'complain_category', 'checked'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user' => $this->user,
            'content' => $this->content,
            'action' => $this->action,
            'infringedNorm' => $this->infringedNorm,
            'fulfilledNorm' => $this->fulfilledNorm,
            'complain_category' => $this->complain_category,
            'checked' => $this->checked,
        ]);

        return $dataProvider;
    }
}
