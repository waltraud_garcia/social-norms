<?php

namespace common\modules\nam\models\event;

use Yii;

/**
 * This is the model class for table "action".
 *
 * @property integer $id
 * @property string $description
 */
class Action extends \yii\db\ActiveRecord
{
	const UPLOAD = 1;
	const VIEW = 2;
	const COMPLAIN = 3;
	const NOTUPLOAD = 4;
	const VIEWSECTION = 5;
	const CREATENORM = 6;
	const DISCUSSNORM = 7;
	const ADDARGUMENT = 8;
	const RATEARGUMENT = 9;
	const ACTIVENORM = 10;
	
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'description'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
