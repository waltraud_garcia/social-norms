<?php

namespace common\modules\nam\models\event;

use Yii;
use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\norm\Argument;
use common\modules\nam\models\event\Event;
use common\modules\nam\models\content\Section;
use common\modules\nam\models\configuration\Configuration;


/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $id_experiment
 * @property integer $event
 * @property integer $argument
 * @property integer $argument_rate
 * @property integer $norm
 * @property integer $section
 * @property string $date
 *
 * @property Event $event0
 * @property Section $section0
 * @property Argument $argument0
 * @property Norm $norm0
 * @property Configuration $idExperiment
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_experiment', 'event', 'argument', 'argument_rate', 'norm', 'section'], 'integer'],
            [['date'], 'required'],
            [['date'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('log', 'ID'),
            'id_experiment' => Yii::t('log', 'Id Experiment'),
            'event' => Yii::t('log', 'Event'),
            'argument' => Yii::t('log', 'Argument'),
            'argument_rate' => Yii::t('log', 'Argument Rate'),
            'norm' => Yii::t('log', 'Norm'),
            'section' => Yii::t('log', 'Section'),
            'date' => Yii::t('log', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent0()
    {
        return $this->hasOne(Event::className(), ['id' => 'event']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection0()
    {
        return $this->hasOne(Section::className(), ['id' => 'section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArgument0()
    {
        return $this->hasOne(Argument::className(), ['id' => 'argument']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNorm0()
    {
        return $this->hasOne(Norm::className(), ['id' => 'norm']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdExperiment()
    {
        return $this->hasOne(Configuration::className(), ['id_experiment' => 'id_experiment']);
    }
    
    /**
     * Method to save attributes into event object
     * @param unknown $user User
     * @param unknown $content Content
     * @param unknown $action Type of action
     * @param string $complaint_category Type of complain
     */
    public function saveAttributes($event, $section, $date,$argument,$argument_rate,$norm){
    	$configuration = Configuration::find()->where(['active' => true])->one();
    	if(is_null($configuration['id_experiment'])){
    		$this->id_experiment = 1;
    	}else{
    		$this->id_experiment = $configuration['id_experiment'];
    	}
    	$this->argument = $argument;
    	$this->argument_rate = $argument_rate;
    	$this->norm = $norm;
    	$this->event = $event;
    	$this->section = $section;
    	$this->date = $date;
    }
}
