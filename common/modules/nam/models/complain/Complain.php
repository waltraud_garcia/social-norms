<?php

namespace common\modules\nam\models\complain;

use Yii;

/**
 * This is the model class for table "complaint".
 *
 * @property integer $id
 * @property integer $user
 * @property integer $content
 * @property integer $complain_category
 * @property string $reply
 */
class Complain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'content', 'complain_category'], 'required'],
            [['user', 'content', 'complain_category'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'content' => Yii::t('app', 'Content'),
            'complain_category' => Yii::t('app', 'Complain Category'),
        ];
    }
    
    public function categories(){
    	return $this->hasMany(User::className(), ['id' => 'complain_category' ]);
    }
}
