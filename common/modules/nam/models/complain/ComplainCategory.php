<?php

namespace common\modules\nam\models\complain;

use Yii;

/**
 * This is the model class for table "complain_category".
 *
 * @property integer $id
 * @property string $description
 */
class ComplainCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complain_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'description'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
    
    public function complain(){
    	return $this->hasMany(User::className(), ['complain_category' => 'id' ]);
    }
}
