<?php

namespace common\modules\nam\models\complain;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\nam\models\complaint\Complaint;

/**
 * ComplainSearch represents the model behind the search form about `common\modules\nam\models\complaint\Complaint`.
 */
class ComplainSearch extends Complaint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user', 'content', 'complain_category'], 'integer'],
            [['reply'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Complaint::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user' => $this->user,
            'content' => $this->content,
            'complain_category' => $this->complain_category,
        ]);

        $query->andFilterWhere(['like', 'reply', $this->reply]);

        return $dataProvider;
    }
}
