<?php

namespace common\modules\nam\models\norm;

use Yii;

/**
 * This is the model class for table "modality".
 *
 * @property string $modality
 */
class Modality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['modality'], 'required'],
            [['modality'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'modality' => Yii::t('app', 'Modality'),
        ];
    }
}
