<?php

namespace common\modules\nam\models\norm;

use Yii;

/**
 * This is the model class for table "argument".
 *
 * @property integer $id
 * @property integer $norm
 * @property integer $user
 * @property integer $type
 * @property float $rate
 * @property interger $numrate
 * @property double $weight
 * @property string $description
 *
 * @property Norm $norm0
 * @property User $user0
 * @property ArgumentType $type0
 * @property ArgumentRate[] $argumentRates
 * @property User[] $idUsers
 * @property Log[] $logs
 */
class Argument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'argument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['norm', 'description'], 'required'],
            [['norm', 'user', 'type','numrate'], 'integer'],
            [['description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('argument', 'ID'),
            'norm' => Yii::t('argument', 'Norm'),
            'user' => Yii::t('argument', 'User'),
            'type' => Yii::t('argument', 'Type'),
            'rate' => Yii::t('argument', 'Rate'),
            'numrate' => Yii::t('argument', 'NumRate'),
            'description' => Yii::t('argument', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNorm0()
    {
        return $this->hasOne(Norm::className(), ['id' => 'norm']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(ArgumentType::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArgumentRates()
    {
        return $this->hasMany(ArgumentRate::className(), ['id_argument' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'id_user'])->viaTable('argument_rate', ['id_argument' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['argument' => 'id']);
    }
}
