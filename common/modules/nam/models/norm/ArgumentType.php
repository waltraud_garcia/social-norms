<?php

namespace common\modules\nam\models\norm;

use Yii;

/**
 * This is the model class for table "argument_type".
 *
 * @property integer $id
 * @property string $description
 *
 * @property Argument[] $arguments
 */
class ArgumentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'argument_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('argument', 'ID'),
            'description' => Yii::t('argument', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArguments()
    {
        return $this->hasMany(Argument::className(), ['type' => 'id']);
    }
}
