<?php

namespace common\modules\nam\models\norm;

use Yii;

/**
 * This is the model class for table "argument_rate".
 *
 * @property integer $id_argument
 * @property integer $id_user
 * @property integer $rate
 *
 * @property Argument $idArgument
 * @property User $idUser
 */
class ArgumentRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'argument_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_argument', 'id_user'], 'required'],
            [['id_argument', 'id_user', 'rate'], 'integer']
        ];
    }

    public static function isRated($id){
    	$user = \Yii::$app->user->identity->id;
    	$query = ArgumentRate::find()->where(['id_user' => $user, 'id_argument' => $id])->one();
    	return is_null($query);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_argument' => Yii::t('argument', 'Id Argument'),
            'id_user' => Yii::t('argument', 'Id User'),
            'rate' => Yii::t('argument', 'Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArgument()
    {
        return $this->hasOne(Argument::className(), ['id' => 'id_argument']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
