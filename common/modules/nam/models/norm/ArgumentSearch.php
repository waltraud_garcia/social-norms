<?php

namespace common\modules\nam\models\norm;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\nam\models\norm\Argument;

/**
 * ArgumentSearch represents the model behind the search form about `common\modules\nam\models\norm\Argument`.
 */
class ArgumentSearch extends Argument
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'norm', 'user', 'type', 'rating'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Argument::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'norm' => $this->norm,
            'user' => $this->user,
            'type' => $this->type,
            'rating' => $this->rating,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
