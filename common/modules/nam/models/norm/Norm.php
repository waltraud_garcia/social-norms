<?php

namespace common\modules\nam\models\norm;

use Yii;
use common\modules\nam\models\complain\ComplainCategory;
use common\modules\nam\models\norm\Modality;
use common\modules\nam\models\event\Action;
use common\modules\nam\models\content\Section;
use common\modules\nam\models\event\Log;
use common\modules\nam\models\event\Event;
use common\modules\nam\models\norm\Argument;

/**
 * This is the model class for table "norm".
 *
 * @property integer $id
 * @property integer $user
 * @property integer $section
 * @property integer $complain_category
 * @property integer $modality
 * @property integer $action
 * @property float $rating
 * @property floatçfloat $ratingpos
 * @property float $ratingneg
 * @property string $name
 * @property integer $active
 * @property string $closing

 * @property Argument[] $arguments
 * @property Event[] $events
 * @property Log[] $logs
 * @property ComplainCategory $complainCategory
 * @property Section $section0
 * @property Modality $modality0
 * @property Action $action0
 */
class Norm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'norm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'section', 'complain_category', 'modality', 'action', 'active'], 'integer'],
            [['action', 'name'], 'required'],
            [['name'], 'string', 'max' => 20],
            ['closing','filter','filter' => function ($value){
                sscanf($value, '%d:%d',$h,$m);
                if($m===0 || !is_null($m)){
                  return time() + $h * 3600 + $m * 60;
                }else{
                  return $value;
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('norm', 'ID'),
            'user' => Yii::t('norm', 'User'),
            'section' => Yii::t('norm', 'Section'),
            'complain_category' => Yii::t('norm', 'Complain Category'),
            'modality' => Yii::t('norm', 'Modality'),
            'action' => Yii::t('norm', 'Action'),
            'rating' => Yii::t('norm', 'Rating'),
            'ratingpos' => Yii::t('norm', 'RatingPos'),
            'ratingneg' => Yii::t('norm', 'RatingNeg'),
            'name' => Yii::t('norm', 'Name'),
            'closing' => Yii::t('norm', 'Closing'),
            'active' => Yii::t('norm', 'Active'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArguments()
    {
        return $this->hasMany(Argument::className(), ['norm' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['fulfilledNorm' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['norm' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplainCategory()
    {
        return $this->hasOne(ComplainCategory::className(), ['id' => 'complain_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection0()
    {
        return $this->hasOne(Section::className(), ['id' => 'section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModality0()
    {
        return $this->hasOne(Modality::className(), ['id' => 'modality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction0()
    {
        return $this->hasOne(Action::className(), ['id' => 'action']);
    }
}
