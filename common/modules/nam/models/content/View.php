<?php

namespace common\modules\nam\models\content;

use Yii;

/**
 * This is the model class for table "view".
 *
 * @property integer $user
 * @property integer $content
 */
class View extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'content'], 'required'],
            [['user', 'content'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user' => Yii::t('app', 'User'),
            'content' => Yii::t('app', 'Content'),
        ];
    }
}
