<?php

namespace common\modules\nam\models\content;

use Yii;
use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\content\View;
use common\models\User;
use frontend\widgets\Alert;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $category
 * @property integer $section
 * @property integer $owner
 * @property string $title
 * @property string $type
 * @property string $url
 * @property string $message
 * @property integer $num_of_views
 * @property integer $num_of_complaints
 * @property integer $violated_norm
 * @property integer $actualDate
 */
class Content extends \yii\db\ActiveRecord{
	
	const COMPLAINT_ALERT = 1;
	const VIEW_ALERT = 10;
	
	//States
	const STATE_NORMAL = 0;
	const STATE_COMPLAIN = 1;
	const STATE_NORM = 2;
	const STATE_NORMAL_CONTENT = 3;
	
	const CONTENT_SPAM = 1;
	const CONTENT_NORMAL = 2;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'section', 'title', 'type'], 'required'],
            [['category', 'section', 'owner', 'num_of_views', 'num_of_complaints', 'violated_norm'], 'integer'],
            [['message'], 'string'],
            [['title'], 'string', 'max' => 512],
            [['type','actualdate'], 'string', 'max' => 512],
            [['url'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'section' => Yii::t('app', 'Section'),
            'owner' => Yii::t('app', 'Owner'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'url' => Yii::t('app', 'Url'),
            'message' => Yii::t('app', 'Message'),
            'num_of_views' => Yii::t('app', 'Num Of Views'),
            'num_of_complaints' => Yii::t('app', 'Num Of Complaints'),
            'violated_norm' => Yii::t('app', 'Violated Norm'),
        	'actualdate' => Yii::t('app', 'Violated Norm'),
        ];
    }
	
    
    public function getUsers(){
    	return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	
	public function incrementViews(){
		$this->num_of_views++;
	}
	
	public function incrementComplaints(){
		$this->num_of_complaints++;
	}
	/**
	 * Method that check if necessary to show the complaint alert
	 * @return boolean
	 */
	public function alertComplain(){
// 		if($this->num_of_views == 0) return false;
// 		$factor = (float)$this->num_of_complaints/(float)$this->num_of_views;
// 		if(Content::COMPLAINT_ALERT <= $factor) return true;
		return (Content::COMPLAINT_ALERT == $this->num_of_complaints);
// 		return false;
	}
	
	/**
	 * Method that check if necessary to show the complaint alert
	 * @return boolean
	 */
	public function alertView(){
		if(Content::VIEW_ALERT >= $this->num_of_views) return true;
		return false;
	}
	
	/**
	 * Check if exist a norm for this type of content
	 * @param unknown $user
	 * @return boolean
	 */
	public function isNorm(){
		$query = Norm::find()->where(['complain_category' => $this->category, 'section' => [$this->section,Section::ALL]])->one();
		return is_null($query);
	}
	
	public function activeNorm(){
		$query = Norm::find()->where(['complain_category' => $this->category, 'section' => $this->section, 'section' => [$this->section,Section::ALL], 'active' => 1])->one();
		return is_null($query);
	}
	
	public function getNorm(){
		$query = Norm::find()->where(['complain_category' => $this->category, 'section' => $this->section])->one();
		return $query['id'];
	}
	
	/**
	 * Get the state of the content
	 * @return number Get the type of the state
	 */
	public function getState(){
		$query = Norm::find()->where(['complain_category' => $this->category, 'section' => [$this->section,Section::ALL]])->one();
		
		if(is_null($query)){
			if($this->category == CONTENT::CONTENT_NORMAL){
				return Content::STATE_NORMAL_CONTENT;
			}else{
				if($this->alertComplain()){
					return Content::STATE_NORMAL;
				}else{
					return Content::STATE_COMPLAIN;
				}
			}
		}else{
			//Check if the norm is closed or openned
			if($query->active){
				return Content::STATE_NORM;
			}else{
				return Content::STATE_NORMAL;
			}
		}
	}
	
	public function getSections(){
		return $this->hasOne(Section::className(), ['id' => 'section']);
	}
	
    public function getCategories(){
    	return $this->hasOne(ContentCategory::className(), ['id' => 'content_category']);
    }
}
