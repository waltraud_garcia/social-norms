<?php

namespace common\modules\nam\models\content;

use Yii;
use common\modules\nam\models\norm\Norm;

/**
 * This is the model class for table "content_category".
 *
 * @property integer $id
 * @property string $description
 */
class ContentCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
    
    public function getNorm(){
    	return $this->hasMany(Norm::className(), ['id' => 'user' ]);
    }
    
    public function getContent(){
    	return $this->hasMany(Content::className(), ['id' => 'content' ]);
    }
}
