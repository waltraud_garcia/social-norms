<?php

namespace common\modules\nam\models\content;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $description
 */
class Section extends \yii\db\ActiveRecord
{
	
	const REPORTER = 1;
	const FORUM = 2;
	const IMAGEVIDEO = 3;
	const ALL = 4;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
