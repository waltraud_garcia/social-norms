<?php

namespace common\modules\nam\controllers;

use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\norm\Argument;
use common\modules\nam\models\norm\ArgumentRate;
use yii\data\ActiveDataProvider;

class OWAController{

	const MODE = 0;
	const a = 0.4;
	const b = 1;


	public static function getArgumentRates($id){
		$array = array();
		$sql = 'select * from argument_rate where id_argument='.$id;
		$query = ArgumentRate::findBySql($sql);
		$dataProvider = new ActiveDataProvider(['query' => $query]);
		$modelsArguments = $dataProvider->models;
		foreach($modelsArguments as $argument){
			array_push($array,$argument['rate']);
		}
		return $array;
	}

	public static function updateRatingArgument($id){
		$array = OWAController::getArgumentRates($id);
		exec('java -jar ../Wrapper.jar -ra ' . implode(',',$array), $output);
		$arResults = explode(',',$output[0]);
		$argument = Argument::findOne($id);
		$argument->rate=$arResults[0];
		$argument->numrate=sizeof($array);
		$argument->weight=$arResults[2];
		$argument->save();
		OWAController::updateRatingNorm($argument->norm);
	}

	public static function updateRatingNorm($id){
		exec('java -jar ../Wrapper.jar -n ' .
			implode(':',OWAController::getRatingsArgumentSet($id,1)) . '-' .
			implode(':',OWAController::getRatingsArgumentSet($id,2)), $output);
		$nResults = explode(',',$output[0]);
		$norm = Norm::findOne($id);
		$norm->ratingpos = $nResults[0];
		$norm->ratingneg = $nResults[1];
		$norm->rating = $nResults[2];
		$norm->save();
	}

	public static function getRatingsArgumentSet($id,$type)
	{
		$result = 0;
		$array = array();
		$sql = 'select * from argument where type = '.$type.' && norm = '.$id;
		$query = Argument::findBySql($sql);
		$dataProvider = new ActiveDataProvider(['query' => $query]);
		$models = $dataProvider->models;

		foreach($models as $model) array_push($array,$model['rate'] . ',' . $model['numrate'] . ',' . $model['weight']);
		return $array;
	}
}
