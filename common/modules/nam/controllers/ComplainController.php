<?php

namespace common\modules\nam\controllers;

use Yii;
use common\modules\nam\controllers\LanguageController;
use common\modules\nam\models\content\Content;
use common\modules\nam\models\complain\Complain;
use common\modules\nam\models\event\Action;
use common\modules\nam\models\event\Event;
use common\modules\nam\models\complain\ComplainSearch;
use common\modules\nam\models\configuration\Configuration;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\nam\controllers\EventController;

/**
 * ComplainController implements the CRUD actions for Complain model.
 */
class ComplainController extends LanguageController
{
	
	private $content;
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Complaint models.
     * @return mixed
     */
    public function actionIndex($id){
    	//Increment the complain values
    	$model = Content::findOne($id);
    	$model->num_of_complaints++;
    	$model->save();
    	
    	$user = \Yii::$app->user->identity->id;
    	$sql = 'select * from complaint where user = '.$user.' && content ='.$id;
    	$query = Complain::findBySql($sql);
    	
    	//You can make a complain
    	if(is_null($query)){
    		$searchModel = new ComplainSearch();
    		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    		
    		return $this->render('index', [
    				'searchModel' => $searchModel,
    				'dataProvider' => $dataProvider,
    		]);
    	//You already do the complain
    	}else{
    		return $this->render('norm/index');
    	}
    }

    /**
     * Displays a single Complaint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Complaint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Complaint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Complaint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Complaint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Complain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNew($content_id, $section){
    	$model = Content::findOne($content_id);
    	
    	//Create and fill the fields of the new complaint
    	$complaint = new Complain();
    	$complaint->content = $content_id ; //Give the category form the content
    	$user = $this->getActualUser();
    	$complaint->user = $user;
    	$complaint->complain_category = $model->category;
    	
    	if(Configuration::UNIQUECOMPLAINTS){
    		$query = Complain::find()->where(['user' => $user, 'content' => $content_id])->one();
    		
    		if(is_null($query)){
    			$complaint->save();
    			//Increment the number of complaint of the content
    			$model->num_of_complaints++;
    			$model->save();
    			//Create the complaint event
    			EventController::createEvent($section,$user,$content_id,$action=ACTION::COMPLAIN); //Noupload action
    		}else{
    			Yii::$app->session->setFlash('error', Yii::t('message','You already complain this content!'));
    		}
    	}else{
    		$complaint->save();
    		//Increment the number of complaint of the content
    		$model->num_of_complaints++;
    		$model->save();
    		//Create the complaint event
    		EventController::createEvent($section,$user,$content_id,$action=ACTION::COMPLAIN); //Noupload action
    	}
    	
    	switch ($section){
    		case 1:
    			$sectionText = 'reporter';
    			break;
    		case 2:
    			$sectionText = 'forum';
    			break;
    		case 3:
    			$sectionText = 'imagevideo';
    			break;
    	}
    	
    	$this->redirect(["content/".$sectionText]);
    }
    
    private function getActualUser(){
    	return \Yii::$app->user->identity->id;
    }
}
