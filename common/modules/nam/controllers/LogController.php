<?php

namespace common\modules\nam\controllers;

use common\modules\nam\models\event\Log;

class LogController extends LanguageController{
    
	public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Method to save into event table the actions
     * @param object $user User
     * @param object $content Content
     * @param object $action Action
     * @param string $complaint_category Complaint category
     */
    public static function createLog($event,$section,$argument,$argument_rate,$norm){
    	$log = new Log();
    	$date = date('Y-m-d H:i:s');
    	$log->saveAttributes($event,$section,$date,$argument,$argument_rate,$norm);
    	$log->save();
    }
}
