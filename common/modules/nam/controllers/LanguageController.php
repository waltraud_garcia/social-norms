<?php
namespace common\modules\nam\controllers;

use Yii;
use yii\web\Controller;

/**
 * Language controller to manage the translations of text within in the web
*  @author David Sanchez Pinsach
*  @version 1.0
 */
class LanguageController extends Controller
{
	
	/**
	 * Init function to load the cookie property
	 * @see \yii\base\Object::init()
	 */
	public function init(){
		$app = Yii::$app;
		$cookie = \Yii::$app->request->cookies->getValue('language');
		$app->language = $cookie;
	}
	
	/**
	 * Action method to change the language of the app
	 * @param unknown $language
	 * @return \yii\web\Response
	 */
	public function actionLanguage($language){
		if (is_null(\Yii::$app->request->cookies->getValue('language'))){
			Yii::$app->session->setFlash('warning', 'This web uses cookies to give you a better user experience');
		}
		
		$app = Yii::$app;
		$options['name'] = 'language';
		$options['value'] = $language;
		$options['expire'] = time()+86400*365;
		$cookie = new \yii\web\Cookie($options);
		\Yii::$app->response->cookies->add($cookie);
		return $this->goHome();
	}
}
