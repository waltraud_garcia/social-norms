<?php

namespace common\modules\nam\controllers;

use Yii;
use yii\web\Controller;
use common\modules\nam\controllers\LanguageController;
use common\modules\nam\models\content\Content;
use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\content\UserContent;
use common\models\User;
use common\modules\nam\models\event\Event;
use common\modules\nam\controllers\EventController;
use common\modules\nam\models\event\Action;
use common\modules\nam\models\complaint\Complaint;
use common\modules\nam\models\complaint\ComplaintCategory;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Console;
use common\modules\nam\models\configuration\Configuration;
use common\modules\nam\models\content\View;

class ContentController extends LanguageController
{
	const SECTION_REPORTER = 1;
	const SECTION_FORUM = 2;
	const SECTION_IMAGEVIDEO = 3;
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Content::find(),
        		
        ]);

        return $this->render('/content/index', [
            'dataProvider' => $dataProvider,
        		
        ]);
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    
    private function generateDataProviderSection($section){
    	$sql = 'select * from content where section = '.$section.' && owner is not null';
    	$query = Content::findBySql($sql);
    
    	$dataProvider = new ActiveDataProvider(['query' => $query->orderBy('id ASC'),]);
    	return $dataProvider;
    }
    
    public function actionReporter(){
    	EventController::createEvent(1,$this->getActualUser(),null,$action=ACTION::VIEWSECTION);
    	$dataProvider = $this->generateDataProviderSection(1);    	 
    	return $this->render('/content/reporter', ['dataProvider' => $dataProvider, 'section'=> 1]);
    }
    
    
    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionForum(){
    	EventController::createEvent(2,$this->getActualUser(),null,$action=ACTION::VIEWSECTION);
   		$dataProvider = $this->generateDataProviderSection(2);
    	return $this->render('/content/forum', ['dataProvider' => $dataProvider, 'section'=> 2]);
    }
    
    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionImagevideo(){
    	EventController::createEvent(3,$this->getActualUser(),null,$action=ACTION::VIEWSECTION);
    	$dataProvider = $this->generateDataProviderSection(3);
    	return $this->render('/content/imagevideo', ['dataProvider' => $dataProvider, 'section'=> 3]);
    }
    
    
    /**
     * Displays a single Content model.
     * @param in
     * teger $id
     * @return mixed
     */
    public function actionView($id){
    	$model = $this->findModel($id);
    	$user = $model['owner'];
    	$section = $model['section'];
    	$result = false;
    	if(Configuration::UNIQUEVIEWS){
    		$query = View::find()->where(['user' => $user, 'content' => $id])->one();
    		if(is_null($query)){
    			$view = new View();
	    		$view->content = $id;
	    		$view->user = $user;
	    		$view->save();
	    		
	    		$model->incrementViews();
	    		$model->save();
	    		EventController::createEvent($section,$user,$id,$action=ACTION::VIEW); 
    		}
    	}else{
    		$model->incrementViews();
    		$model->save();
    		EventController::createEvent($section,$user,$id,$action=ACTION::VIEW);
    	}
    	
        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Content();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * Updates an existing Content model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [ 'model' => $model]);
        }
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

   /**
    * Action to see the upload list files depending on the section
    * @param unknown $type Category of the content
    * @param unknown $section Section of the content
    * @return string
    */ 
   public function actionUpload($type, $section){
		$query = (new \yii\db\Query());
   		if($type!='*'){
   			$query->select('*')->from('content')->where('section='.$section.' and category='.$type.' and owner is NULL');
   		}else{
   			$query->select('*')->from('content')->where('section='.$section.' and owner is NULL');
   		};
   		$dataProvider = new ActiveDataProvider(['query' => $query]);
   		$dataProvider->setSort(['attributes' => ['desc' => 'actualdate']]);

   		switch ($section){
   			case 1:
   				$sectionText = 'reporter';
   				break;
   			case 2:
   				$sectionText = 'forum';
   				break;
   			case 3:
   				$sectionText = 'imagevideo';
   				break;
   		}
   		$page = '/content/upload/_upload_'.$sectionText;
   		return $this->render($page, ['dataProvider' => $dataProvider]);	 
   }
   
   /**
    * Method to upload the predefined content
    * @param String $section
    */
   public function actionUpcontent($id, $section){
   		$this->uploadContent($id, ACTION::UPLOAD, $section);
   }
   
   /**
    * Method that save the event of not upload
    * @param unknown $id
    * @param unknown $section
    */
   public function actionNoupcontent($id, $section){
   		$user = $this->getActualUser();
   		$model = $this->findModel($id);
   		$query = Norm::find()->where(['complain_category' => $model->category, 'section' => $model->section])->one(); 
   		
   		//Create an event
   		EventController::createEvent($model->section,$user,$id,ACTION::NOTUPLOAD,null,null,null,null,$query['id']); //Noupload action
   		$this->redirect([$section]);
   		Yii::$app->session->setFlash('success',Yii::t('message','Great! You have respected the norm!'));
   }
   
   /**
    * Method that save the event of alegal upload (Upload in which the content does not have the norm)
    * @param unknown $id
    * @param unknown $section
    */
   public function actionAlegalupcontent($id, $section){
   		$this->uploadContent($id, ACTION::UPLOAD, $section);
   }
   
   private function uploadContent($id, $action, $section, $infringedNorm=null, $fulfilledNorm=null){
   		//$user = \Yii::$app->user->identity->id;
   		$user = $this->getActualUser();
   		$model = $this->findModel($id);
   		$query = Norm::find()->where(['complain_category' => $model->category, 'section' => $model->section])->one();
	   	$model->owner =  $user;
 	   	$model->actualdate = date('Y-m-d H:i:s');
 	   	
	   	if($model->save()){
	   		//Create an event
	   		if(!is_null($query)){
	   			EventController::createEvent($model->section,$user,$id,$action,null,null,null,$query['id']); //Upload action
	   		}else{
	   			EventController::createEvent($model->section,$user,$id,$action); //Upload action
	   		}
	   		Yii::$app->session->setFlash('success', Yii::t('message','Your content was uploaded!'));
	   	}else{
	   		Yii::$app->session->setFlash('error', 'This content is already uploaded!');
	   	}
	   	$this->redirect([$section]);
   }
   
    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    private function getActualUser(){
    	return \Yii::$app->user->identity->id;
    }
}
