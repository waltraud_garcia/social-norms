<?php

namespace common\modules\nam\controllers;

use Yii;
use yii\helpers\Html;
use common\modules\nam\controllers\LanguageController;
use common\modules\nam\models\norm\Argument;
use common\modules\nam\models\norm\Norm;
use common\modules\nam\models\content\Content;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\nam\models\norm\ArgumentRate;
use common\modules\nam\models\event\Action;

/**
 * NormController implements the CRUD actions for Norm model.
 */
class NormController extends LanguageController
{

	const NORM_ACTIVE = 2;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Norm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Norm::find(),
        ]);
				foreach($dataProvider->getModels() as $model){
					$this->shouldCloseNorm($model);
				}
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Norm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($content)
    {
        return $this->render('view', ['model' => $this->findModel($content->getNorm())]);
    }


    /**
     * Displays a single Argument model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewargument($id)
    {
    	$model = Argument::findOne($id);
    	return $this->render('../argument/view', ['model' => $model]);
    }

    /**
     * Displays a single Norm model.
     * @param integer $id
     * @return mixed
     */
    public function actionDetail($category,$section){
     	$model = Norm::find()->where(['complain_category' => $category, 'section' => [$section,4]])->orderBy('id')->one();
    	return $this->showNorm($model,$section);
    }

    /**
     * Displays a single Norm model.
     * @param integer $id
     * @return mixed
     */
    public function actionDetailargument($norm, $category,$section){
    	$model = Norm::find()->where(['id' => $norm])->orderBy('id')->one();
    	return $this->showNorm($model,$section);

    	}

    private function showNorm($model,$section){
    	$user = \Yii::$app->user->identity->id;
    	if(!is_null($model)){

	    	$users = Argument::find()->select(['user'],'distinct')->where(['norm' => $model->id])->all();
	    	$num_users = sizeof($users);
	    	//      	var_dump($num_users);
	    	//      	$arguments = Argument::find()->select(['id'])->where(['norm' => $model->id])->all();

	    	//      	$array = array();
	    	//     	foreach($arguments as $a){
	    	//      		$userArgument = ArgumentRate::find()->select(['id_user'],'distinct')->where(['id_argument' => $a['id']])->one();
	    		//      		array_push($array, $userArgument['id_user']);
	    		//      	}
	    		//      	echo sizeof($array);
	    		//      	$uniques = array_unique($array);
	    		//      	$usersRate = sizeof($uniques);
	    		//      	echo $usersRate;
    	}

    		return $this->render('detail', ['model' => $model]);
    }

		public static function shouldCloseNorm($model){
			Yii::error($model->closing);
			if($model->active==0 && $model->closing <= time()){
				Yii::error($model->closing);
				if($model->rating>2){
					$model->active = 1;
					EventController::createEvent($model->section,$model->user,null,$action=ACTION::ACTIVENORM,null,null,$model->id);
					//Yii::$app->session->setFlash('success', 'The norm was activated!');
				}else{
					$model->active = 2;
				}
				$model->save();
			}
		}
    public function actionAddargument($norm, $positive, $category, $section){
    	$model = new Argument();
    	$model->norm = $norm;
    	$user = \Yii::$app->user->identity->id;
    	$model->user = $user;
    	$model->type = $positive;
			$model->rate = 0;
			$model->numrate = 0;
			$model->weight = 0;

    	if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	EventController::createEvent($section,$user,null,$action=ACTION::ADDARGUMENT,$model->id,null,$norm); //add argument action

    		Yii::$app->session->setFlash('success', 'Your argument was added!');
    		$model = $this->findModel($norm);
    		return $this->redirect(['detailargument', 'norm'=>$norm, 'category' => $category, 'section' => $section]);
    	} else {
    		return $this->render('/argument/create', ['model' => $model]);
    	}
    }

    public function actionRate($rating,$id_argument, $norm, $section, $category){
    	$model = new ArgumentRate();
    	$user = \Yii::$app->user->identity->id;
    	$model->id_user = $user;
    	$model->id_argument = $id_argument;
    	$model->rate = $rating;

    	if($model->save()){
    		EventController::createEvent($section,$user,null,$action=ACTION::RATEARGUMENT,$id_argument,$rating); //Noupload action
				OWAController::updateRatingArgument($id_argument);
    		Yii::$app->session->setFlash('success', 'Your rate was added!');
    	}else{
    		Yii::$app->session->setFlash('danger', 'Your rate was not added!');
    	}
    	$model = $this->findModel($norm);

    	return $this->redirect(['detail', 'category' => $category, 'section' => $section]);
    }

    /**
     * Creates a new Norm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $category, $section)
    {
        $norm = new Norm();
        $norm->name = "Forbidden to upload";
        $norm->modality = 1;
        $norm->action = 1;
        $norm->complain_category = $category;
        $norm->section = $section;
        $norm->section = $section;
        $norm->rating = 0;
        $norm->ratingpos = 0;
        $norm->ratingneg = 0;
        $user = \Yii::$app->user->identity->id;

        if ($norm->load(Yii::$app->request->post()) && $norm->save()) {
        	EventController::createEvent($section,$user,null,$action=ACTION::CREATENORM,null,null,$norm->id); //Noupload action

        	//Add the first positive argument
        	$modelArgument = new Argument();
        	$modelArgument->norm = $norm->id;
        	$modelArgument->user = \Yii::$app->user->identity->id;
        	$modelArgument->type = 1;
					$modelArgument->rate = 0;
					$modelArgument->numrate = 0;
					$modelArgument->weight = 0;

        	//$content = Content::findOne($id); //Find the model
        	$modelArgument->description = $id;
        	//$modelArgument->description = "Forbidden to upload this content ".$id;
        	if($modelArgument->save()){
        		EventController::createEvent($norm->section,$user,null,$action=ACTION::ADDARGUMENT,$modelArgument->id,null,$norm->id); //add argument action
        		Yii::$app->session->setFlash('success', Yii::t('message','Your norm was added!'));
        	}else{
        		Yii::$app->session->setFlash('danger', Yii::t('message','Your norm was not added!'));
        	}

    		return $this->redirect(['detail', 'category' => $category, 'section' => $norm->section]);
       	} else {
            return $this->render('create', ['model' => $norm]);
        }
    }


    /**
     * Creates a new Norm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatenorm()
    {
    	$norm = new Norm();
    	$norm->name = "Forbidden to upload";
    	$norm->modality = 1;
    	$norm->action = 1;
    	$norm->rating = 0;
    	$norm->ratingpos = 0;
    	$norm->ratingneg = 0;

    	$user = \Yii::$app->user->identity->id;

    	if ($norm->load(Yii::$app->request->post()) && $norm->save()) {
    		EventController::createEvent($norm->section,$user,null,$action=ACTION::CREATENORM,null,null,$norm->id); //Noupload action
    		Yii::$app->session->setFlash('success', Yii::t('message','Your norm was added!'));
    		return $this->redirect(['index']);
    	} else {
    		return $this->render('create', ['model' => $norm]);
    	}
    }

    /**
     * Creates a new Norm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate2()
    {
    	$model = new Norm();
    	$model->modality = 1;
    	$model->action = 1;
    	$model->active = 0;

    	$user = \Yii::$app->user->identity->id;

    	if ($model->load(Yii::$app->request->post()) && $model->save()) {
    		EventController::createEvent($section,$user,null,$action=ACTION::CREATENORM,null,null,$model->id); //Noupload action

			//Add the first positive argument
        	$modelArgument = new Argument();
        	$modelArgument->norm = $model->id;
        	$modelArgument->user = \Yii::$app->user->identity->id;
        	$modelArgument->type = 1;
        	$modelArgument->description = "This is automatic argument";
        	$modelArgument->save();;
	    	Yii::$app->session->setFlash('success', Yii::t('message','Your norm was added!'));
    		return $this->render('detail', ['model' => $model]);
    	} else {
    		return $this->render('create', ['model' => $model]);
    	}
    }

    /**
     * Updates an existing Norm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * Deletes an existing Norm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionComplain($id){
    	$model = Complain::findOne($id);
    }


    /**
     * Finds the Norm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Norm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Norm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
