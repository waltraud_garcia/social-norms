<?php
use yii\i18n\I18N;
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

    	'i18n' =>[
	    	'translations' => [
	    		'*' => [
	    		'class' => 'yii\i18n\DbMessageSource',
	    		],    	
	    	] 
		]
    ],
		
	'modules' => [
			'admin' => ['class' => 'common\modules\admin\Module'],
			'nam' => ['class' => 'common\modules\nam\Module'],
			'gridview' =>  [
					'class' => '\kartik\grid\Module',
					// enter optional module parameters below - only if you need to
					// use your own export download action or custom translation
					// message source
					//'downloadAction' => 'gridview/export/download',
					// 'i18n' => []
			]
	],
];
