<?php
use yii\helpers\Html;
use common\modules\nam\controllers\LanguageController;

/* @var $this yii\web\View */
$this->title = 'Norm crowdsourcing';
?>


<div class="site-index">

	<div class="left">	
		<?php 
// 			$language = Yii::$app->request->cookies->getValue('language');
// 			if($language == 'ca'){
// 				echo Html::a(Yii::t('language', 'Catalan'), ['language', 'language' => 'ca'], ['class' => 'btn btn-xs btn-success']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'Spanish'), ['language', 'language' => 'es'],['class' => 'btn btn-xs btn-default']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'English'), ['language', 'language' => 'en'],['class' => 'btn btn-xs btn-default']);
// 			}else if($language == 'es'){
// 				echo Html::a(Yii::t('language', 'Catalan'), ['language', 'language' => 'ca'], ['class' => 'btn btn-xs btn-default']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'Spanish'), ['language', 'language' => 'es'],['class' => 'btn btn-xs btn-success']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'English'), ['language', 'language' => 'en'],['class' => 'btn btn-xs btn-default']);
// 			}else{
// 				echo Html::a(Yii::t('language', 'Catalan'), ['language', 'language' => 'ca'], ['class' => 'btn btn-xs btn-default']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'Spanish'), ['language', 'language' => 'es'],['class' => 'btn btn-xs btn-default']);
// 				echo "  ";
// 				echo Html::a(Yii::t('language', 'English'), ['language', 'language' => 'en'],['class' => 'btn btn-xs btn-success']);
// 			}
		?>
	</div>
	
    <div class="jumbotron">
        <h1>Norm crowdsourcing!</h1>
        <p class="lead"><?=Yii::t('title', 'Your social football network.')?></p>
        <?php 
        if(Yii::$app->user->isGuest){
		?>
        	<p><a class="btn btn-lg btn-primary" href=<?= Yii::$app->urlManager->createUrl(['site/login']) ?>>Enter</a></p> 
        	<p><a class="btn btn-lg btn-primary" href=<?= Yii::$app->urlManager->createUrl(['/site/signup']) ?>>Signup</a></p>
        	
        <?php 
        };?>
    </div>
</div>

