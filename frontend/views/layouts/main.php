<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Norm crowdsourcing',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                //['label' => 'Home', 'url' => ['/site/index'], 'visible'=>!Yii::$app->user->isGuest],
//                 ['label' => 'About', 'url' => ['/site/about'], 'visible'=>!Yii::$app->user->isGuest],
//                 ['label' => 'Contact', 'url' => ['/site/contact'], 'visible'=>!Yii::$app->user->isGuest],
                ['label' => Yii::t('section', 'Norms'), 'url' => ['/nam/norm/index'], 'visible'=>!Yii::$app->user->isGuest],
                ['label' => Yii::t('section', 'Reporter'), 'url' => ['/nam/content/reporter'], 'visible'=>!Yii::$app->user->isGuest],
                ['label' => Yii::t('section', 'Forum'), 'url' => ['/nam/content/forum'], 'visible'=>!Yii::$app->user->isGuest],
                ['label' => Yii::t('section', 'Image & Video'), 'url' => ['/nam/content/imagevideo'], 'visible'=>!Yii::$app->user->isGuest],
//                 ['label' => 'Norms', 'url' => ['/nam/norm/index'], 'visible'=>!Yii::$app->user->isGuest],
            ];
            if (Yii::$app->user->isGuest) {
//                 $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//                 $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                 $menuItems[] = [
                    'label' => Yii::$app->user->identity->username,
//                     'url' => ['/site/logout'],
//                     'linkOptions' => ['data-method' => 'post'],
                	'items' =>
                 		[
                 			//['label' => Yii::t('profile', 'Norms'), 'url' => ['/nam/norm/index'], 'visible'=>!Yii::$app->user->isGuest],
                 			//'<li class="divider"></li>',
                 			['label' => Yii::t('profile', 'My profile'), 'url' => ['/nam/user/view'], 'visible'=>!Yii::$app->user->isGuest],
                 			[
                 			'label' => Yii::t('menu', 'Logout'),
                 			'url' => ['/site/logout'],
                 			'linkOptions' => ['data-method' => 'post'],
                 			'visible'=>!Yii::$app->user->isGuest
                 			],
                 		]
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Norm crowdsourcing <?= date('Y') ?></p>
        <p class="pull-right">IIIA CSIC <?=Yii::t('other', 'and')?> UB</p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
